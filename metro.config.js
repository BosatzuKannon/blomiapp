// // // // // Learn more https://docs.expo.io/guides/customizing-metro
// // // const { getDefaultConfig } = require('expo/metro-config');

// // // module.exports = getDefaultConfig(__dirname);
// // module.exports = {
// //     resolver: {
// //       sourceExts: ['js', 'json', 'ts', 'tsx', 'cjs'], // Add 'cjs' as a source extension
// //     },
// //   };



// //_____________________________________________________________________________
// // Learn more https://docs.expo.io/guides/customizing-metro
// const { getDefaultConfig } = require('@expo/metro-config');

// const defaultConfig = getDefaultConfig(__dirname);

// // module.exports = getDefaultConfig(__dirname);
// // module.exports = {
// //     resolver: {
// //       sourceExts: ['js', 'json', 'ts', 'tsx', 'cjs'], // Add 'cjs' as a source extension
// //     },
// //   };
// module.exports = {
//   resolver: {
//     sourceExts: [...defaultConfig.resolver.sourceExts, 'cjs'],
//     defaultConfig:getDefaultConfig(__dirname)
//   },
//   // transformer: {
//   //   babelTransformerPath: require.resolve('react-native-svg-transformer'),
//   // },
// };
// //_____________________________________________________________________________

// const { getDefaultConfig } = require('@expo/metro-config');

// module.exports = getDefaultConfig(__dirname);

const { getDefaultConfig } = require('@expo/metro-config');

module.exports = (async () => {
  const defaultConfig = await getDefaultConfig(__dirname);
  if (defaultConfig.transformer && defaultConfig.transformer.babelTransformerPath) {
    defaultConfig.transformer.babelTransformerPath = require.resolve('react-native-svg-transformer');
  }
  return defaultConfig;
})();