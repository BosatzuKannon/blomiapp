import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image,TouchableWithoutFeedback} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500, statesPrimaryOverlaySelected,colorGray, surfaseDisabled,secondary50,errorColor } from '../../utils/colorVar';
import { TextInput as PaperTextInput, HelperText} from 'react-native-paper';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Checkbox from 'expo-checkbox';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default function AddBeneficiary ({navigation}) {

    const [spinner,setSpinner] = useState(false);
    const [vehicles, setVehicles] = useState([]);
    const [vehiclesSend, setVehiclesSend] = useState([]);
    const [select1, setSelect1] = useState(false);
    const [select2, setSelect2] = useState(false);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            getVehicles(value);
          }
        } catch (error) { }
      };

    const selectVehicle = (vehicle,index) => {
            
        if(vehiclesSend.length === 0){
            setVehiclesSend(vehiclesSend.concat({id:vehicle.id,typeId:vehicle.vehicle_type_id,license:vehicle.plate}));
            const array1 = vehicles.map(v => {
                if(vehicle.id === v.id){
                    v.select = true;
                    return v;
                }
                return v;
            });
            setVehicles(array1);
        }
        if(vehiclesSend.length === 1){
            setVehiclesSend(vehiclesSend.concat({id:vehicle.id,typeId:vehicle.vehicle_type_id,license:vehicle.plate}));
            const array1 = vehicles.map(v => {
                if(vehicle.id === v.id){
                    v.select = true;
                    return v;
                }
                return v;
            });
            const array2 = array1.map(v => {
                if(v.select === false){
                    v.disabled = true;
                    return v;
                }
                return v;
            });
            setVehicles(array2);
        }
    }
        const getVehicles = (token) => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            const cod = response.data.ResponseCode;
            setSpinner(false);
            if(cod === 0){
                var ve = []; 
                response.data.ResponseMessage.vehicles.map((vehi)=>{
                    vehi.disabled = false;
                    vehi.select = false;
                });
                setVehicles(response.data.ResponseMessage.vehicles);
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
        const save = async (vehicle) => {
        try {
          await AsyncStorage.setItem("vehicle", JSON.stringify(vehicle));
          navigation.navigate('Courtesy');
        } catch (e) {
          // saving error
          console.log('Fallo en guardar storage');
        }
      };
        useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        });
        return unsubscribe;
    }, [navigation]);

    return (
        <ScrollView>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            <View style={styles.container}> 
                <Text style={{color:secondary,fontSize:24,fontFamily:'Gotham-Bold'}}>Datos del beneficiario</Text>
                <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold',marginTop:10,marginBottom:20}}>Para compartir tu cortesia completa los datos correspondientes</Text>
                <Text style={{color:'#1C7A00',fontSize:16,fontFamily:'Gotham-Bold',marginTop:10,marginBottom:20,marginLeft:80}} > + AGREGAR DESDE CONTACTOS</Text> 
                <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,fontSize:14,marginTop:10}} 
                    keyboardType='default' maxLength={20}
                    theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                    mode='outlined' label="Teléfono" outlineColor={colorInputBorder}
                />
                <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,fontSize:14,marginTop:10}} 
                    keyboardType='email-address' maxLength={50}
                    theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                    mode='outlined' label="Correo eléctronico" outlineColor={colorInputBorder}
                />
                <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Bold',marginTop:20,marginBottom:20}}>Elija una placa para este beneficiario</Text>
                    <View >
                    {
                        vehicles.length !== 0 ?
                        vehicles.map( (vehicle,index) => {
                            return <View style={{justifyContent:'center'}} key={vehicle.id} >
                                        <TouchableOpacity
                                        disabled={vehicle.disabled}
                                        onPress={() => selectVehicle(vehicle,index)}
                                        style={[
                                            vehicle.disabled ?
                                            {
                                                backgroundColor: surface,
                                                borderRadius: 15,
                                                width:width-40,
                                                marginLeft: 0,
                                                marginRight: 1,
                                                marginBottom:10,
                                                marginTop:10,
                                                paddingHorizontal:10,
                                                paddingVertical:5,
                                            }
                                            :
                                            vehicle.select ?
                                            {
                                                backgroundColor: statesPrimaryOverlaySelected,
                                                borderColor:statesPrimaryOverlaySelected,
                                                borderRadius:5,
                                                shadowColor:statesPrimaryOverlaySelected,
                                                width:width-40,
                                                marginLeft: 0,
                                                marginRight: 1,
                                                elevation: 1,
                                                marginBottom:10,
                                                marginTop:10,
                                                paddingHorizontal:10,
                                                paddingVertical:5,
                                            }
                                            :
                                            {
                                                backgroundColor: surface,
                                                borderRadius: 15,
                                                width:width-40,
                                                marginLeft: 0,
                                                marginRight: 1,
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                width: 0,
                                                height: 1,
                                                },
                                                shadowOpacity: 0.49,
                                                shadowRadius: 4.65,
                                                elevation: 3,
                                                marginBottom:10,
                                                marginTop:10,
                                                paddingHorizontal:10,
                                                paddingVertical:5,
                                            }
                                        ]}
                                        >
                                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                                                {
                                                    vehicle.vehicle_type_id === 1 &&
                                                    <FontAwesomeIcon size={20} icon={faCarSide} color={vehicle.disabled ? surfaseDisabled : surfaceHighEmphasis} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 2 &&
                                                    <FontAwesomeIcon size={20} icon={faMotorcycle} color={vehicle.disabled ? surfaseDisabled : surfaceHighEmphasis} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 3 &&
                                                    <FontAwesomeIcon size={20} icon={faBicycle} color={vehicle.disabled ? surfaseDisabled : surfaceHighEmphasis} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                <View>
                                                    <Text style={[{ fontFamily: "Gotham-Bold",fontSize:16,marginTop:10 },vehicle.disabled ? {color:surfaseDisabled}:{color:surfaceHighEmphasis}]}>
                                                        {
                                                            vehicle.vehicle_type_id === 1 &&
                                                            "Automóvil"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 2 &&
                                                            "Motocicleta"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 3 &&
                                                            "Bicicleta"
                                                        }
                                                                
                                                    </Text>
                                                    <Text style={[{ fontFamily: "Gotham-Light",fontSize:14,marginTop:5 },vehicle.disabled ? {color:surfaseDisabled}:{color:surfaceHighEmphasis}]}>
                                                        {vehicle.plate}
                                                    </Text>
                                                </View>
                                                {
                                                    vehicle.select &&
                                                        <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'center',marginLeft:'auto',}}>
                                                            <MaterialIcons name="check-circle" size={24} color={primary800} />
                                                        </View>
                                                }
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                            })
                        :
                        <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 24,marginRight:10}}>
                            No tiene vehículos registrados
                        </Text> 
                    }
                    
                </View>
                <View style={{backgroundColor:surface,paddingVertical:10,alignItems:'center',alignContent:'center'}}>
                    
                        {/* {  vehiclesSend.length > 0?} */}
                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10,width: width-50,alignItems:'center'}} onPress={() => save(vehiclesSend)}>
                                <Text style={{color: '#fff',fontSize: 15,fontFamily:'Gotham-Bold'}}>AGREGAR BENEFICIARIO</Text>
                        </TouchableOpacity> 
                        
{/* 
                        <TouchableOpacity disabled={true}  style={{backgroundColor: OnSurfaceOverlay15,padding: 14,borderRadius: 10,width: width-50,alignItems:'center'}} disabled={true}>
                                <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>AGREGAR BENEFICIARIO</Text>
                        </TouchableOpacity>  */}                  
                </View>
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-350,
      marginTop:16,
    },

})