import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,Image} from 'react-native';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function BeneficiaryCourtesy({navigation }) {
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const [typeVehicleId2, setTypeVehicleId2] = useState(null);
    const [licensePlate2, setLicensePlate2] = useState(null);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };
      const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicleTypeId1");
          const value2 = await AsyncStorage.getItem("licensePlate1");
          const value3 = await AsyncStorage.getItem("vehicleTypeId2");
          const value4 = await AsyncStorage.getItem("licensePlate2");
          if (value1 !== null && value2 !== null) {
            var id = parseInt(value1);  
            setTypeVehicleId(id);
            setLicensePlate(value2);
            AsyncStorage.removeItem("vehicleTypeId1");
            AsyncStorage.removeItem("licensePlate1");
        }
        if(value3 !== null && value4 !== null){
            var id2 = parseInt(value3);
            setTypeVehicleId2(id2);
            setLicensePlate2(value4);
            AsyncStorage.removeItem("vehicleTypeId2");
            AsyncStorage.removeItem("licensePlate2");
          }
        } catch (error) {
            console.log(error);
         }
      };

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
          getDataStorage();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('BarNavigationRegister')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, paddingTop:10,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'center'}}>¡Cortesía activada!</Text>
                    <Text style={{color: secondary,fontSize: 10,fontFamily: 'Gotham-Light',textAlign:'center'}}>NOMBRE DE LA CORTESÍA</Text>
                    <View style={{alignItems:'center'}}>
                        <Image
                        source={require("../../../assets/account/ilustracion_paseo.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <View style={{paddingHorizontal:20}}>
                        <Text style={{color: secondary,fontSize: 15,fontFamily: 'Gotham-Light',textAlign:'center'}}>Para usar <Text style={{fontFamily:'Gotham-Bold'}}>CORTESÍA</Text> presenta este codigo a la entrada y salida del punto de servicio.</Text>
                    </View>
                    <View style={{marginTop:20,alignContent:'center',alignItems:'center',justifyContent:'center',marginBottom:10}}>
                        
                        <View>
                            <View style={{alignItems:'center'}}>
                                <Image
                                source={require("../../../assets/reserve/qrcode1.png")}
                                style={{ marginVertical:20 }}></Image>
                            </View> 
                        </View>
                    </View>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:20,paddingTop:10,marginTop:16}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:10}}>Detalles del servicio</Text> 
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Fecha inicio</Text>
                            <Text style={styles.textBold}>
                                dia/mes/año
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Fecha expiración</Text>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                dia/mes/año
                            </Text>
                        </View>
                    </View>
                    <TouchableOpacity style={{justifyContent:'center'}} disabled>
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 15,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        >
                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                            <View style={{justifyContent:'center'}}>
                                {
                                    typeVehicleId === null &&
                                    <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                                {
                                    typeVehicleId === 1 &&
                                    <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                                {
                                    typeVehicleId === 2 &&
                                    <FontAwesomeIcon size={20} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                                {
                                    typeVehicleId === 3 &&
                                    <FontAwesomeIcon size={20} icon={faBicycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                            </View>
                                <View style={{justifyContent:'center',alignContent:'center',marginHorizontal:'auto'}}>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16 }}>
                                        {licensePlate ? licensePlate : 'OII 27F' }
                                    </Text>
                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14 }}>
                                        {typeVehicleId2 ? typeVehicleId2 === 1 ? 'Automóvil' : typeVehicleId2 === 2 ? 'Motocicleta' : typeVehicleId2 === 3 && 'Bicicleta'  : 'Automovil' }
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{justifyContent:'center',alignItems:'center'}} disabled>
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 15,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        >
                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                                <View style={{justifyContent:'center'}}>
                                    {
                                        typeVehicleId2 === null &&
                                        <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                    }
                                    {
                                        typeVehicleId2 === 1 &&
                                        <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                    }
                                    {
                                        typeVehicleId2 === 2 &&
                                        <FontAwesomeIcon size={20} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                    }
                                    {
                                        typeVehicleId2 === 3 &&
                                        <FontAwesomeIcon size={20} icon={faBicycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                    }
                                </View>
                                <View style={{justifyContent:'center',alignContent:'center',marginHorizontal:'auto'}}>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16}}>
                                        {licensePlate2 ? licensePlate2 : 'OII 27F' }
                                    </Text>
                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14 }}>
                                        {typeVehicleId2 ? typeVehicleId2 === 1 ? 'Automóvil' : typeVehicleId2 === 2 ? 'Motocicleta' : typeVehicleId2 === 3 && 'Bicicleta'  : 'Automovil' }
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.containerAccordion}>
                    <View>
                        <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary}}>Beneficios</Text>
                    </View>
                    <Text style={{color:secondary,fontFamily:'Gotham-Light',fontSize:16,marginVertical:10}}><Text style={{fontFamily:'Gotham-Bold'}}>1.</Text> Puedes cambiar de parqueadero hasta 10 veces en el día</Text> 
                    <Text style={{color:secondary,fontFamily:'Gotham-Light',fontSize:16}}><Text style={{fontFamily:'Gotham-Bold'}}>2.</Text> Estaciona el tiempo que quieras hasta el cierre del parqueadero, o hasta media noche si éste es 24 horas</Text>
                </View>
            </ScrollView>
            <View style={styles.container2}>
                <View style={{marginVertical:10,alignItems:'center',alignContent:'center'}}>
                    <TouchableOpacity style={styles.button1} >
                            <Text style={styles.titleButton1}>PARQUEADEROS DISPONIBLES</Text>
                    </TouchableOpacity> 
                </View>
                <View style={{marginVertical:10,flex:3}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ListCourtesy')}>
                        <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',textAlign:'center'}}>CERRAR</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
        marginBottom:150,
        padding:20
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 16,
        fontFamily: 'Gotham-Medium',
        marginBottom:50,
        marginTop:20,
        alignItems:'center',
        textAlign:'center'
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        marginLeft:10,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-60,
        flexDirection:'row',
        alignItems: "center",
        alignContent:'center',
        justifyContent:'center',
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })