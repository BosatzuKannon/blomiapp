import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,Image} from 'react-native';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import QRCode from 'react-native-qrcode-svg';
import Spinner from "react-native-loading-spinner-overlay";
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import Toast from 'react-native-simple-toast';
import { Request } from '../../utils/api';
import { PRODUCT } from '../../utils/endpoints';

export default function ActiveCourtesy1({route,navigation }) {
    const [spinner,setSpinner] = useState(false);
    const [qrValue, setQrValue] = useState('');
    const [infoService, setInfoService] = useState([]);
    const [vehicles, setVehicles] = useState([]);
    const [customer, setCustomer] = useState('');

    const {id} = route.params;

    const getServiceInfo = async () => {

        setSpinner(true)
        const request = new Request();
        const result = await request.request(PRODUCT.infoCustomerProduct,'POST',{id:id})
        if(result === 0){
            setSpinner(false)
            Toast.showWithGravity('Código no valido', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        }else{
            setSpinner(false)
            setInfoService(result.ResponseData)
            setVehicles(result.ResponseData.vehicles)
            setCustomer(result.ResponseData.customer)
            const data = {
                customer_product:result.ResponseData.product_type_id,
                customer_id:result.ResponseData.customer_id,
                status:result.ResponseData.status
            }
            setQrValue(JSON.stringify(data))
        }
    }
      //ir a la lista de parking
    //   const goToListParking = () => {
    //       global.listParkingCourtesy = true;
    //       global.listParkingBeParking = false;
    //       navigation.navigate('ListParking');
    //   }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getServiceInfo()
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <View style={{ marginBottom:20 }}>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:15, alignSelf: 'center' }}>Cortesía adquirida</Text>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color: "#005A6D", alignSelf: 'center' }}>Presenta este codigo a la entrada y salida del punto de servicio para disfrutar tus beneficios</Text>
                    </View>

                    <View style={{alignItems:'center', marginLeft: -25, marginBottom:30}}>
                        <QRCode
                            value={ qrValue ? qrValue : 'NA carajo' }
                            size={ 130 }
                            color= 'white'
                            backgroundColor='#519B00'
                        >
                        </QRCode>
                    </View> 

                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:20, color:secondary, marginBottom:15 }}>Detalles del servicio</Text>

                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginRight:150, marginBottom:5}}>Estado</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {infoService.status === 1 ? 'Activo' : 'Inactivo' }
                            </Text>
                        </View>
                        {/* <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginRight:50, marginBottom:5}} >Fecha de expiración</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {dataVip1.final_date}
                            </Text>
                        </View> */}
                    </View>
                    {/* Botón lista parqueaderos */}
                    {/* <TouchableOpacity style={styles.button} onPress={ () => navigation.navigate('ListParkingVipGold') }>      
                        <Text style={{fontFamily:'Gotham-Bold', color:'#005A6D', fontSize: 14, textAlign:'center'}}>CONSULTAR PARQUEADEROS</Text>                
                    </TouchableOpacity> */}
                    {/* boton para agregar otro vehiculo */}
                    {/* {
                        vehicles.length  > 1 ? 
                            <TouchableOpacity style={styles.button1} onPress={ () => navigation.navigate('ListParkingVipGold') }>      
                                <Text style={{fontFamily:'Gotham-Bold', color:'#005A6D', fontSize: 14, textAlign:'center'}}>AGREGAR VEHICULO</Text>                
                            </TouchableOpacity>
                        :
                            <View></View>
                    } */}
                    
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:20, color:secondary, marginBottom:15, marginTop:20 }}>Detalles del Beneficiario</Text>
                    
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary,  marginBottom:5}}>Nombre</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.name} {customer.lastname} 
                            </Text>
                        </View>
                        <View style={{marginLeft:'auto', marginRight:10}}>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >Tipo de documento</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.document_type == 1 ? 'CC' : '-'}
                            </Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}}>N° Documento</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.document}
                            </Text>
                        </View>
                        <View style={{marginLeft:'auto', marginRight:50}}>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >N° Télefono</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.phone}
                            </Text>
                        </View>
                    </View> 
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}}>Correo</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.email}
                            </Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View >
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >Placas</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>                                
                                <View style={{flexDirection:'row',justifyContent:'center', alignItems:'center'}}>
                                    <Text style={{marginRight:10, color:secondary,fontFamily:'Gotham-Medium'}}>{Object.entries(vehicles).length > 0 ? `${vehicles[0].vehicle_plate}` : ''} {Object.entries(vehicles).length > 1 ? ` - ${vehicles[1].vehicle_plate}` : ''}</Text>
                                </View>
                            </Text>
                        </View>
                    </View>
                    
                
                    <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                        <View style={{ marginTop:40}}>
                            <Text style={{alignSelf:'center', fontSize:14, color:'rgba(0, 45, 51, 0.6)'}}>CERRAR</Text>
                        </View>
                    </TouchableOpacity>
                </View>
        </ScrollView>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
        marginBottom:150,
        padding:20
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 16,
        fontFamily: 'Gotham-Medium',
        marginBottom:50,
        marginTop:20,
        alignItems:'center',
        textAlign:'center'
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        marginLeft:10,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-60,
        flexDirection:'row',
        alignItems: "center",
        alignContent:'center',
        justifyContent:'center',
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })