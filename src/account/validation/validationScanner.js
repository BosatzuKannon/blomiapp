import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Modal,Dimensions, TouchableWithoutFeedback, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faQrcode} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { TextInput as PaperTextInput, HelperText} from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ValidationScanner ({navigation}){
    const [spinner,setSpinner] = useState(false);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [code, setCode] = useState(null);

    const codValid = () => {

        if(code === null || code === ""){
            Alert.alert("","La placa es obligatoria");
        }
            else if (code == 'ABC123'){
                Alert.alert("", "Placa Valida");
                navigation.navigate('ViewValidation')
            }
        
        else {
            Alert.alert("", "Placa no valida o no autorizada"); 
        }
    }
    const terms = () => {
        if(setDialogTermsVisible){
            //setDialogTermsVisible(true);
            navigation.navigate('ViewValidation')
        }
        else {
            navigation.navigate('ViewValidation')
            console.log('entra vista')
        }
        //setDialogTermsVisible(true);       
    }
   
    return (
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('Account')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <Text style={{fontFamily:'Gotham-Bold', color:secondary, fontSize:24}}>Bienvenido a validaciones</Text>
                <Text style={{fontFamily:'Gotham-Medium', color:'#005A6D', fontSize:16, marginTop:20}}>Si has adquirido un descuento por compras en los almacenes habilitados, podras canjearlos de manera
                    facil y rapida. Solo tienes que leer el codigo QR que se te ha proporcionado para redimir tu beneficio.
                </Text>
                <Text style={{fontFamily:'Gotham-Medium', color:'#005A6D', fontSize:16, marginTop:20}}>Recuerda: Al canjear el beneficio solo aplicara para el servicio en curso
                </Text>
                <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 30}}>
                    <TouchableOpacity style={styles.button} onPress={terms}>
                        <View style={{flexDirection:'row'}}>
                            <View style={{marginRight:10,marginLeft:20}}>
                                <FontAwesomeIcon size={25} icon={faQrcode} color={secondary} />
                            </View>
                            <View>
                                <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold', color:secondary}}>
                                    LEER CODIGO
                                </Text>
                            </View>
                        </View>               
                    </TouchableOpacity>
                </View>
                <View style={{marginBottom:10}}>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,fontSize:14,marginTop:10, marginBottom:30}} 
                            onChangeText={(text) => {setCode(text)}} 
                            keyboardType='text' maxLength={6}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Introduzca placa" />
                        <TouchableOpacity style={{marginBottom:20, borderWidth: 1, borderColor:secondary, width:200, height:40, borderRadius: 10, justifyContent: 'center', marginLeft:'auto', marginRight:'auto'}} onPress={codValid}>
                            <Text style={{color: secondary,fontSize: 14,fontFamily:'Gotham-Medium',marginLeft:3,textAlign:'center'}}> VALIDAR PLACA</Text>
                        </TouchableOpacity>
                </View>
                <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>No se registran beneficios por validacion actualmente</Text>
                                <Text style={styles.text2}>Si cuenta con un beneficio, redimalo escaneando el codigo que le han suministrado.</Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: '#BAE65B', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                </View>
                <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 30, marginTop:100}}>
                    <TouchableOpacity style={styles.button1}>
                        <View>
                            <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold', color:secondary, textAlign:'center'}}>
                                VER BENEFICIOS ADQUIRIDOS
                            </Text>
                        </View>        
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>    
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:20,
        height: 710
    },
    button: {
        borderColor: '#005A6D',
        borderWidth: 1,
        width: 200,
        height:50,
        justifyContent:'center',
        borderRadius: 10
    },
    button1: {
        borderColor: '#005A6D',
        borderWidth: 1,
        width: 300,
        marginLeft:'auto',
        marginRight:'auto',
        height: 40,
        justifyContent: 'center',
        borderRadius:10 
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
    },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    title: {
        color: '#005A6D',
        fontSize: 20,
        fontFamily:'Gotham-Bold',
        textAlign: 'center'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Medium',
        marginBottom:50,
        marginTop:20,
        textAlign: 'center'
    },
})