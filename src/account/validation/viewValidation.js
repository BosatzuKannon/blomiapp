import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View,Image, TouchableOpacity,ScrollView,CheckBox,Modal,Dimensions, TouchableWithoutFeedback, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faQrcode} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ViewValidation ({navigation}){
    const [spinner,setSpinner] = useState(false);


    return (
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('ValidationScanner')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <Text style={{fontFamily:'Gotham-Bold', color:secondary, fontSize:24, textAlign:'center'}}>En hora buena tienes un nuevo beneficio</Text>
                <Text style={{fontFamily:'Gotham-Medium', color:'#005A6D', fontSize:16, textAlign:'center', marginTop:30}}>Felicidades Kenso Jeans te ha otorgado un beneficio sobre tu servicio en curso</Text>
                <View style={{marginLeft:'auto', marginRight:'auto'}}>
                    <Image
                    source={require("../../../assets/reserve/ilustracion_valet.png")}
                    style={{ marginVertical:20 }}></Image>
                </View>
                <Text style={{fontFamily:'Gotham-Medium', color:'#005A6D', fontSize:24, marginTop:10}}>Detalles</Text>
                <View style={{flexDirection: 'row', marginTop:20}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14}}>Beneficiario</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14, marginLeft:'auto', marginRight:30}}>Tipo de descuento</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:10}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16}}>Kenzo Jeans</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16, marginLeft:'auto', marginRight:30}}>Porcentaje</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:20}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14}}>Descuento</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14, marginLeft:'auto', marginRight:30}}>Fecha expiracion</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:10}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16}}>20%</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16, marginLeft:'auto', marginRight:30}}>15/08/2022</Text>
                </View>
                {/* boton accion */}
                <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 30, marginTop:80}}>
                    <TouchableOpacity style={styles.button1} onPress={() => navigation.navigate('SumaryValidation')}>
                        <View>
                            <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold', color:'#F2FAE0', textAlign:'center'}}>
                                SIGUIENTE
                            </Text>
                        </View>        
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:20,
      },
      button1: {
        backgroundColor:'#005A6D',
        width: 300,
        marginLeft:'auto',
        marginRight:'auto',
        height: 40,
        justifyContent: 'center',
        borderRadius:10 
    },
})