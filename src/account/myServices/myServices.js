import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../../utils/api';
import { PRODUCT } from '../../utils/endpoints';

export default function myServices({navigation}){
    const [spinner, setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [services, setServices] = useState([]);

    const getServices = async () => {
        setSpinner(true)
        const request = new Request();
        const result = await request.request(PRODUCT.listProductPerUser,'POST',null)
        if(result === 0){
            setSpinner(false)
            Alert.alert("Error al registrar producto");
            console.log(result)
        }else{
            setSpinner(false)
            let serv = result.ResponseData;
            let temp = serv.filter(ser => ser.status.toString() === '1' || ser.status.toString() === '7');
            setServices(temp);
        }
    }


    const viewService =  async (service) =>{
        //aqui se valida si esta pendiente de pago
        if(service.product_id.toString()  == "1"){
          navigation.navigate('LegalActive');
        }else if(service.product_id.toString()  == "2"){
            navigation.navigate('InfoServiceHour', {service:service}); //vista para detalle de horas
        }else if(service.product_id.toString()  == "3"){
            navigation.navigate('MembershipActivated1', {id:service.id}); //vista para detalle de horas
        }else if(service.product_id.toString()  == "4"){
            navigation.navigate('ActiveCourtesy', {id:service.id}); //vista para detalle de horas
        }else{
          // await AsyncStorage.setItem('newMonthlyId',id.toString()); navigation.navigate('MembershipActivated1', {id:progress});
          navigation.navigate('ActiveMonthly');
        }
        
    }

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
        getServices()
    });
    return unsubscribe;
  }, [navigation]);

    return(

       <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                {/* <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View> */}

                <View style={styles.container}>
                <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:10}}>Mis Servicios</Text>
                <Text style={{ fontFamily: 'Gotham-Light', fontSize:14, color: "#000", marginBottom:10}}>Aquí puedes ver la lista de servicios que tienes habilitados en el momento.</Text>
                <View >
                    {services.map((service) =>{
                    
                        return <TouchableOpacity style={{justifyContent:'center'}} key={service.id} onPress={() => viewService(service) }>
                                    <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >   
                                            <View>
                                                 <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color: 'rgba(0, 45, 51, 0.8)', marginLeft:15  }}>
                                                    {service.product_id.toString() === '1' && service.product_type_id.toString() === '1' ? 'Mensualidad' :
                                                     service.product_id.toString() === '1' && service.product_type_id.toString() === '3' ? 'Mensualidad Jurídica' : 
                                                     service.product_id.toString() === '2' && service.product_type_id.toString() === '1' ? 'Horas' :
                                                     service.product_id.toString() === '3' && service.product_type_id.toString() === '1' ? 'VIP/GOLD' :
                                                     service.product_id.toString() === '4' && service.product_type_id.toString() === '1' ? 'Cortesía' : 'Otro'
                                                    }  
                                                </Text> 
                                            </View>

                                            <View style={{flexDirection: 'row',marginLeft:15}}>
                                                {/* <FontAwesomeIcon size={23} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/> */}
                                                <Text style={{textTransform: 'uppercase', fontFamily: "Gotham-Bold", color: "rgba(0, 45, 51, 0.8)",fontSize:12 }}>
                                                   {service.status.toString() === '1' ? 'Activo' : 
                                                    service.status.toString() === '7' ? 'Pendiente de pago' : '-'} | 
                                                    { Object.keys(service.vehicles).length > 2 ? ` ${service.vehicles.vehicle_plate}` : 
                                                        ` ${service.vehicles[0].vehicle_plate} ${service.vehicles[1] ? `- ${service.vehicles[1].vehicle_plate}` : ''} ` }             
                                                </Text>
                                            </View>
                                            {/* <View>
                                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14, marginLeft:15 }}>
                                                    Vigente por 2 meses                                                    
                                                </Text>
                                            </View> */}
                                            <View>
                                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14, marginLeft:15, marginBottom:5 }}>
                                                    {service.product_id.toString() != '2' ?
                                                    service.automatic_suscription ? 'Suscripción automatica activada' : 'Suscripción automatica desactivada' :
                                                    service.facility.name}
                                                </Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                        })
                    }
                </View>
                
            </View>
       </ScrollView>


    )


}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:20,
      paddingTop:20,
    },




})