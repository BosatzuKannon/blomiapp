import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, TouchableWithoutFeedback} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50 } from '../../utils/colorVar';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { TextInput as PaperTextInput } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import Checkbox from 'expo-checkbox';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default function FormBeParking({navigation }) {
    const [spinner, setSpinner] = useState(false);
    const [document, setDocument] = useState(null);
    const [typeDocumentId, setTypeDocumentId] = useState(null);
    const [typeDocument, setTypeDocument] = useState([
        {
            name:'Cédula de ciudadania',
            id:1,
        },
        {
            name:'Documento extranjero',
            id:2,
        },
        {
            name:'NIT',
            id:3,
        },
        {
            name:'Pasaporte',
            id:4,
        },
        {
            name:'Rut',
            id:5,
        },
    ]);
    const [isSelected, setSelection] = useState(false);
    const [modalTermsVisible, setModalTermsVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [modalSkipVisible, setModalSkipVisible] = useState(false);
    

    const getToken =  async () => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                global.token = value
                setToken(value);
                //continueButton(value)
            }
          } catch (error) {
            console.log(error);
          }
    }

    const continueButton = () => {

        const config = {
            headers: { Authorization: global.token }
            };
            setSpinner(true);

        // Activar usuario a be parking

            axios.post(`${API_URL}customer/app/beParking`,
            {
                "documentTypeId": typeDocumentId,
                "documentNumber": document     
            },config).then(response => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                console.log(response.data.ResponseMessage);
                console.log(cod);
                if(cod === 0){
                    console.log('usuario activo be parking');
                }else{
                    Alert.alert("ERROR","No se pudo traer los vehiculos");
                }
                }).catch(error => {
                setSpinner(false);
                Alert.alert("",error.response.data.ResponseMessage);
                console.log(error.response.data.ResponseMessage);
                })  

        // Verificar lista vehiculo usuario
    
           setSpinner(true);
            axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            // console.log(response.data.ResponseMessage)
                if(cod === 0){
                     if (response.data.ResponseMessage.vehicles.length === 0){
                         navigation.navigate('AddVehicleBeParking');
                    }else{
                        navigation.navigate('BeParking');
                        console.log('entra be parking');
                    } 
                }else{
                    Alert.alert("ERROR","No se pudo registar be parking");
                } 
            }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            
            })        
         
    }
    const getInfo = () => {

        const config = {

            headers: { Authorization: global.token }
        };

         axios.get(`${API_URL}customer/app/getInfoCustomer`,config).then(response => {
        setSpinner(false);
        const cod = response.data.ResponseCode;
        console.log(response.data.ResponseMessage);
        if(cod === 0){
            if (response.data.ResponseMessage.document_number.length > 0) {
                setDocument(response.data.ResponseMessage.document_number)
                if (response.data.ResponseMessage.document_type_id === 1 ||
                    response.data.ResponseMessage.document_type_id === 2 ||
                    response.data.ResponseMessage.document_type_id === 3 ||
                    response.data.ResponseMessage.document_type_id === 4 ||
                    response.data.ResponseMessage.document_type_id === 5
                ) {
                   setTypeDocumentId(response.data.ResponseMessage.document_type_id)
                }
                navigation.navigate('FormBeParking')
            } 
        }else{
            Alert.alert("ERROR","No se pudo traer la informacion del usuario");
        }
        }).catch(error => {
        setSpinner(false);
        Alert.alert("",error.response.data.ResponseMessage);
        console.log(error.response.data.ResponseMessage);
        }) 

    }
    const terms = () => {
        setModalTermsVisible(true);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
          getInfo();
        });
        return unsubscribe;
      }, [navigation]);
    return (
        <KeyboardAvoidingView style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{ marginTop: 15 }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Text style={{color:secondary,fontSize:24,fontFamily:'Gotham-Bold',marginTop:20}}>Be parking</Text>
                <Text style={{color:'#000',fontSize:14,fontFamily:'Gotham-Medium',marginTop:20}}>lorem ipsum dolor consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                et dolore magna aliqua</Text>
                <TouchableOpacity onPress={() => navigation.navigate('GetPoints')}>
                    <View style={{flexDirection:'row',backgroundColor:onSurfaceOverlay8,borderRadius:25,marginVertical:10,paddingVertical:8,paddingHorizontal:10,borderWidth:1,borderColor:secondary,width:200,marginTop:20}}>
                        <MaterialIcons name="help-outline" size={24} color={surfaceHighEmphasis} />
                        <Text style={{color: surfaceHighEmphasis,fontSize: 14,fontFamily:'Gotham-Medium',marginLeft:3}}>Mas información</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.form}>
                    <Text style={styles.title}>Documento de identidad</Text>
                    <Text style={styles.text}>Descripción</Text>
                    <View style={{justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                        <View style={styles.pickerStyle}>
                            <Picker 
                                selectedValue={typeDocumentId}
                                style={{height: 40, width: width-60,borderColor: secondary,borderWidth: 1}} 
                                onValueChange={(itemValue, itemIndex) => setTypeDocumentId(itemValue)}>
                                    <Picker.Item label="Tipo de documento" color={colorInput} value={null} enabled={false}/>
                                    {
                                    typeDocument.map( (type) => {
                                    return <Picker.Item label={type.name} color={colorInput} value={type.id} key={type.id}/>
                                    })
                                    }  
                            </Picker>
                        </View> 
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:20,fontSize:14}} 
                        onChangeText={(text) => {setDocument(text)}} 
                        value={document} keyboardType='numeric'
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Número de documento" outlineColor={colorInputBorder}/>
                    </View>
                    <View style={{flexDirection:'row',top:70}}>
                        <Checkbox
                            value={isSelected}
                            onValueChange={setSelection}
                            style={{ color:primary800}}
                            color={isSelected ? primary600 : surfaceMediumEmphasis}
                        />
                        <TouchableOpacity onPress={terms}>
                            <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Light',marginTop:10,textDecorationLine:'underline'}}>Acepto terminos y condiciones</Text>
                        </TouchableOpacity>
                    </View>
                    <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalTermsVisible}
                    >
                        <TouchableWithoutFeedback onPress={() => setModalTermsVisible(!modalTermsVisible)}>
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <Text style={styles.title}>Terminos y condiciones</Text>
                                    <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                    <View style={{marginBottom:10}}>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setModalTermsVisible(!modalTermsVisible)}>
                                            <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </Modal>
                </View>
                {
                    document !== null && typeDocumentId !== null && isSelected !== false ?
                    <TouchableOpacity style={styles.button1} onPress={continueButton}>
                        <Text style={styles.titleButton1}>CONTINUAR</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={{marginTop: 90,height: 50,marginBottom:10,alignItems: "center",backgroundColor: OnSurfaceOverlay15,padding: 14,borderRadius: 10,width: width-50,}} disabled={true}>
                        <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>CONTINUAR</Text>
                    </TouchableOpacity> 

                }
                <TouchableOpacity style={{alignContent:'center',padding:15,alignItems:'center',justifyContent:'center',}} onPress={() => setModalSkipVisible(true)}>
                    <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily:'Gotham-Light',textAlign:'center',
                    justifyContent:'center',alignContent:'center',alignItems:'center'}}>OMITIR</Text>
                </TouchableOpacity>
                <Modal
                animationType="slide"
                transparent={true}
                visible={modalSkipVisible}
                >
                    <ScrollView>
                    <TouchableOpacity style={styles.centeredView2}>
                    <View style={{marginTop:-10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                        <TouchableOpacity onPress={() => setModalSkipVisible(!modalSkipVisible)}>
                            <MaterialIcons name="close" size={25} color={secondary} />
                        </TouchableOpacity>
                    </View>
                        <TouchableWithoutFeedback style={styles.modalView2}>
                            <View >
                                <View >
                                    <Text style={styles.title}>Beneficios Be Parking</Text>
                                    <Text style={styles.text2}>Si omites la cedula no podrás acumlar puntos y convertirte en Be Parking</Text>
                                    <View style={{marginBottom:10}}>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setModalSkipVisible(!modalSkipVisible)}>
                                            <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity style={{alignContent:'center',padding:15,alignItems:'center',justifyContent:'center',}}>
                                        <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily:'Gotham-Light',textAlign:'center',
                                        justifyContent:'center',alignContent:'center',alignItems:'center'}}>OMITIR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TouchableWithoutFeedback> 
                    </TouchableOpacity>         
                    </ScrollView>
                </Modal>
            </ScrollView>
        </KeyboardAvoidingView>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:surface
    },
    form: {
        alignItems: 'flex-start',
        marginTop: 60,
    },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily: 'Gotham-Bold',
    },
    text: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Light',
    },
    input: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 320,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
    inputPhone: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 180,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily: 'Gotham-Bold',
    },
    button1: {
        marginTop: 90,
        height: 50,
        backgroundColor: secondary,
        padding: 14,
        marginBottom:10,
        borderRadius: 10,
        width: width-50,
        alignItems: "center",
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginVertical:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
    },
    modalView2: {
        elevation:10,
    },
    centeredView2: {
        flex: 1,
        backgroundColor:"white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        elevation: 15,
        marginTop:height-210,
        padding: 20,
      },
})
