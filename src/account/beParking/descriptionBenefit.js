import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,errorColor,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50 } from '../../utils/colorVar';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default function DescriptionBenefit ({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [modalTermsVisible, setModalTermsVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [progress, setProgress] = useState(0.5);
    const [blue, setBlue] = useState(true);
    

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }

    const continueButton = () => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage.vehicles.length === 0){
                    navigation.navigate('AddVehicleBeParking');
                }else{
                    setVehicles(true);
                }
            }else{
                Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
            }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            })

    }
    const terms = () => {
        setModalTermsVisible(true);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
        });
        return unsubscribe;
      }, [navigation]);
    return (
        <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('BeParking')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={{backgroundColor:surface,padding:20}}>
                <View>
                    <Image source={require('../../../assets/account/logopeque_green1.png')} width={106} height={62}/>
                </View>
            </View>
            <View style={{alignContent:'center',alignItems:'center',justifyContent:'center',backgroundColor:surface,paddingHorizontal:20,paddingBottom:10}}>
                <View style={{borderRadius:50,backgroundColor:onSurfaceOverlay8,padding:20}}>
                    <Ionicons name="build-outline" size={24} color={surfaceHighEmphasis} style={{textAlign:'center'}} />
                </View>
                <Text style={{color: secondary,fontSize: 24,fontFamily:'Gotham-Bold',}}>15% de descuento</Text>
                <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily:'Gotham-Light'}}>Mecánica</Text>
                <TouchableOpacity onPress={() => navigation.navigate('GetPoints')}>
                    <View style={{flexDirection:'row',backgroundColor:onSurfaceOverlay8,borderRadius:25,marginVertical:10,paddingVertical:8,paddingHorizontal:10}}>
                        <MaterialCommunityIcons name="alert-circle" size={24} color={surfaceHighEmphasis}/>
                        <Text style={{color: surfaceHighEmphasis,fontSize: 14,fontFamily:'Gotham-Medium',marginLeft:3}}>Terminos y condiciones</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row',paddingHorizontal:20,paddingTop:10}}>
                <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 24,marginRight:10}}>
                    Puntos de servicio
                </Text>
            </View>
            <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:20,marginTop:16,height:height-300}}>
                <View style={{flexDirection:'row',marginBottom:20}}>
                    <View style={{justifyContent:'center'}}>
                        <MaterialCommunityIcons name="map-marker-outline" size={24} color={surfaceHighEmphasis} />
                    </View>
                    <View style={{marginLeft:20}}>
                        <Text style={{textAlign:'left', color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Alcazares</Text>
                        <Text style={{textAlign:'left', color: surfaceMediumEmphasis,fontSize: 13,fontFamily:'Gotham-Light',}}>Calle 27 # 3</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row',marginBottom:20}}>
                    <View style={{justifyContent:'center'}}>
                        <MaterialCommunityIcons name="map-marker-outline" size={24} color={surfaceHighEmphasis} />
                    </View>
                    <View style={{marginLeft:20}}>
                        <Text style={{textAlign:'left', color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Colsubsidio</Text>
                        <Text style={{textAlign:'left', color: surfaceMediumEmphasis,fontSize: 13,fontFamily:'Gotham-Light',}}>Calle 27 # 30-90</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row',marginBottom:20}}>
                    <View style={{justifyContent:'center'}}>
                        <MaterialCommunityIcons name="map-marker-outline" size={24} color={surfaceHighEmphasis} />
                    </View>
                    <View style={{marginLeft:20}}>
                        <Text style={{textAlign:'left', color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Parking</Text>
                        <Text style={{textAlign:'left', color: surfaceMediumEmphasis,fontSize: 13,fontFamily:'Gotham-Light',}}>Calle 27 # 45-25</Text>
                    </View>
                </View>
            </View>
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        padding:20,
    },
    form: {
        alignItems: 'flex-start',
        marginTop: 120,
    },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily: 'Gotham-Bold',
    },
    text: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Light',
    },
    input: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 320,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
    inputPhone: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 180,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily: 'Gotham-Bold',
    },
    button1: {
        marginTop: 90,
        height: 50,
        backgroundColor: secondary,
        padding: 14,
        marginBottom:10,
        borderRadius: 10,
        width: width-50,
        alignItems: "center",
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginVertical:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
    },
    modalView2: {
        elevation:10,
    },
    centeredView2: {
        flex: 1,
        backgroundColor:"white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        elevation: 15,
        marginTop:height-210,
        padding: 20,
      },
})
