import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import { TextInput as PaperTextInput } from 'react-native-paper';
import axios from "axios";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListParksBeParking ({navigation}) {

    const [spinner,setSpinner] = useState(false);
    const [listParking, setListParking] = useState([]);
    const [token, setToken] = useState(null);
    const [filterList , setFilterList] = useState('');

    const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        global.token = value;
        setToken(value);
        getParkingList(value);
      }
    } catch (error) { }
    };

    const searchTextList = (text) => {
        setFilterList(text);
    }

    const selectedParking = async (parkingId) => {
        try {
          let aux = 20;
            await AsyncStorage.setItem("parkingId", aux.toString());
            navigation.navigate('SelectedPark');
        } catch (error) {
            console.log(error);
        }
        
    }


  ///Método para traer los parqueaderos activos
  const getParkingList = (token) =>{

    const config = {
      headers: { Authorization: token },  
    };
    setSpinner(true);

    let arrayParking = {
        "productId":21
    }
  
    axios.get(`${API_URL}customer/app/getInfoMonthlyByUser/getListParkingQuoteMonthly/27`, config).then(response => {
    setSpinner(false);
    console.log(`Archivo config __________ ${JSON.stringify(response.data.ResponseMessage)}`)
        const cod = response.data.ResponseCode;
      if(cod === 0){
        const list = response.data.ResponseMessage;
        setListParking(list);
      }else{
        Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
      }   
    }).catch(error => {
      setSpinner(false);
      console.log(error.response  );
      Alert.alert("error prueba",error.response.data.ResponseMessage);
      //console.log(error);
    }) 
   }
    useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getToken();
      //getParkingList();
    });
    return unsubscribe;
  }, [navigation]);


    return(
       
        <ScrollView>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('BeParking')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
             <View style={styles.container}>
                <View >
                    <Text style={{color:secondary, fontSize:24, fontFamily:'Gotham-Bold',marginTop:15}}>Tiempo grátis por afiliación</Text>
                    <Text style={{color:'rgba(0, 45, 51, 0.8)', fontSize:14, fontFamily:'Gotham-Bold', textAlign: 'center'}}>1 Hora | Disponible por 30 dias</Text>
                </View>
                <Text style={{color:secondary, fontSize:18, fontFamily:'Gotham-Bold',marginTop:50}}>Listado de parqueaderos</Text>
                <PaperTextInput style={{width:width-40,backgroundColor:surface,color:colorInput,borderRadius:10,marginTop:5}} 
                    right={<PaperTextInput.Icon 
                    name={() => <MaterialIcons name="search" size={30} color={secondary} />} />}
                    theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                    mode='outlined' label="Buscar" outlineColor={colorInputBorder}
                    onChangeText={(text) => searchTextList(text)}
                />
                            {
                  listParking.length !== 0 &&

                listParking.filter((parking) =>{
                          if(filterList == ""){
                            return parking
                          }else if(parking.commercial_name.toLowerCase().includes(filterList.toLowerCase())){
                              return parking
                          }
                        }).map( (parking) => {
                    return <TouchableOpacity style={{justifyContent:'center'}} key={parking.id} onPress={() => selectedParking(parking.id)} >
                                <View
                                style={{
                                    backgroundColor: surface,
                                    borderRadius: 5,
                                    width:width-40,
                                    marginLeft: 0,
                                    marginRight: 1,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                    width: 0,
                                    height: 1,
                                    },
                                    shadowOpacity: 0.49,
                                    shadowRadius: 4.65,
                                    elevation: 3,
                                    paddingBottom:10,
                                    marginTop:10,
                                    paddingTop:5,
                                }}
                                >
                                    <View style={{marginBottom:10,marginLeft:15}}>
                                        <View>
                                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}>
                                                {parking.commercial_name}
                                            </Text>
                                            {
                                                parking.address !== null ? 
                                                <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                    {parking.address}
                                                </Text>
                                                :
                                                <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                    Este parqueadero no tiene dirección
                                                </Text> 

                                            }
                                            
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                    }) 
            }
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-50,
        paddingTop:10,
    }
})