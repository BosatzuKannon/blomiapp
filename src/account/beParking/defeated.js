import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50 } from '../../utils/colorVar';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
export default function Defeated({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [modalTermsVisible, setModalTermsVisible] = useState(false);
    const [token, setToken] = useState(null);
    const [progress, setProgress] = useState(0.5);
    const [blue, setBlue] = useState(true);
    

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }

    const continueButton = () => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage.vehicles.length === 0){
                    navigation.navigate('AddVehicleBeParking');
                }else{
                    setVehicles(true);
                }
            }else{
                Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
            }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            })

    }
    const terms = () => {
        setModalTermsVisible(true);
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
        });
        return unsubscribe;
      }, [navigation]);
    return (
        <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingVertical:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row',paddingHorizontal:20,paddingVertical:10}}>
                <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 24,marginRight:10}}>
                    Historial
                </Text> 
            </View>
            <View style={{backgroundColor:surface,paddingHorizontal:20,paddingVertical:10,}}>
                <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                    <View >
                        <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>1 hora gratis por afiliación</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>24 horas posterior a la afiliación, recibe una hora (1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>GRATIS de parqueo con vigencia de un mes*(1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',textDecorationLine:'underline',marginTop:10}}>Venció el dd/mm/aa</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                    <View >
                        <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>1 hora gratis por afiliación</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>24 horas posterior a la afiliación, recibe una hora (1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>GRATIS de parqueo con vigencia de un mes*(1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',textDecorationLine:'underline',marginTop:10}}>Venció el dd/mm/aa</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                    <View >
                        <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>1 hora gratis por afiliación</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>24 horas posterior a la afiliación, recibe una hora (1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>GRATIS de parqueo con vigencia de un mes*(1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',textDecorationLine:'underline',marginTop:10}}>Venció el dd/mm/aa</Text>
                    </View>
                </TouchableOpacity>   
                <TouchableOpacity style={{borderTopWidth:0,padding:15,backgroundColor: surface,borderRadius: 15, marginLeft: 0,marginRight: 1,shadowColor: "#000",shadowOffset: {width: 0,height: 1,},shadowOpacity: 0.49,shadowRadius: 4.65, elevation: 3,marginBottom:10, marginTop:10}}>
                    <View >
                        <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>1 hora gratis por afiliación</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>24 horas posterior a la afiliación, recibe una hora (1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 12,fontFamily: 'Gotham-Light',}}>GRATIS de parqueo con vigencia de un mes*(1)</Text>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',textDecorationLine:'underline',marginTop:10}}>Usó el dd/mm/aa</Text>
                    </View>
                </TouchableOpacity>   
            </View> 
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        padding:20,
    },
    form: {
        alignItems: 'flex-start',
        marginTop: 120,
    },
    title: {
        color: secondary,
        fontSize: 22,
        fontFamily: 'Gotham-Bold',
    },
    text: {
        color: secondary,
        fontSize: 15,
        fontFamily: 'Gotham-Light',
    },
    input: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 320,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
    inputPhone: {
        height: 50,
        color: colorInput,
        borderRadius: 10,
        width: 180,
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily: 'Gotham-Light',
        textDecorationLine: 'none'
    },
    titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily: 'Gotham-Bold',
    },
    button1: {
        marginTop: 90,
        height: 50,
        backgroundColor: secondary,
        padding: 14,
        marginBottom:10,
        borderRadius: 10,
        width: width-50,
        alignItems: "center",
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginVertical:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
    },
    modalView2: {
        elevation:10,
    },
    centeredView2: {
        flex: 1,
        backgroundColor:"white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: "#000",
        elevation: 15,
        marginTop:height-210,
        padding: 20,
      },
})
