import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,LogBox,Image,TouchableWithoutFeedback, Switch} from 'react-native';
import CountDown from 'react-native-countdown-component';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import QRCode from 'react-native-qrcode-svg';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500,onSurfaceOverlay8 } from '../../utils/colorVar';
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import { marginTop } from 'styled-system';
LogBox.ignoreLogs(['Non-serializable values were found in the navigation state'],["Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function."]);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import WebView from 'react-native-webview'

import Toast from 'react-native-simple-toast';
import { Request } from '../../utils/api';
import { PRODUCT } from '../../utils/endpoints';

export default function InfoWallet({route, navigation}) {
    const [isSelected, setSelection] = useState(false);
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [expanded, setExpanded] = useState(true);
    const [hour, setHour] = useState();    
    const [hourFinal, setHourFinal] = useState();
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [plate, setPlate] = useState(null);
    const [duration, setDuration] = useState(null);
    const [price, setPrice] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [qrInfo, setQrInfo] = useState(null);
    const [date, setDate] = useState(null);
    const [timeElapsed, setTimeElapsed] = useState(null);
    const [dialogReserveVisible, setDialogReserveVisible] = useState(false);
    const [isEnabled, setIsEnabled] = useState(false);
    const [dialogModalVisible, setDialogModalVisible] = useState(false);
    const [dialogModal1Visible, setDialogModal1Visible] = useState(false);
    const [service, setService] = useState(null);
    const [isEnabledWebView, setIsEnabledWebView] = useState(false); 
    const [paymentData, setPaymentData] = useState(null)
    const [amountWallet, setAmountWallet] = useState(0)
    const [isEmpty, setIsEmpty] = useState(false);
    const [listTransaction, setListTransaction] = useState([]);
    
    // const {service} = route.params;

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        //var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/20`).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            console.log(`${JSON.stringify(response.data.ResponseMessage)}`);
            if(cod === 0){
                
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    //Mostrar acordion
    const _handlePress = () => {
        setExpanded(!expanded);
    }
    //Obtener tiempo trasncurrido
    const getTime = (until) =>{
        setTimeElapsed(until);
    }
    const toggleSwitch =  () => {

        if (isEnabled == true) {
            setIsEnabled(previousState => !previousState);  
            modal()
        }else if (isEnabled == false) {
            setIsEnabled(previousState => !previousState);
            modal1() 
            
        }     
    }
    const modal = () => {
        setDialogModalVisible(true);
        console.log('modal desactivado');
    }
    const modal1 = () => {
        setDialogModal1Visible(true);
        console.log('modal activado');
    }

    ////Ir tiempo extendido
    const goToTimeExtension = (min) => {
        global.timeElapsed = timeElapsed;
        global.attachedTime = min;
        console.log(global.idReserva)
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/validate/schedule/${global.idReserva}/${min}`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setDialogReserveVisible(true);
            }else{
              Alert.alert("ERROR","No se en extender el tiempo");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }
    const goToExtension = () => {
        setDialogReserveVisible(false);
        navigation.navigate('TimeExtensionReserve');
    }
    //Metodo para traer data de la reserva
    const getDataReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/get/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                var date = moment(response.data.ResponseMessage.initial_date).format('MMMM DD YYYY');
                var timeI = moment(response.data.ResponseMessage.initial_date).format('HH:mm');
                setHour(timeI);
                var timeF = moment(response.data.ResponseMessage.final_date).format('HH:mm');
                setDuration(response.data.ResponseMessage.duration);
                setHourFinal(timeF);
                setDate(date);
                setPrice(response.data.ResponseMessage.price);
                setTypeVehicleId(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.vehicle_type_id);
                setPlate(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.plate);
            }else{
              Alert.alert("ERROR","No se en traer datos de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
        
      };

      const getTimeReserve = () =>{
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/reservation/time/left/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setTotalDuration(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo que queda de la reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }


    //Finalizar reserva
    const finishReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.put(`${API_URL}customer/app/reservation/finish/`+global.idReserva,{},config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                Alert.alert("",response.data.ResponseMessage);
                navigation.navigate('ReserveCompleted');
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo que queda de la reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }

    const makePayment = async (opc) => {

        let value = 0
        if(opc == 1 ){
            value = 15000
        }else if(opc == 2 ){
            value = 60000
        }else if(opc == 3 ){
            value = 80000
        }else if(opc == 4 ){
            value = 200000
        }else if(opc == 5 ){
            value = 150000
        }

        setSpinner(true)
        const request = new Request();
        console.log(value)
        const result = await request.request(PRODUCT.createOrderBuy,'POST',{amount:value})
        if(result === 0){
            setSpinner(false)
            Toast.showWithGravity('No es posible realizar el pago', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
            console.log(result)
        }else{
            setSpinner(false)
            console.log(`respuesta ${JSON.stringify(result.ResponseData)}`)
            // AsyncStorage.removeItem("token");
            // Toast.showWithGravity(result.ResponseMessage, Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
            // navigation.navigate('Register')

            //<form id="paymentForm" method="POST" action="https://merchant.paymentsway.co/cartaspago/redirect">
            const data = `
                <html>
                <body>
                    <form id="paymentForm" method="POST" action="https://merchantpruebas.vepay.com.co/cartaspago/redirect">
                    
                            <input name="merchant_id" type="hidden"  value="276">
                            <input name="form_id" type="hidden"  value="243">
                            <input name="terminal_id" type="hidden"  value="298">
                            <input name="order_number" type="hidden"  value="${result.ResponseData.order_buy}">
                            <input name="amount" type="hidden"  value="${value}">
                            <input name="currency" type="hidden"  value="cop">
                            <input name="order_description" type="hidden"  value="Recarga de billetera parking">
                            <input name="client_email" type="hidden" value="carlos87jaramillo@gmail.com">
                            <input name="client_phone" type="hidden" value="3175345577">
                            <input name="client_firstname" type="hidden" value="carlos">
                            <input name="client_lastname" type="hidden" value="jaramillo">
                            <input name="client_doctype" type="hidden" value="4">
                            <input name="client_numdoc" type="hidden" value="1087116281">
                            <input name="response_url" type="hidden" value="http://www.test.com/response">
                            <input name="Submit" type="submit"  value="Enviar" style="display: none;">
                    </form>
                    <script>
                    document.getElementById('paymentForm').submit();
                    </script>
                </body>
                </html>
            `

            setPaymentData(data)
            setIsEnabledWebView(true)
        }
    }

    const handleNavigation = (event) => {
        // Verificar si la solicitud es una respuesta de la URL que estás esperando
        console.log('Intento de navegación:', event.url);
        if (event.url.includes('http://www.test.com/response')) {
          // Aquí puedes manejar la respuesta como desees
        //   console.log('Respuesta del WebView:', event.url);
            setIsEnabledWebView(!isEnabledWebView);
            console.log('verificar el estado de la compra');
            setDialogReserveVisible(!dialogReserveVisible)
            getWalletAmount()
            getWalletTransaction()
          // Puedes extraer información adicional según tus necesidades
          // ...
    
          // Devuelve `false` para evitar que la URL cargue en el WebView
          return false;
        }
    
        // Devuelve `true` para permitir la navegación normal
        return true;
    };

    const toggleModal = () => {
        setIsEnabledWebView(!isEnabledWebView);
    };

    const getWalletAmount = async () => {
        setSpinner(true)
        const request = new Request();
        const result = await request.request(PRODUCT.getWalletCustomerAmount,'POST',null)
        if(result === 0){
            setSpinner(false)
            Toast.showWithGravity('No es posible obtener el saldo del monedero', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        }else{
            setSpinner(false)
            setAmountWallet(result.ResponseData)
            if(result.ResponseData === 0){
                setIsEmpty(true)
            }else{
                setIsEmpty(false)
            }
        }
    }

    const getWalletTransaction = async () => {
        setSpinner(true)
        const request = new Request();
        const result = await request.request(PRODUCT.listTransactionPerUser,'POST',null)
        if(result === 0){
            setSpinner(false)
            Toast.showWithGravity('No es posible obtener el listado de transacciones', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        }else{
            setSpinner(false)
            setListTransaction(result.ResponseData)
        }
    }

    const unidad = (cant) => {
        if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
        }else{
                return cant
        }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getWalletAmount()
            getWalletTransaction()
        });

      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate("Account")}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, padding:20, paddingTop:10,}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'left'}}>Billetera</Text>
                    {
                        isEmpty ?
                        <TouchableOpacity style={{justifyContent:'center'}} onPress={() => setDialogReserveVisible(!dialogReserveVisible)} >
                            <View
                            style={{
                                backgroundColor: surface,
                                borderRadius: 10,
                                width:width-38,
                                marginLeft: 0,
                                marginRight: 1,
                                shadowColor: "#000",
                                shadowOffset: {
                                width: 0,
                                height: 1,
                                },
                                shadowOpacity: 0.49,
                                shadowRadius: 4.65,
                                elevation: 3,
                                marginBottom:10,
                                marginTop:10,
                                padding:15,
                            }}
                            >
                                <View style={{flexDirection: 'row'}}>
                                    <View style={{alignItems:'flex-end',alignContent:'flex-start',marginLeft:0,marginTop:5}}>
                                        <MaterialIcons name="warning" size={24} color='#FFD700' />    
                                    </View>
                                    <View >
                                        <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',marginLeft:5}}>
                                            Tu billetera esta en $0, recarga para seguir disfrutando sin demoras.
                                        </Text>
                                    </View>
                                </View>
                            </View> 
                        </TouchableOpacity> : <Text></Text> 
                    }
                    <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: '#85fa05',
                            borderRadius: 10,
                            width:width-38,
                            height:130,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <View style={{alignItems:'flex-end',alignContent:'flex-start',marginLeft:0,marginTop:0, marginBottom:0}}>
                                    <MaterialIcons name="account-balance-wallet" size={24} color='#FFFFFF' />    
                                </View>
                            </View>
                            <View style={{ marginLeft: 10 }}>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:0,marginTop:0, marginBottom:0}}>
                                    <Text style={{color: '#FFFFFF',fontSize: 18,fontFamily:'Gotham-Bold',marginLeft:5}}>
                                        Saldo disponible
                                    </Text>
                                </View>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:0,marginTop:0, marginBottom:0}}>
                                    <Text style={{color: '#FFFFFF',fontSize: 28,fontFamily:'Gotham-Bold',marginLeft:5}}>
                                        ${unidad(amountWallet)}
                                    </Text>
                                </View>
                            </View>
                            <View style={{  borderRadius: 10, marginBottom: 50 }}>
                                <Image
                                    source={require("../../../assets/account/wallet-removebg.png")}
                                    style={{ width: '100%', height: '100%' }}
                                    resizeMode="contain"
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{justifyContent:'center'}} onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View
                        style={{
                            backgroundColor: '#76aa00',
                            borderRadius: 10,
                            width:width-240,
                            height:45,
                            marginLeft: width-210,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:0,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <MaterialIcons name="attach-money" size={18} color='#FFFFFF' />
                                <Text style={{color: '#FFFFFF',fontSize: 14,fontFamily:'Gotham-Bold',marginLeft:5}}>
                                    Recargar billetera
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:22, paddingTop:10,marginTop:16}}> 
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:10}}>Historial de transacciones</Text>
                    {
                        listTransaction.map( (transaction) => {
                            return <TouchableOpacity style={{justifyContent:'center'}} key={transaction.id} >
                                    <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width: width - 40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom: 10,
                                            marginTop: 10,
                                            paddingTop: 5,
                                        }}
                                    >
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <View style={{ marginLeft: 20 }}>
                                                <Text style={{ fontFamily: 'Gotham-Bold', fontSize: 14, color: '#2C2B2B' }}>
                                                    {moment(transaction.created).format("YYYY-MM-DD HH:mm:ss")}
                                                </Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 20 }}>
                                                <Text style={{ textTransform: 'uppercase', fontFamily: "Gotham-Bold", color:transaction.type === 0 ? '#F81F1F' : '#97D710', fontSize: 16, marginTop: 10 }}>
                                                    {`$${unidad(transaction.amount)}`}
                                                </Text>
                                            </View>
                                        </View>
                                        <View style={{ marginLeft: 20, marginBottom: 10 }}>
                                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary, fontSize: 14, color: '#929292' }}>
                                                {transaction.type === 1 ? "Recarga de monedero" : `Pago de servicio`}
                                            </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                        }) 
                    }
                    {/* <View style={{marginTop:20}}>
                        <Text style={styles.text}>No hay transacciones</Text>
                    </View> */}
                </View>
                {/* modal cancelacion mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModalVisible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas cancelar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal cancelacion automatica */}
                {/* modal habilitar mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModal1Visible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas habilitar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal renovación automatica */}
                <Modal
                animationType="slide"
                transparent={true}
                visible={dialogReserveVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title2}>Selecciona la cantidad a recargar</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                        <MaterialIcons name="credit-card" size={24} color='#76aa00' />
                                        <Text style={styles.text2}>Recarga mínima</Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.text3}>$15.000</Text>
                                        <TouchableOpacity style={styles.button2} onPress={() => makePayment(1)}>
                                            <Text style={styles.titleButton2}>Cargar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#ededed', marginBottom: 5, marginTop:10 }}></View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                        <MaterialIcons name="credit-card" size={24} color='#76aa00' />
                                        <Text style={styles.text2}>Recarga rápida</Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.text3}>$60.000</Text>
                                        <TouchableOpacity style={styles.button2} onPress={() => makePayment(2)}>
                                            <Text style={styles.titleButton2}>Cargar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#ededed', marginBottom: 5, marginTop:10 }}></View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                        <MaterialIcons name="credit-card" size={24} color='#76aa00' />
                                        <Text style={styles.text2}>Recarga rápida</Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.text3}>$80.000</Text>
                                        <TouchableOpacity style={styles.button2} onPress={() => makePayment(3)}>
                                            <Text style={styles.titleButton2}>Cargar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#ededed', marginBottom: 5, marginTop:10 }}></View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                        <MaterialIcons name="credit-card" size={24} color='#76aa00' />
                                        <Text style={styles.text2}>Recarga máxima</Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.text3}>$200.000</Text>
                                        <TouchableOpacity style={styles.button2} onPress={() => makePayment(4)}>
                                            <Text style={styles.titleButton2}>Cargar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#ededed', marginBottom: 5, marginTop:10 }}></View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                                        <MaterialIcons name="credit-card" size={24} color='#76aa00' />
                                        <Text style={styles.text2}>Recarga personalizada</Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <Text style={styles.text3}>Digitar monto</Text>
                                        <TouchableOpacity style={styles.button2} onPress={() => makePayment(5)}>
                                            <Text style={styles.titleButton2}>Cargar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: '#ededed', marginBottom: 5, marginTop:10 }}></View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title3}>* Por tu seguridad el monto máximo de recarga será de $ 200.000.</Text>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </ScrollView>
            <Modal
                animationType="slide"
                transparent={false}
                visible={isEnabledWebView}
                onRequestClose={toggleModal}
                style={{width:width-80,height:height-80}}
            >
                <View style={{ flex: 1,   }}>
                    <TouchableOpacity onPress={() => setIsEnabledWebView(!isEnabledWebView)}>
                        <MaterialIcons style={{marginTop:10,marginLeft:10,marginBottom:10}} name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                    <WebView
                        source={{ html: paymentData }}
                        onShouldStartLoadWithRequest={handleNavigation}
                    />
                </View>
            </Modal>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    title3: {
        color: '#76aa00',
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20,
        marginTop:10
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        // textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 16,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
        marginTop:7,
        marginLeft:5
    },
    text3: {
        color: '#000',
        fontSize: 16,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
        marginTop:7,
        marginRight:20
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign: "center",
        justifyContent:'center',
      },
      titleButton2: {
        color: '#FFFFFF',
        fontSize: 14,
        fontFamily:'Gotham-Bold',
        textAlign: "center",
        justifyContent:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-50,
      },
      button2: {
        backgroundColor: '#76aa00',
        padding: 14,
        borderRadius: 10,
        width: 100,
        height:40
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        // alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })