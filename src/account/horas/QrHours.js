import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,Image,TouchableWithoutFeedback} from 'react-native';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import CountDown from 'react-native-countdown-component';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import QRCode from 'react-native-qrcode-svg';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function QrHours ({navigation}) {

    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [nowDate, setNowDate] = useState(null);
    const [date, setDate] = useState(new Date());
    const [validity, setValidity] = useState(null);
    const [viewQR, setViewQR] = useState(false);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [showModalEntry, setShowModalEntry] = useState(false);
    const [showModalTime, setShowModalTime] = useState(false);
    const [qrValue, setQrValue] = useState('');
    const [vehicles, setVehicles] =  useState([]);
    const [contador, setContador] = useState('0');
    const [idVehicle, setIdVehicle] = useState(null);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          const passId =  await AsyncStorage.getItem("pasadiaId")
          
          console.log(passId);
          if (value !== null) {
            global.token = value;
            setToken(value);
            getParkingInfo(passId, value);
            const aux1 = await AsyncStorage.getItem("vehicleTypeId")
            const aux2 = await AsyncStorage.getItem("licensePlate")
            global.vehicle = {
                typeVehicleId: aux1,
                licensePlate: aux2,
                
                }
          }
        } catch (error) { }
      };

////Método para calcular fechas
    const dates = () => {
        const date = moment(String(new Date())).format('MMMM DD YYYY');
        setNowDate(date.toString());
        
        const dateV1 = new Date();
        dateV1.setDate(dateV1.getDate()+30);
        const dateV2 = moment(String(dateV1)).format('DD/MM/YYYY');
        setValidity(dateV2.toString());
    }


    ///Método para traer la info del parking
    const getParkingInfo = (passId, token) => {
        
        let config = {
            headers: { Authorization: token }
            };

        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/dayPass/get/`+ passId, config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                console.log(response.data.Data);
                setContador(response.data.Data.contadorPasadia);
                /* const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF); */
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
          })
    }
    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicle");
          if (value1 !== null) { 
            const value = JSON.parse(value1);
            setIdVehicle(value);
            let idVehicle = value[0].id
            console.log(`vehiclo id  -- ${JSON.stringify(idVehicle)}`);
          }

        } catch (error) {
            console.log(error);
         }
    };
    const activeVehicleParking = async () => {

        const value1 = await AsyncStorage.getItem("vehicle");
          if (value1 !== null) { 
            const value = JSON.parse(value1);
            setIdVehicle(value);
            const idVehicle = value[0].id
            console.log(`idVehicle datas -- ${JSON.stringify(idVehicle)}`);
          }
        setSpinner(true);
        axios.post(`${API_URL}customer/app/entrance/vehicle`,{
            vehicle_id : 75,
            parking_id: 58,
            manual: 0
        }).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                Alert.alert("Exitoso", response.data.ResponseMessage);
                console.log(`idVehicle entra -- ${JSON.stringify(AsyncStorage.getItem("vehicle"))}`);
                navigation.navigate('ActiveServiceHours')
            }else{
                Alert.alert("ERROR","Error al enviar la solicitud");
            }
        }).catch(error => {
                setSpinner(false);
                Alert.alert("ERROR", "No se pudo enviar la informacion, intente mas tarde");
                console.log(error);
                console.log(`idVehicle catch -- ${JSON.stringify(idVehicle)}`);
            })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          //getParkingInfo();
          //getToken();
          //dates();
          getDataStorage();
        });
        var date = new Date();
        var expirydate = new Date().setHours(24,0,0);
        var diffr = moment.duration(moment(expirydate).diff(moment(date)));

        var hours = parseInt(diffr.asHours());
        var minutes = parseInt(diffr.minutes());
        var seconds = parseInt(diffr.seconds());
        
        var d = hours * 60 * 60 + minutes * 60 + seconds;
        setTotalDuration(d);
        const newDuration = d;
        var variable = 1;
        setInterval(()=> {
                    variable = variable - 1/newDuration;
                    setProgress(variable);
                }, 1000);
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('HomeHours')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, paddingTop:10,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'center'}}>Dirigete al parqueadero</Text>
                    <View style={{alignItems:'center'}}>
                        <Image
                        source={require("../../../assets/register/ilustracion1.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <View style={{paddingHorizontal:20}}>
                        <Text style={{color: secondary,fontSize: 15,fontFamily: 'Gotham-Medium',textAlign:'center'}}>Presenta este codigo a la entrada del punto del servicio, una vez valide tu servicio entrara en vigencia </Text>
                    </View>
                    <View style={{marginTop:20,alignContent:'center',alignItems:'center',justifyContent:'center',marginBottom:10}}>
                        
                        <View> 
                            <TouchableOpacity onPress={() => activeVehicleParking()}>
                                <View style={{alignItems:'center'}}>
                                    <QRCode
                                        value={ qrValue ? qrValue : 'No se pudo generar el código QR' }
                                        size={ 180 }
                                        color= 'white'
                                        backgroundColor='#000'                                
                                        >
                                    </QRCode>
                                </View> 
                            </TouchableOpacity>
                            
                            <View
                                style={{
                                    backgroundColor: surface,
                                    borderRadius: 10,
                                    width:width-38,
                                    marginLeft: 0,
                                    marginRight: 1,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                    width: 0,
                                    height: 1,
                                    },
                                    shadowOpacity: 0.49,
                                    shadowRadius: 4.65,
                                    elevation: 3,
                                    marginBottom:10,
                                    marginTop:10,
                                    padding:10,
                                }}
                                >
                            </View>
                        </View>
                    </View>

                </View>
                {/* parqueadero */}
                <Text style={{fontFamily:'Gotham-Medium', color:'#005A6D', fontSize:18, marginTop:30, marginLeft:30}}>Parqueadero asociado</Text>
                <TouchableOpacity style={{left:20}}>
                    <View
                        style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                        }}
                        >
                        <View style={{flexDirection: 'row'}}>
                            <View >
                                <Text style={{ fontFamily:'Gotham-Bold',fontSize:24, color:secondary }}>Parqueadero Prueba</Text>
                                <Text style={{ fontFamily:'Gotham-Bold',fontSize:16, color:'#93D500' }}>Abierto</Text>
                               {/*  <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                    {
                                        parking !== null ?
                                        parking.name
                                        :
                                        ""
                                    }
                                </Text>
                                {
                                    parking !== null ?
                                        parking.address !== null ? 
                                            <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    :
                                    <Text style={styles.text}>No hay dirección disponible</Text>
                                } */}
                                {/* <View style={{flexDirection: 'row'}} >
                                    {
                                        parking !== null &&
                                            parking.open == 1 ? 
                                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                            :
                                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                    }    
                                        
                                    {
                                        parking !== null ?
                                            parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay horario disponible</Text>
                                    }
                                </View> */}
                            </View>
                            <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                            </View>
                        </View>
                    </View> 
                </TouchableOpacity>

                                
            </ScrollView>
            <Modal
                animationType="slide"
                transparent={true}
                visible={showModalEntry}
            >
                <TouchableWithoutFeedback onPress={() => setShowModalEntry(!showModalEntry)}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <View style={{marginTop:-10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                <TouchableOpacity onPress={() => setShowModalEntry(!showModalEntry)}>
                                    <MaterialIcons name="close" size={25} color={secondary} />
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.text2}>Haz cumplido tus 10 ingreso a X parqueadero ya no podras volver a ingresar de nuevo. </Text>
                            <View style={{marginBottom:10}}>
                                <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}}>
                                    <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>RENOVA PASADÍA</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View style={{marginTop:-10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                            <TouchableOpacity onPress={() => setShowModalEntry(!showModalEntry)}>
                                <MaterialIcons name="close" size={25} color={secondary} />
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.text2}>Haz cumplido tus 10 ingreso a X parqueadero ya no podras volver a ingresar de nuevo. </Text>
                        <View style={{marginBottom:10}}>
                            <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}}>
                                <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>RENOVA PASADÍA</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={showModalTime}
            >
                <TouchableWithoutFeedback onPress={() => setShowModalTime(!showModalTime)}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.text2}>Tu pasadía ha finalizado porque ha llegado a su limite de tiempo. </Text>
                            <View style={{marginBottom:10}}>
                                <TouchableOpacity style={{padding:14}} onPress={() => navigation.navigate('PasadiaCompleted')}>
                                    <Text style={{fontFamily:'Gotham-Medium',color: surfaceMediumEmphasis,fontSize: 14,textAlign:'center'}}>CONSULTAR DETALLE</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{marginBottom:10}}>
                                <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => navigation.navigate('Pasadia')}>
                                    <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ADQUIRIR NUEVA PASADÍA</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
        padding:20
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 16,
        fontFamily: 'Gotham-Medium',
        marginBottom:50,
        marginTop:20,
        alignItems:'center',
        textAlign:'center'
    },
    text3: {
        flex: 1,
        backgroundColor: surface,
        padding:5,
        fontSize:15,
        marginTop:16,
        marginBottom: 16,
        textAlign: 'center',
        fontFamily:'Gotham-Light',
        fontStyle: 'italic'
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        marginLeft:10,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-100,
        flexDirection:'row',
        alignItems: "center",
        alignContent:'center',
        justifyContent:'center',
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })