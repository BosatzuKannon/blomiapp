import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View,Image, TouchableOpacity,ScrollView,CheckBox,Modal,Dimensions, TouchableWithoutFeedback, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faQrcode} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
const moment = require('moment');
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../../utils/api';
import { FACILITY } from '../../utils/endpoints';

export default function HomeHours({ navigation, route }) {


    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [id,setId] = useState(null);
    const [open, setOpen] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);

    const {parkingId} = route.params;

    //Método para traer la info del parking
    const getParkingInfo = async () => {
        setSpinner(true)
        
        const data = {id:parkingId}
        const request = new Request();
        const result = await request.request(FACILITY.infoFacility,'POST',data)
        if(result === 0){
            setSpinner(false)
            Alert.alert('Error',"No es posible consultar la información del parqueadero");                      
        }else{
            setSpinner(false)
            setParking(result.ResponseData)
            checkOpening(result.ResponseData)
            
        }
    }

    const checkOpening = async (parking) => {
        const currentDate = moment();
        const dayOfWeekNumber = currentDate.day();

        await parking.schedule.standard.map((day)=>{
            if(day.day == dayOfWeekNumber){
                setOpenHour(day.open_hours)
                setCloseHour(day.close_hours)

                const now = moment();

                const horaAperturaMoment = moment(day.open_hours, 'HH:mm', true);
                const horaCierreMoment = moment(day.close_hours, 'HH:mm', true);

                if (now.isBetween(horaAperturaMoment, horaCierreMoment)) {
                    setOpen(1)
                }
            }
        })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
        //   const { params } = route;
        //   const parkingId = params ? params.parkingId : null;
    
        //   setId(parkingId)
          getParkingInfo()
        });
        return unsubscribe;
      }, [navigation, route]);

    


    return (
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                {/* parqueadero */}
                <TouchableOpacity style={{right:10}}>
                    <View
                        style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                        }}
                        >
                        <View style={{flexDirection: 'row'}}>
                            <View >
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                    {
                                        parking !== null ?
                                        parking.name
                                        :
                                        ""
                                    }
                                </Text>
                                {
                                    parking !== null ?
                                        parking.address !== null ? 
                                            <Text style={styles.text}> {parking.address} </Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    :
                                    <Text style={styles.text}>No hay dirección disponible</Text>
                                }
                                <View style={{flexDirection: 'row'}} >
                                    {
                                        parking !== null &&
                                            open == 1 ? 
                                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                            :
                                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                    }    
                                        
                                    {
                                        parking !== null ?
                                            parking.shedule !== ""  ? 
                                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay horario disponible</Text>
                                    }
                                </View>
                            </View>
                            <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                            </View>
                        </View>
                    </View> 
                </TouchableOpacity>

                {/* servicios */}
                <TouchableOpacity onPress={() => navigation.navigate('ServiceHours', {'parking' : parking})}>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        > 
                        <View style={{flexDirection:'row'}}>
                            <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                            <View>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Ingresa por horas</Text>
                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                            </View>
                            <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('Courtesy')}>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        > 
                        <View style={{flexDirection:'row'}}>
                            <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                            <View>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Cortesía</Text>
                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                            </View>
                            <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        > 
                        <View style={{flexDirection:'row'}}>
                            <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                            <View>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Mensualidades</Text>
                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                            </View>
                            <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('CodeVipGold')}>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        > 
                        <View style={{flexDirection:'row'}}>
                            <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                            <View>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>VIP</Text>
                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                            </View>
                            <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                        </View>
                    </View>
                </TouchableOpacity>


                {/* <Text style={{fontFamily:'Gotham-Medium', color:'#002D33', fontSize:24, marginTop:10}}>VIP</Text>
                <Text style={{marginTop: 10, fontFamily:'Gotham-Medium', color:'#002D33'}}>Descripción opcional para mensualidades</Text>
                
                <Text style={{color:'#68AF00', fontFamily:'Gotham-Medium', fontSize:18, marginTop:30}}>Nombre plan destacado</Text>
                <View style={{flexDirection: 'row', marginTop:20}}>
                    <Text style={{color:'#002D33', fontFamily:'Gotham-Medium', fontSize:18}}>$100.000 cop</Text>
                    <Text style={{color:'#002D33', fontFamily:'Gotham-Medium', fontSize:18, marginLeft:'auto', marginRight:40}}>% descuento</Text>
                </View> */}
        
            </View>
        </ScrollView>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:20,
      },
      button1: {
        backgroundColor:'#005A6D',
        width: 280,
        marginLeft:'auto',
        marginRight:'auto',
        height: 40,
        justifyContent: 'center',
        borderRadius:10 
    },
})