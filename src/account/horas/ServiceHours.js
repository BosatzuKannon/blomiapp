import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View,Image, TouchableOpacity,ScrollView,CheckBox,Modal,Dimensions, TouchableWithoutFeedback, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faQrcode} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
const moment = require('moment');
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ServiceHours ({navigation, route}){
    const [spinner,setSpinner] = useState(false);
    // const [parking,setParking] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [open, setOpen] = useState(null);

    const   { parking } = route.params

    const checkOpening = async () => {
        const currentDate = moment();
        const dayOfWeekNumber = currentDate.day();

        await parking.schedule.standard.map((day)=>{
            if(day.day == dayOfWeekNumber){
                setOpenHour(day.open_hours)
                setCloseHour(day.close_hours)

                const now = moment();

                const horaAperturaMoment = moment(day.open_hours, 'HH:mm', true);
                const horaCierreMoment = moment(day.close_hours, 'HH:mm', true);

                if (now.isBetween(horaAperturaMoment, horaCierreMoment)) {
                    setOpen(1)
                }
            }
        })
    }

    //Método para traer la info del parking
        //Método para traer la info del parking
        const getParkingInfo = () => {
        
            setSpinner(true);
            var id = parseInt(global.parkingId);
            axios.get(`${API_URL}/customer/app/parking/getParkingInto/58`).then(response => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                if(cod === 0){
                    //setParking(response.data.ResponseMessage);
                    /* const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                    const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                    setOpenHour(hourO);
                    setCloseHour(hourF);  */
                    console.log(`info parking-- ${JSON.stringify(response.data.ResponseMessage)}`);
                }else{
                  Alert.alert("ERROR","No se pudo traer el parqueadero");
                }
              }).catch(error => {
                setSpinner(false);
                Alert.alert("",error.response.data.ResponseMessage);
                console.log(error.response.data.ResponseMessage);
              })
        }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            checkOpening()
        });
        return unsubscribe;
    }, [navigation]);
    return (
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('HomeHours')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                {/* parqueadero */}
                    <TouchableOpacity style={{right:10}}>
                        <View
                            style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                            }}
                            >
                            <View style={{flexDirection: 'row'}}>
                                <View >
                                    {/* <Text style={{ fontFamily:'Gotham-Bold',fontSize:24, color:secondary }}>{parking.name}</Text>
                                    <Text style={{ fontFamily:'Gotham-Bold',fontSize:16, color:'#93D500' }}>Abierto</Text> */}
                                 <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                        {
                                            parking !== null ?
                                            parking.name
                                            :
                                            ""
                                        }
                                    </Text>
                                    {
                                        parking !== null ?
                                            parking.address !== null ? 
                                                <Text style={styles.text}> {parking.address} </Text>
                                            :
                                            <Text style={styles.text}>No hay dirección disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    } 
                                     <View style={{flexDirection: 'row'}} >
                                        {
                                            parking !== null &&
                                                open == 1 ? 
                                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                                :
                                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                        }    
                                            
                                        {
                                            parking !== null ?
                                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                                :
                                                <Text style={styles.text}>No hay horario disponible</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        }
                                    </View>
                                </View>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                    <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                                </View>
                            </View>
                        </View> 
                    </TouchableOpacity>
                <Text style={{fontFamily:'Gotham-Medium', color:'#002D33', fontSize:24, marginTop:10}}>Bienvenido a horas</Text>
                <Text style={{marginTop: 10, fontFamily:'Gotham-Medium', color:'rgba(0, 0, 0, 0.6)'}}>Descripción opcional para mensualidades</Text>

                <Text style={{fontFamily:'Gotham-Medium', color:'#002D33', fontSize:18, marginTop:30, marginBottom:20}}>Tus beneficios</Text>

                {/* Beneficios*/}
                <TouchableOpacity>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        > 
                        <View style={{flexDirection:'row'}}>
                            <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                            <View>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Accede facil y rapido</Text>
                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        > 
                        <View style={{flexDirection:'row'}}>
                            <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                            <View>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Ver el tiempo actual del servicio</Text>
                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        > 
                        <View style={{flexDirection:'row'}}>
                            <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                            <View>
                                <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Pago en linea desde nuestra App </Text>
                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>

                {/* <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 30, marginTop:30}}>
                    <TouchableOpacity style={styles.button1} onPress={() => navigation.navigate('ListVehiclesHours')}>
                        <View>
                            <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold', color:'#F2FAE0', textAlign:'center'}}>
                                CONTINUAR
                            </Text>
                        </View>        
                    </TouchableOpacity>
                </View>  */}
            </View>
        </ScrollView>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:20,
        height:height
      },
      button1: {
        backgroundColor:'#005A6D',
        width: 300,
        marginLeft:'auto',
        marginRight:'auto',
        height: 40,
        justifyContent: 'center',
        borderRadius:10 
    },
})