import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View,Image, TouchableOpacity,ScrollView,CheckBox,Modal,Dimensions, TouchableWithoutFeedback, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faQrcode} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import moment from "moment";
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function finalHours ({navigation}){
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [nowDate, setNowDate] = useState(null);
    const [validity, setValidity] = useState(null);

    //Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/20`).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF); 
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    },
    goToEndService = () => {
        alert('Servicio Finalizado')
        navigation.navigate('Home')
    }
    const dates = () => {
        const date = moment(String(new Date())).format('MMMM DD YYYY');
        setNowDate(date.toString());
        
        const dateV1 = new Date();
        dateV1.setDate(dateV1.getDate()+30);
        const dateV2 = moment(String(dateV1)).format('DD/MM/YYYY');
        setValidity(dateV2.toString());
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          //getParkingInfo();
          dates();
        });
        return unsubscribe;
    }, [navigation]);
    return (
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('ActiveServiceHours')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <Text style={{fontFamily:'Gotham-Bold', color:secondary, fontSize:24, textAlign:'center'}}>Servicio finalizado</Text>
                <View style={{marginLeft:'auto', marginRight:'auto'}}>
                    <Image
                    source={require("../../../assets/reserve/ilustracion_valet.png")}
                    style={{ marginVertical:20 }}></Image>
                </View>

                <View>  
                    <Text style={{fontFamily:'Gotham-Bold',color: secondary, fontSize:22}}> Realiza el pago de tu servicio</Text>  
                    <Text style={{fontFamily:'Gotham-Medium',color: secondary, fontSize:16, textAlign:'center',marginRight:10}}>Ahora puedes realizar el pago de tus servicios por medio de nuestra App de parking o dirigite 
                        a nuestros puntos de pago fisicos
                    </Text>
                </View>

                 {/* parqueadero */}
                 <TouchableOpacity style={{left:5}}>
                    <View
                        style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                        }}
                        >
                        <View style={{flexDirection: 'row'}}>
                            <View >
                                <Text style={{ fontFamily:'Gotham-Bold',fontSize:24, color:secondary }}>Parqueadero Prueba</Text>
                                <Text style={{ fontFamily:'Gotham-Bold',fontSize:16, color:'#93D500' }}>Abierto</Text>
                               {/*  <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                    {
                                        parking !== null ?
                                        parking.name
                                        :
                                        ""
                                    }
                                </Text>
                                {
                                    parking !== null ?
                                        parking.address !== null ? 
                                            <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    :
                                    <Text style={styles.text}>No hay dirección disponible</Text>
                                } */}
                                {/* <View style={{flexDirection: 'row'}} >
                                    {
                                        parking !== null &&
                                            parking.open == 1 ? 
                                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                            :
                                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                    }    
                                        
                                    {
                                        parking !== null ?
                                            parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay horario disponible</Text>
                                    }
                                </View> */}
                            </View>
                            <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                            </View>
                        </View>
                    </View> 
                </TouchableOpacity>

                <Text style={{fontFamily:'Gotham-Medium', color:'#005A6D', fontSize:24, marginTop:10}}>Detalles</Text>
                <View style={{flexDirection: 'row', marginTop:20}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14}}>Fecha</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14, marginLeft:'auto', marginRight:30}}>Tiempo trasncurrido</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:10}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16}}>{nowDate}</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16, marginLeft:'auto', marginRight:30}}>30 minutos</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:20}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14}}>Hora Inicial</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14, marginLeft:'auto', marginRight:30}}>Hora Final</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:10}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16}}>10:00</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16, marginLeft:'auto', marginRight:30}}>10:30</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:20}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14}}>Tipo de vehiculo</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14, marginLeft:'auto', marginRight:30}}>Placas</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:10}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16}}>Automovil</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16, marginLeft:'auto', marginRight:30}}>ZZZ689</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:20}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14}}>Costo Total</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Medium', fontSize:14, marginLeft:'auto', marginRight:30}}>Descuentos</Text>
                </View>
                <View style={{flexDirection: 'row', marginTop:10}}>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16}}>$15.000</Text>
                    <Text style={{color:secondary,fontFamily:'Gotham-Bold', fontSize:16, marginLeft:'auto', marginRight:30}}>$5.000</Text>
                </View>
               
                <TouchableOpacity onPress={() => navigation.navigate('PaymentMethods')} style={{marginTop:20}}>
                    <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 2,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        >
                        <View style={{flexDirection: 'row'}}>
                            <Ionicons name="card-outline" size={20} color={secondary} style={{marginLeft:10}}/>
                            <View style={{marginLeft:10}}>
                                <Text style={{color: '#002D33',fontSize: 16,fontFamily:'Gotham-Bold',}}>Medios de pago electrónicos</Text>
                            </View>
                            <MaterialIcons name="navigate-next" size={24} color={secondary} style={{marginLeft:'auto'}} />
                        </View>
                    </View>
                </TouchableOpacity>
                {/* boton accion */}
                <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 30, marginTop:30}}>
                    <TouchableOpacity style={styles.button1} onPress={() => goToEndService()}>
                        <View>
                            <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold', color:'#F2FAE0', textAlign:'center'}}>
                                FINALIZAR SERVICIO
                            </Text>
                        </View>        
                    </TouchableOpacity>
                </View> 
            </View>
        </ScrollView>               

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:20,
      },
      button1: {
        backgroundColor:'#005A6D',
        width: 280,
        marginLeft:'auto',
        marginRight:'auto',
        height: 40,
        justifyContent: 'center',
        borderRadius:10 
    },
})