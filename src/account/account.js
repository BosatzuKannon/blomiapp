import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert, Image} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,secondary,
        surface, colorGray, colorGrayOpacity, secondary500,
        colorInput, colorInputBorder, colorPrimaryLigth, 
        primary600,primary50,primary500,primary900 } from '../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import Toast from 'react-native-simple-toast';
import { Request } from '../utils/api';
import { AUTH } from '../utils/endpoints';

export default function Account({navigation }) {
const [spinner,setSpinner] = useState(false);
const [personalInfo, setPersonalInfo] = useState(false);
const [payBilling, setPayBilling] = useState(false);
const [benefits, setBenefits] = useState(false);
  
const logout = async () => {
    setSpinner(true)
    const request = new Request();
    const result = await request.request(AUTH.logout,'POST',null)
    if(result === 0){
        setSpinner(false)
        Alert.alert("Error al cerrar sesión");
        console.log(result)
    }else{
        setSpinner(false)
        AsyncStorage.removeItem("token");
        Toast.showWithGravity(result.ResponseMessage, Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        navigation.navigate('Register')
    }
}

const userBeParking  = () => {

    navigation.navigate('BeParking')
    // navigation.navigate('BeParkingBlue')
    // navigation.navigate('FormBeParking')
}

const back = () =>{
    setPersonalInfo(false);
    setPayBilling(false);
    setBenefits(false);
}

useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
    //   getToken();
    });
    return unsubscribe;
  }, [navigation]);


    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <View style={{ padding:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                {
                    personalInfo || payBilling || benefits ?
                    <TouchableOpacity onPress={back}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                    :
                    <View></View>
                }
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Mi cuenta</Text>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            {
                personalInfo && 
                    <View>
                        <Text style={{color: '#000',fontSize: 14,fontFamily: 'Gotham-Light',padding:20}}>Información personal</Text>
                        <View style={styles.container}>
                            <TouchableOpacity>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Mis datos</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Información personal</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('ListVehicleMonthly', {'parking' : [], 'monthly':[]})}>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Mis vehículos</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Información de mis placas</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
            }
            {
                payBilling && 
                    <View >
                        <Text style={{color: '#000',fontSize: 14,fontFamily: 'Gotham-Light',padding:20}}>Billetera</Text>
                        <View style={styles.container}>
                            <TouchableOpacity>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Historial de pagos</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
            }
            {
                benefits && 
                    <View >
                        <Text style={{color: '#000',fontSize: 14,fontFamily: 'Gotham-Light',padding:20}}>Beneficios</Text>
                        <View style={styles.container}>
                            <TouchableOpacity>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Bonos</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            {/* <TouchableOpacity>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>VIP</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity> */}
                        </View>
                    </View>
            }
            {
                personalInfo || payBilling || benefits ?
                <View></View>
                :
                    <View>
                        <View style={{paddingHorizontal:20,paddingTop:20}}>
                            <TouchableOpacity onPress={() => userBeParking()}> 
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 25,
                                        width:width-200,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <Image source={require('../../assets/account/logoBeParking.png')}width={20} height={20} style={{marginRight:10}}></Image>
                                        <View>
                                            <Text style={{color: surfaceHighEmphasis,fontSize: 14,fontFamily: 'Gotham-Light',}}>Be Parking</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.container}>
                            <TouchableOpacity onPress={() => setPersonalInfo(true)}>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Mi Información personal</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Datos tuyos y del vehículo</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            {/* <TouchableOpacity onPress={() => setPayBilling(true)}> */}
                            <TouchableOpacity onPress={() => navigation.navigate('InfoWallet')}>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Billetera</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Medios de pago e historial</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => setBenefits(true)}>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={secondary} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: secondary,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Beneficios</Text>
                                            <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={secondary} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{paddingHorizontal:20,paddingTop:20}}>
                            <Text style={{color: '#000',fontSize: 14,fontFamily: 'Gotham-Light',}}>Servicios</Text>
                        </View>
                        <View style={styles.container2}>
                            <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={primary500} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: primary900,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Mensualidades</Text>
                                            <Text style={{color: secondary500,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={primary500} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('Courtesy')}>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={primary500} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: primary900,fontSize: 16,fontFamily: 'Gotham-Medium',}}>Cortesías</Text>
                                            <Text style={{color: secondary500,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={primary500} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={ () => navigation.navigate('CodeVipGold')}>
                                <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 2,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    > 
                                    <View  style={{flexDirection:'row'}}>
                                        <MaterialCommunityIcons name="heart-outline" size={24} color={primary500} style={{marginTop:10,marginRight:20}}/>
                                        <View>
                                            <Text style={{color: primary900,fontSize: 16,fontFamily: 'Gotham-Medium',}} >Vip</Text>
                                            <Text style={{color: secondary500,fontSize: 14,fontFamily: 'Gotham-Light',}}>Descripción</Text>
                                        </View>
                                        <MaterialIcons name="arrow-forward-ios" size={18} color={primary500} style={{marginTop:15,justifyContent:'flex-end',marginLeft:'auto'}}/>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <TouchableOpacity style={styles.button3}  onPress={logout}>
                                <Text style={styles.titleButton3}>CERRAR SESIÓN</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
            }
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      marginTop:10,
    //   height:height,
    },
    container2: {
        flex: 1,
        backgroundColor: primary50,
        paddingHorizontal:20,
        paddingVertical:10,
        marginTop:10,
        height:height-500,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 15,
        borderRadius: 12,
        width: width-30,
        marginTop:10,
        alignItems: "center",
      },
      button3: {
        height:50, 
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })