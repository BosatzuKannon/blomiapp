import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity,TextInput,KeyboardAvoidingView,ScrollView, Alert,Dimensions} from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
// import { faGooglePlusSquare,faFacebookF} from '@fortawesome/free-brands-svg-icons';
import {faEye,faEyeSlash} from '@fortawesome/free-regular-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { primary800,colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600,surfaceMediumEmphasis } from './utils/colorVar';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { TextInput as PaperTextInput } from 'react-native-paper';
import { Ionicons,MaterialIcons } from '@expo/vector-icons';

import Toast from 'react-native-simple-toast';
import { Request } from './utils/api';
import { AUTH } from './utils/endpoints';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default function Login ({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [hidePass, setHidePass] = useState(true);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    const login = async () => {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
      if (email === null || email === ''){
        // Alert.alert("","El correo es obligatorio.");
        Toast.showWithGravity("El correo es obligatorio.", Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
      }else if(reg.test(email) === false){
              // Alert.alert("Correo inválido","Escriba un correo válido por favor.");
              Toast.showWithGravity("Correo inválido","Escriba un correo válido por favor.", Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
      }else if(password === null || password === ''){
              // Alert.alert("","La contraseña es obligatoria.");
              Toast.showWithGravity("La contraseña es obligatoria.", Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
      }else {
        setSpinner(true)
        const request = new Request();
        const result = await request.request(AUTH.login,'POST',{email:email, password:password})
        if(result === 0){
          setSpinner(false)
          // Alert.alert("Error","Credenciales incorrectas");                    
          Toast.showWithGravity("Credenciales incorrectas", Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        }else{
          setSpinner(false)
          // Alert.alert(result.ResponseMessage)
          Toast.showWithGravity(result.ResponseMessage, Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
          const resp = await saveToken(result.ResponseData.tokenEnc.accessToken)
          if(resp){
            const customer =  result.ResponseData.customer
            if(customer.change_password){
              navigation.navigate('FormRegister',{completeProfile:customer.complete_profile,pass:password, email:email});
            }else{
              navigation.navigate("BarNavigationRegister",{register:true});
            }
          }else{
            // Alert.alert("Error","Credenciales incorrectas"); 
            Toast.showWithGravity("Credenciales incorrectas", Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
          }
        }
      }
    }
    
    const saveToken = async (token) => {
      try {
        await AsyncStorage.setItem("token", token);
        axios.defaults.headers.common['Authorization'] = token;
        return true
      } catch (e) {
        return false
      }
    };

    useEffect(() => {
      // const getToken = async () => {
      //   try {
      //     const token = await AsyncStorage.getItem("token");
      //     if (token) {
      //       navigation.navigate("BarNavigationRegister",{register:true});
      //     }
      //   } catch (error) {
      //     console.error("Error al recuperar el token:", error);
      //   }
      // };
      // getToken();
    }, []);

    return(
      <KeyboardAvoidingView style={styles.container}>
         <ScrollView  showsVerticalScrollIndicator={false}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{marginLeft:5,marginTop:10}}>
              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                <MaterialIcons name="close" size={25} color={secondary} />
              </TouchableOpacity>
            </View>
            <View style={{alignItems:'center'}}>
                <Image
                    source={require("../assets/register/logoGreen.png")}
                    style={{ marginTop: 40,marginBottom:40 }}
                ></Image>
                <Image
                    source={require("../assets/register/ilustracion1.png")}
                    style={{ marginBottom:30 }}
                ></Image>
                <Text style={styles.title}>Inicia sesión</Text>
                <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,}}  onChangeText={text => {setEmail(text)}}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Correo electrónico" outlineColor={colorInputBorder}/>
                <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:20,fontSize:14}} secureTextEntry={hidePass} onChangeText={(text) => {setPassword(text)}} 
                        right={<PaperTextInput.Icon 
                          name={() => <Ionicons name={hidePass ? "eye" : "eye-off" } size={24} color={surfaceMediumEmphasis} onPress={() => setHidePass(!hidePass)} />} />}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Contraseña" outlineColor={colorInputBorder}/>
                        
                <TouchableOpacity style={styles.button1} onPress={login}>
                    <Text style={styles.titleButton1}>INICIAR SESIÓN</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={{ fontSize: 15, marginTop: 20,fontFamily:'Gotham-Bold', color:primary800}}>RESTABLECER CONTRASEÑA</Text>
                </TouchableOpacity>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
    );

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: surface,
    },
    title: {
        color: secondary,
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    input:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      inputPassword:{
        height:50,  
        color: colorInput, 
        borderRadius:10,
        width:320,
        marginTop:10,
        marginBottom:10,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none',
        flexDirection:'row',
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        height:50,
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width:width-50,
        alignItems: "center",
      },
      button3: {
        height:50, 
        backgroundColor: surface,
        padding: 14,
        borderRadius: 10,
        width:width-50,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
  })
