export const AUTH = {
    register: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/customer/register",
    login: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/customer/login",
    completeProfile: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/customer/completeProfile",
    changePassword: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/customer/changePassword",
    infoCustomer: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/infoCustomerMobile",
    logout: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/customer/logout"
}

export const FACILITY = {
    listGeneral: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/facility/listFacilityMovile",
    listFacilityPerProductMovile: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/facility/listFacilityPerProductMovile",
    infoFacility: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/facility/infoFacilityMovile"
}

export const PRODUCT = {
    listProduct: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/listProductMovile",
    createProduct: 'https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/saveMonthlyNaturalCustomer',
    listProductPerUser: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/listProductPerUser",
    saveHourProduct: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/createFacilityParking",
    checkBeneficiaryCode:"https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/verifyCode",
    activateBeneficiaryCode: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/ActivateCodeBeneficiary",
    infoCustomerProduct: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/infoCustomerProduct",
    createOrderBuy: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/app/wallet/createOrderBuy",
    getWalletCustomerAmount: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/customer/app/wallet/getWalletCustomerAmount",
    listTransactionPerUser: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/productos/app/wallet/listTransactionPerUser"
}

export const VEHICLE = {
    listVehiclePerCustomer: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/vehicles/listVehiclesPerCustomer",
    registerVehicle: "https://xwjphnfan2.execute-api.us-east-1.amazonaws.com/dev/vehicles/registerVehicle",
}