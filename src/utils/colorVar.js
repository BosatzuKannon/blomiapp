export const primary900 = '#1C7A00'
export const primary800 = '#519B00';
export const colorPrimaryLigth= '#BAE65B';
export const primary700 = '#68AF00';
export const primary600 = '#80C300';
export const primary500 = '#90D400';
export const primary400 = '#A1DA26';
export const primary300 = '#B6E26A';
export const primary50 = '#F2FAE0';
export const colorPrimarySelect = 'rgba(172, 224, 61, 0.5)';
export const secondary = '#005A6D';
export const secondary500 = '#387881';
export const secondary600 = '#2D6971';
export const secondary50 = '#D8F2F8';
export const colorGray = '#f4f4f4';
export const colorGrayOpacity = 'rgba(0, 45, 51, 0.05)';
export const onSurfaceOverlay8 = 'rgba(0, 45, 51, 0.08)';
export const colorInput = 'rgba(0, 45, 51, 0.5)';
export const colorInputBorder = 'rgba(0, 45, 51, 0.3)';
export const surface = '#ffffff';
export const surfaceHighEmphasis = 'rgba(0, 45, 51, 0.8)';
export const surfaceMediumEmphasis = 'rgba(0, 45, 51, 0.6)';
export const surfaseDisabled = 'rgba(0, 45, 51, 0.4)';
export const OnSurfaceOverlay15 = 'rgba(0, 45, 51, 0.15)';
export const OnSurfaceDisabled = 'rgba(0, 45, 51, 0.15)';
export const OnSecondary = '#F2FAE0';
export const errorColor = '#D40059';
export const statesPrimaryOverlaySelected = 'rgba(145, 212, 0, 0.08)';
export const errorLight = 'rgba(212, 0, 89, 0.1)';



