import { KJUR } from 'jsrsasign';

const decodedRequest = (info) => {
    try {
        return KJUR.jws.JWS.parse(info).payloadObj;
    } catch (error) {
        console.log(`Error: ${error}`);
        return 0;
    }
}

const encodeRequest = (info) => {
    try {
        const header = {alg: 'HS256', typ: 'JWT'};
        
        const jwt = KJUR.jws.JWS.sign(header.alg, JSON.stringify(header), JSON.stringify(info), '498vyn4onvwiehnlezs98umo9umcm94slc84nhtla8lo9l8vynch9l8c4e');
        return jwt;
    } catch (error) {  
        console.log(error);      
        return 0;
    }
}

export { decodedRequest, encodeRequest };