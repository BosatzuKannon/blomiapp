import axios from 'axios';
import { encodeRequest, decodedRequest } from './jwt';
import AsyncStorage from "@react-native-async-storage/async-storage";

export class Request {
  async request(endpoint, method, data) {
    try {
      let config = {
        method,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      if (data !== '') {
        const encodedData = await encodeRequest(data);
        config.data = { requestInfo: encodedData };
      }

      const token = await AsyncStorage.getItem("token");
      if (token) {
        axios.defaults.headers.common['Authorization'] = token;
      }

      console.log(`data que se va ${JSON.stringify(config.data)}`)
      const response = await axios(`${endpoint}`, config);
      const result = await decodedRequest(JSON.stringify(response.data.info));
      // console.log(`respuesta ${JSON.stringify(result)}`)
      
      return result;
    } catch (error) {
      console.log(error);
      return 0;
    }
  }
}