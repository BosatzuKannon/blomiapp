import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image,TouchableWithoutFeedback} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50,errorColor } from '../../utils/colorVar';
import { TextInput as PaperTextInput, HelperText} from 'react-native-paper';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import Checkbox from 'expo-checkbox';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default function CodeEvent ({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [codeInvalid, setCodeInvalid] = useState(false);
    const [code, setCode] = useState(null);

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }
     const codeValid = () => {
        if (code == '123456') {
            alert('codigo válido')
            navigation.navigate('DetailsEvent')
            } else  if (code == '654321') {
            alert('código no autorizado')
        } else {
            alert('código Incorrecto')
        }
     }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
        });
        return unsubscribe;
    }, [navigation]);

    return  (
        <View>
            <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,paddingBottom:10,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}> 
                    <Text style={{color:secondary, fontSize:24, fontFamily:'Gotham-Bold', textAlign: 'center', marginTop:20}}>Bienvenido a eventos</Text>
                    <Image source={require("../../../assets/reserve/ilustracion_valet.png")}
                        style={{marginTop:20, marginLeft:'auto', marginRight:'auto'}}
                    />
                <Text style={{color:secondary, fontSize:24, fontFamily:'Gotham-Bold', marginTop:20}}>Ingrese el código</Text>
                <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:20}}>Ingresa el código único de tu cupo de parqueadero para este evento</Text>
                <View>
                    <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,fontSize:14,marginTop:20}} 
                        onChangeText={(text) => {setCode(text)}} 
                        keyboardType='text' maxLength={6}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Código" outlineColor={codeInvalid ? errorColor : colorInputBorder}/>
                    <TouchableOpacity style={{marginBottom:20, borderWidth: 1, borderColor:secondary, width:200, height:40, borderRadius: 10, justifyContent: 'center', marginLeft:'auto', marginRight:'auto', marginTop:40}} onPress={codeValid} >
                        <Text style={{color: secondary,fontSize: 14,fontFamily:'Gotham-Medium',marginLeft:3,textAlign:'center'}}> VALIDAR CÓDIGO</Text>
                    </TouchableOpacity>
                    <HelperText type="error" visible={codeInvalid}>
                            Código inválido
                    </HelperText>
                </View>
                </View>
            </ScrollView>
        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        paddingHorizontal:20,

    },
})