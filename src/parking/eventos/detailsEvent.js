import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image,TouchableWithoutFeedback} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { colorPrimaryLigth,secondary,colorInput,colorInputBorder,surface,primary600, surfaceMediumEmphasis,
    primary800,OnSurfaceOverlay15,OnSurfaceDisabled,primary500, surfaceHighEmphasis, onSurfaceOverlay8,primary900,
    secondary600,secondary500,colorGray, surfaseDisabled,secondary50,errorColor } from '../../utils/colorVar';
import { TextInput as PaperTextInput, HelperText} from 'react-native-paper';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import Checkbox from 'expo-checkbox';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default function CodeEvent ({navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [pago, setPago] = useState(true)
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);

    const getToken =  async() => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }
    //Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/20`).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF); 
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
          getParkingInfo()
        });
        return unsubscribe;
    }, [navigation]);

    return  (
        <View>
            <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,paddingBottom:10,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}> 
                    <Text style={{color:secondary, fontSize:24, fontFamily:'Gotham-Bold', textAlign: 'center', marginTop:20}}>Bienvenido a eventos</Text>
                    <Image source={require("../../../assets/reserve/ilustracion_valet.png")}
                        style={{marginTop:20, marginLeft:'auto', marginRight:'auto'}}
                    />
                    <Text style={{color:secondary, fontSize:18, fontFamily:'Gotham-Bold', marginTop:20}}>Detalles</Text>
                    <View style={{flexDirection: 'row', padding: 10,}}>
                        <View>
                            <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>Fecha de Inicio</Text>
                            <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10, marginBottom:10}}>13-06-2022</Text>
                            <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>Fecha de culminación</Text>
                            <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10, marginBottom:10}}>13-06-2022</Text>
                            <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>Tiempo de parqueo</Text>
                            <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10, marginBottom:10}}>4 Horas</Text>
                            <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>Descuento</Text>
                            <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10, marginBottom:10}}>$15.000</Text>   
                        </View>
                        <View style={{marginLeft:'auto', marginRight:10}}>
                            <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>Hora de inicio</Text>
                            <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10, marginBottom:10}}>3:00 Pm</Text>
                            <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>Hora de Culminación</Text>
                            <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10, marginBottom:10}}>7:00 Pm</Text>
                            <Text></Text>
                            <Text></Text>
                            <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold', marginTop:23}}>Total a pagar</Text>
                            <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:7, marginBottom:10}}>$5.000</Text>
                        </View>
                    </View>
                    <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Bold', marginTop:20}}>Parqueadero asociado</Text>
                    <TouchableOpacity style={{right:10}}>
                        <View
                            style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                            }}
                            >
                            <View style={{flexDirection: 'row'}}>
                                <View >
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                        {
                                            parking !== null ?
                                            parking.name
                                            :
                                            ""
                                        }
                                    </Text>
                                    {
                                        parking !== null ?
                                            parking.address !== null ? 
                                                <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                            :
                                            <Text style={styles.text}>No hay dirección disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    }
                                    <View style={{flexDirection: 'row'}} >
                                        {
                                            parking !== null &&
                                                parking.open == 1 ? 
                                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                                :
                                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                        }    
                                            
                                        {
                                            parking !== null ?
                                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                                :
                                                <Text style={styles.text}>No hay horario disponible</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        }
                                    </View>
                                </View>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                    <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                                </View>
                            </View>
                        </View> 
                    </TouchableOpacity>
                    <Text style={{color:secondary, fontSize:18, fontFamily:'Gotham-Bold', marginTop:20}}>Código único</Text>
                    <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10}}>123456</Text>
                    <Text style={{color:secondary, fontSize:16, fontFamily:'Gotham-Medium', marginTop:10, marginTop:20}}>Medio de pago</Text>
                    <View style={{flexDirection: 'row', marginTop:20, marginBottom:20}}>
                        <Ionicons name="card-outline" size={24} color={secondary} />
                        <View style={{marginLeft:20}}>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Nombre del titular</Text>
                        </View>
                            <Text style={{right:40, fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop:20, marginBottom:20}}>
                        {
                            pago == true ?
                                <TouchableOpacity  style={{backgroundColor:secondary,padding: 14,borderRadius: 15,width: 379, height:48}} onPress={() => navigation.navigate('InfoEvent')}>        
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: '#fff', fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR $ 5.000</Text>
                                    </View>           
                                </TouchableOpacity>
                            :
                            <TouchableOpacity  style={{backgroundColor:secondary,padding: 14,borderRadius: 15,width: 379, height:48}} onPress={() => navigation.navigate('InfoEvent')}>        
                                <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                    <Text style={{color: '#fff', fontSize: 15,fontFamily:'Gotham-Bold',}}>FINALIZAR</Text>
                                </View>           
                            </TouchableOpacity>   
                        }                    
                    </View>
                </View>
            </ScrollView>
        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        paddingHorizontal:20,

    },
})