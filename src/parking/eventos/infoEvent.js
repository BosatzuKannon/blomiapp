import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,LogBox,Image} from 'react-native';
import CountDown from 'react-native-countdown-component';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import QRCode from 'react-native-qrcode-svg';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Non-serializable values were found in the navigation state'],["Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function."]);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ScheduledReserve({navigation}) {
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [hour, setHour] = useState();    
    const [hourFinal, setHourFinal] = useState();
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [plate, setPlate] = useState(null);
    const [duration, setDuration] = useState(null);
    const [price, setPrice] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [date, setDate] = useState(null);
    const [qrValue, serQrValue] = useState('');
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/20`).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    const terms = () => {
        setDialogTermsVisible(true);
    }
    //Metodo para traer data de la reserva
    const getDataReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/get/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                var date = moment(response.data.ResponseMessage.initial_date).format('MMMM DD YYYY');
                var timeI = moment(response.data.ResponseMessage.initial_date).format('HH:mm');
                setHour(timeI);
                var timeF = moment(response.data.ResponseMessage.final_date).format('HH:mm');
                setDuration(response.data.ResponseMessage.duration);
                setHourFinal(timeF);
                setDate(date);
                setPrice(response.data.ResponseMessage.price);
                setTypeVehicleId(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.vehicle_type_id);
                setPlate(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.plate);
            }else{
              Alert.alert("ERROR","No se en traer datos de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
        
      };
    const getTimeReserve = () =>{
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/reservation/time/start/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setTotalDuration(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo de inicio de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
          getParkingInfo();
        });
        ///calcular tiempo restante
        // var date = new Date();
        
        // var expirydate = global.dataVehicle.dateSend;
        // var diffr = moment.duration(moment(expirydate).diff(moment(date)));

        // var hours = parseInt(diffr.asHours());
        // var minutes = parseInt(diffr.minutes());
        // var seconds = parseInt(diffr.seconds());
        
        // var d = hours * 60 * 60 + minutes * 60 + seconds;
        // console.log("aa"+d);
        // const newDuration = d;
        // var variable = 1;
        // setInterval(()=> {
        //             variable = variable - 1/newDuration;
        //             setProgress(variable);
        //         }, 1000);

        return unsubscribe;

      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ServiceReserve')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, padding:20, paddingTop:10,}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'center'}}>Evento</Text>
                    <View style={{alignItems:'center'}}>
                        <Image
                        source={require("../../../assets/reserve/ilustracion_paseo1.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <Text style={{color: secondary,fontSize: 15,fontFamily: 'Gotham-Light',textAlign:'center', marginBottom:20}}>Presenta este codigo a la entrada  del punto de servicio </Text>
                    <View style={{alignItems:'center'}}>
                        
                            <QRCode
                                value={ qrValue ? qrValue : 'No se pudo generar el codigo QR' }
                                size={ 180 }
                                color= 'white'
                                backgroundColor='#000'                            
                           >
                            </QRCode>

                    </View>    
                    <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <View >
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                        {
                                            parking !== null ?
                                            parking.name
                                            :
                                            ""
                                        }
                                    </Text>
                                    {
                                        parking !== null ?
                                            parking.address !== null ? 
                                                <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                            :
                                            <Text style={styles.text}>No hay dirección disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    }
                                    <View style={{flexDirection: 'row'}} >
                                        {
                                            parking !== null &&
                                                parking.open == 1 ? 
                                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                                :
                                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                        }    
                                            
                                        {
                                            parking !== null ?
                                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                                :
                                                <Text style={styles.text}>No hay horario disponible</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        }
                                    </View>
                                </View>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                    <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                                </View>
                            </View>
                        </View> 
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:22, paddingTop:10,marginTop:16}}> 
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:10}}>Detalles del servicio</Text> 
                    <View style={{flexDirection: 'row'}}> 
                        <View style={{marginTop:20}}>
                            <Text style={styles.text}>Fecha de inicio</Text>
                            <Text style={styles.textBold}>13-06-2022</Text>
                        </View>
                        <View style={{marginTop:20, marginLeft:'auto', marginRight:50}}>
                            <Text style={styles.text}>Hora de inicio</Text>
                            <Text style={styles.textBold}>3:00 pm</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row'}}> 
                        <View style={{marginTop:20}}>
                            <Text style={styles.text}>Fecha culminación</Text>
                            <Text style={styles.textBold}>13-06-2022</Text>
                        </View>
                        <View style={{marginTop:20, marginLeft:'auto', marginRight:25}}>
                            <Text style={styles.text}>Hora Culminación</Text>
                            <Text style={styles.textBold}>7:00 pm</Text>
                        </View>
                    </View>
                   
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Tiempo de parqueo</Text>
                        <Text style={styles.textBold}>4 Horas</Text>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Descuento</Text>
                            <Text style={styles.textBold}>
                                $15.000
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Total a pagar</Text>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>$ 5.000</Text>
                        </View>
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('CodeEvent')}>
                            <Text style={styles.text3}>CERRAR</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Medium',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text3: {
        flex: 1,
        backgroundColor: surface,
        padding:5,
        fontSize:15,
        marginTop:25,
        marginBottom: 16,
        textAlign: 'center',
        fontFamily:'Gotham-Light',
        fontStyle: 'italic'
    }
  })