import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faUserPlus, faCheck} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import Toast from 'react-native-simple-toast';
import { Request } from '../../utils/api';
import { PRODUCT } from '../../utils/endpoints';

export default function SumaryMonthly ({route, navigation}){

    const [open, setOpen] = useState(true);
    const [isSelected, setSelection] = useState(true);
    const [vehicles, setVehicles] = useState([]);
    const [beneficiary, setBeneficiary] = useState([]);
    const [typeVehicleId2, setTypeVehicleId2] = useState(null);
    const [licensePlate2, setLicensePlate2] = useState(null);
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [dialogReserveVisible,setDialogReserveVisible] = useState(false);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [services, setServices] = useState([]);

    const  { parking, monthly } = route.params
   

    const checkOpening = async () => {
        const vehicle = await AsyncStorage.getItem("vehicle");
        setVehicles(JSON.parse(vehicle))

        const ben = await AsyncStorage.getItem("beneficiary");

        if (ben !== null) {
            setBeneficiary(JSON.parse(ben)[0])
        }

        const currentDate = moment();
        const dayOfWeekNumber = currentDate.day();

        await parking.schedule.standard.map((day)=>{
            if(day.day == dayOfWeekNumber){
                setOpenHour(day.open_hours)
                setCloseHour(day.close_hours)

                const now = moment();

                const horaAperturaMoment = moment(day.open_hours, 'HH:mm', true);
                const horaCierreMoment = moment(day.close_hours, 'HH:mm', true);

                if (now.isBetween(horaAperturaMoment, horaCierreMoment)) {
                    setOpen(1)
                }
            }
        })
    }

    //Metodo para enrutar a elegir un segundo vehiculo
    const listarVehiculos = async () =>{
        const cantVehicles = Object.keys(vehicles).length

        if(cantVehicles == 2){
            Alert.alert("Información","No se puede agregar más de dos vehiculos a la mensualidad.");
        }else{
            navigation.navigate('ListVehicleMonthly');
        }
    }

     const terms = () => {
        setDialogTermsVisible(true);
        if(Object.keys(beneficiary).length > 0){
            Alert.alert("Información","No se puede agregar más de un beneficiario");
        }else{
            setDialogTermsVisible(true);
        }
    }

    const unidad = (cant) => {
        if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
        }else{
                return cant
        }
    }

    const beneficiario = async () => {
        if(Object.keys(beneficiary).length > 0){
            Alert.alert("Información","No se puede agregar más de un beneficiario");
        }else{
            navigation.navigate('InfoBeneficiaryMonthly')
        }

    }
    const saveMonthly = async () => {
        navigation.navigate('PurchaseMonthlys',{'parking' : parking, 'monthly':monthly})
        // const data = {
        //     product_id: 1,
        //     product_type_id: 1,
        //     facility: parking,
        //     automatic_suscription: true,
        //     time_bougth: moment().format("YYYY-MM-DD HH:mm:ss"),
        //     beneficiary: beneficiary,
        //     vehicles: vehicles,
        // }

        // // console.log(`data a aguardar --> ${JSON.stringify(data)}`);
        // setSpinner(true)
        // const request = new Request();
        // const result = await request.request(PRODUCT.createProduct,'POST',data)
        // if(result === 0){
        //     setSpinner(false)
        //     Alert.alert("Error al registrar producto");
        //     console.log(result)
        // }else{
        //     setSpinner(false)
        //     Alert.alert(result.ResponseMessage)
        //     Toast.showWithGravity('Servicio registrado, pendiente de pago.', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        //     navigation.navigate('PurchaseMonthlys',{'parking' : parking, 'monthly':monthly})
        // }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            checkOpening();
        });
        return unsubscribe;
    }, [navigation]);

    return(
        <ScrollView style={{ height:height}}>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
            <View style={styles.container}>
                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:0,textTransform:'capitalize' }}>
                    {
                            parking !== null ?
                            parking.name
                            :
                            ""
                        }
                </Text>
                        {
                            parking !== null ?
                                parking.address !== null ? 
                                    <Text style={styles.text}>{parking.address}</Text>
                                :
                                <Text style={styles.text}>No hay dirección disponible</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        }
                        
                        <View style={{flexDirection: 'row', marginBottom:15}} >
                            {
                            parking !== null &&
                                open == 1 ? 
                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                :
                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                             }    
                            
                            {
                            parking !== null ?
                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            }
                        </View>


                    {/* tarjeta tipo mensualidad */}
                     <View style={{marginBottom:20 , marginTop:50}} >                     
                   
                        <TouchableOpacity> 
                            <View style={{flexDirection:'row'}} >
                                <View style={{flexDirection:'row',marginRight:10, marginTop:5, alignSelf: 'center'}}>
                                    <FontAwesomeIcon size={25} icon={monthly.vehicleType.split(" / ")[0] === 'Carro' ? faCarSide : monthly.vehicleType.split(" / ")[0] === 'Moto' ? faMotorcycle : faBicycle} color={secondary} /> 
                                    <Text> + </Text> 
                                    <FontAwesomeIcon size={25} icon={monthly.vehicleType.split(" / ")[1] === 'Carro' ? faCarSide : monthly.vehicleType.split(" / ")[1] === 'Moto' ? faMotorcycle : faBicycle} color={secondary} /> 
                                </View>
                                <View style={{marginRight:5}}>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:18, color:secondary}}>Mensualidad</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{monthly.name}</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{monthly.vehicleType.split(" / ")[0] === 'Carro' ? "Carro" : monthly.vehicleType.split(" / ")[0] === 'Moto' ? "Moto" : "Bicicleta" } - {monthly.vehicleType.split(" / ")[0] === 'Carro' ? "Carro" : monthly.vehicleType.split(" / ")[0] === 'Moto' ? "Moto" : "Bicicleta" }</Text>
                                </View>
                                <View style={{marginTop:10,justifyContent:'flex-end',marginLeft:'auto', alignContent: 'center',marginBottom:18,right:10 }}>
                                        <Text style={{ fontSize:18, color:'#1C7A00', fontFamily:'Gotham-Bold'}}>${unidad(monthly.price)}</Text>
                                </View>
                             </View>
                        </TouchableOpacity>

                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15, marginTop:40 }}>Vehiculos vinculados</Text>
                    </View>
                        {
                        vehicles.map((vehicle) =>{
                            return     <TouchableOpacity key={vehicle.id} style={{justifyContent:'center'}}>
                                <View
                                style={{
                                    backgroundColor: surface,
                                    borderRadius: 5,
                                    width:width-38,
                                    marginLeft: 0,
                                    marginTop:10,
                                    marginBottom:10,
                                    marginRight: 1,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                    width: 0,
                                    height: 1,
                                    },
                                    shadowOpacity: 0.49,
                                    shadowRadius: 4.65,
                                    elevation: 3,
                                    marginBottom:10,
                                    marginTop:0,
                                    paddingTop:5,
                                    paddingHorizontal:10,
                                }}
                                >
                                    <View style={{flexDirection: 'row',marginBottom:10,}}>
                                        {
                                            <FontAwesomeIcon size={30} icon={vehicle.vehicle_type_id === '1' ? faCarSide : vehicle.vehicle_type_id === '2' ? faMotorcycle : faBicycle } color={secondary} style={{marginLeft:10,marginTop:10,marginRight:20}}/>
                                        }
                                        
                                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                            {
                                                vehicle.vehicle_type_id === '1' ? 'Carro' : vehicle.vehicle_type_id === '2' ? 'Moto' : "Bicicleta"
                                            } 
                                        </Text>
                                        <Text style={{ textTransform: 'uppercase', fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,justifyContent:'flex-end',marginLeft:'auto' }}>
                                            {vehicle.vehicle_plate}
                                        </Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        })    
                        }                    

                {
                    beneficiary && Object.keys(beneficiary).length > 0 ?
                    <TouchableOpacity onPress={() => beneficiario()}>
                    
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15, marginTop:40 }}>Beneficiario agregado</Text>

                        <View style={{flexDirection: 'row', alignItems: 'center',}}>
                            <View>
                                <FontAwesomeIcon size={24} icon={faUserPlus} style={{ color:'#1C7A00', marginTop:5, marginRight:20 }} />
                            </View>
                            <View>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>{beneficiary.name}</Text>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>{beneficiary.document_type} -- {beneficiary.document}</Text>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>{beneficiary.email}</Text>
                            </View>
                            <View style={{marginLeft:50}}>
                                <FontAwesomeIcon Size={24} icon={faCheck} style={{ color:'#1C7A00',justifyContent:'flex-end',marginLeft:'auto' }} />
                            </View>
                        </View>
                    </TouchableOpacity>

                    : 


                    <Text></Text>
                }


                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15, marginTop:40 }}>Beneficios</Text>

                        <TouchableOpacity onPress={() => listarVehiculos()}>
                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15, color:"#D8F2F8"}}>
                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                    {
                                        <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                    }
                                        <View>
                                             <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                                Recibe mas sin costo adicional            
                                             </Text>
                                            <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                                Inscribe una segunda placa
                                            </Text>
                                            <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                            </Text>
                                        </View>
                            </View>
                        </TouchableOpacity>


                        <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                {
                                    <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                }
                                    <View>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                             Ahorro en dinero hasta           
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                              50% en tarifa regular
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                        </Text>
                                    </View>
                        </View>

                       

                        <View style={styles.container2}>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Vas a inscribir un beneficiario para tu mensualidad</Text>
                                <Text style={styles.text2}>Esto podria variar el precio final de tu compra</Text>
                                <View style={{marginBottom:10, flexDirection:'row', }}>
                                    <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: 172, marginRight:20, borderColor: secondary }} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: 172}} onPress={ () => beneficiario() }>
                                        <Text style={{color: '#F2FAE0', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                <View style={{marginVertical:20,alignItems:'center',}}>
                    {
                        isSelected ? 
                        <View style={{ flexDirection: 'row'}}>
                            <TouchableOpacity  style={{backgroundColor: "#FFFFFF",padding: 14,borderRadius: 15,width: 190, height:48}} onPress={terms}>
                                <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                    <Text style={{color: "#005A6D",fontSize: 14,fontFamily:'Gotham-Bold',}}>BENEFICIARIO</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity  style={{backgroundColor: "#005A6D",padding: 14,borderRadius: 15,width: 172, height:48}} onPress={saveMonthly}>
                                <View>
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: "#F2FAE0",fontSize: 15,fontFamily:'Gotham-Bold',}}>IR A PAGAR</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{ flexDirection: 'row'}}>
                            <TouchableOpacity disabled style={{backgroundColor: "#FFFFFF",padding: 14,borderRadius: 15,width: 190, height:48}}>
                                <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                    <Text style={{color: OnSurfaceDisabled,fontSize: 14,fontFamily:'Gotham-Bold',}}>BENEFICIARIO</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity disabled style={{backgroundColor: "rgba(0, 45, 51, 0.15)",padding: 14,borderRadius: 15,width: 172, height:48}}>
                                <View>
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>IR A PAGAR</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        
                    }
                </View>

            </View>



            </View>
        
        </ScrollView>

    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-300,
        paddingTop:10,
        height:height-45
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    box: {
        width:120,
        height:127,
        //borderColor: 'black',
        //  borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginRight: 10,
        left: -20,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.49,
        shadowRadius: 4.65,
        elevation: 3,
    },
    box1: {
        flexDirection:'row',
        padding: 16,
        width: 440,
        height: 200,
        backgroundColor: "#FFFFFF"    
    },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
        color: secondary,
       textAlign: 'center'
    },
    title2: {
        color: secondary,
        fontSize: 23    ,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        alignContent: 'center',
        justifyContent: 'center',
        marginLeft:80
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },

})