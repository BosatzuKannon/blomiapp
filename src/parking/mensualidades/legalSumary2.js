import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,TouchableWithoutFeedback,Switch} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faUserPlus, faCheck} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function SumaryMonthly ({route, navigation}){

    const [isSelected, setSelection] = useState(true);
    const [typeVehicleId1, setTypeVehicleId1] = useState(null);
    const [licensePlate1, setLicensePlate1] = useState(null);
    const [typeVehicleId2, setTypeVehicleId2] = useState(null);
    const [licensePlate2, setLicensePlate2] = useState(null);
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [dialogModalVisible, setDialogModalVisible] = useState(false);
    const [dialogModal1Visible, setDialogModal1Visible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [dialogReserveVisible,setDialogReserveVisible] = useState(false);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [expanded, setExpanded] = useState(true);
    const [services, setServices] = useState([]);
    const [isEnabled, setIsEnabled] = useState(true);

    const { listMonthlys } = route.params
   

    //Metodo para obtener el token
    const getToken = async () => {
        try {
            const value = await AsyncStorage.getItem("token");
            if (value !== null) {
                global.token = value;
                setToken(value);
                global.parkId =  await AsyncStorage.getItem('parkingId');
                global.monthlyId =  await AsyncStorage.getItem('monthlyId');
                setLicensePlate1(await AsyncStorage.getItem('vehiclePlate1'));
                setTypeVehicleId1(await AsyncStorage.getItem('vehicleTypeId1'));
                setLicensePlate2(await AsyncStorage.getItem('vehiclePlate2'));
                setTypeVehicleId2(await AsyncStorage.getItem('vehicleTypeId2'));
                getParkingInfo();
                getMonthlyInfo(value);
                console.log(listMonthlys);

                global.benName =  await AsyncStorage.getItem('benName');
                global.benEmail =  await AsyncStorage.getItem('benEmail');
                global.benDocument =  await AsyncStorage.getItem('benDocument');
                global.benDocumentType =  await AsyncStorage.getItem('benDocumentType');
                global.benPhone =  await AsyncStorage.getItem('benPhone');                
            }
        } catch (error) { }
    };
    
    //Método para obtener la información de parqueadero
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkId);
        console.log(`parqueadero ${id} -- ${global.parkId}`);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);

            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }

    //Metodo para consultar mensualidades de parqueadero
    const getMonthlyInfo =  async (token) =>{

        const config = {
            headers: { Authorization: token },  
        };
        setSpinner(true);        
        var id = parseInt(global.monthlyId);
        
        axios.post(`${API_URL}customer/app/parking/getMonthlyInfo`,{ monthlyId: id }, config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                let list = response.data.ResponseMessage;
                setServices(list);
            }else{
                Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
            } 
            }).catch(error => {
                setSpinner(false);
                console.log(error.response  );
                Alert.alert("error prueba",error.response.data.ResponseMessage);
                console.log(error);
            }) 
    }

    //Metodo para enrutar a elegir un segundo vehiculo
    const listarVehiculos = async () =>{

        if(await AsyncStorage.getItem('vehicleId2')){
            Alert.alert("Información","No se puede agregar más de dos vehiculos a la mensualidad.");
        }else{
            await AsyncStorage.setItem("where", '1');
            navigation.navigate('LegalListVehicles', {"listMonthlys": listMonthlys});
        }
    }

     const terms = () => {
         console.log(`entra`);
        setDialogTermsVisible(true);
        if(global.benName){
            Alert.alert("Información","No se puede agregar más de un beneficiario");
        }else{
            setDialogTermsVisible(true);
            //navigation.navigate('InfoBeneficiaryMonthly', {"listMonthlys": listMonthlys})
        }
    }
    const unidad = (cant) => {
        if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
        }else{
                return cant
        }
    }
    const _handlePress = () => {
        setExpanded(!expanded);
    }
    const toggleSwitch =  () => {

        if (isEnabled == true) {
            setIsEnabled(previousState => !previousState);  
            modal()
        }else if (isEnabled == false) {
            setIsEnabled(previousState => !previousState);
            modal1() 
            
        }     
    }
    const modal = () => {
        setDialogModalVisible(true);
        console.log('modal desactivado');
    }
    const modal1 = () => {
        setDialogModal1Visible(true);
        console.log('modal activado');
    }
    const beneficiario = async () => {
        // console.log(`entra`);
        // if(global.benName){
        //     Alert.alert("Información","No se puede agregar más de un beneficiario");
        // }else{
            navigation.navigate('InfoBeneficiaryMonthly', {"listMonthlys": listMonthlys})
        //}

    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        });
        return unsubscribe;
    }, [navigation]);

    return(
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
            <View style={styles.container}>
                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                    {
                            parking !== null ?
                            parking.name
                            :
                            ""
                        }
                </Text>
                        {
                            parking !== null ?
                                parking.address !== null ? 
                                    <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                :
                                <Text style={styles.text}>No hay dirección disponible</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        }
                        
                        <View style={{flexDirection: 'row', marginBottom:15}} >
                            {
                            parking !== null &&
                                parking.open == 1 ? 
                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                :
                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                             }    
                            
                            {
                            parking !== null ?
                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                    :
                                    <Text style={styles.text}>No hay horario disponible</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            }
                        </View>


                    {/* tarjeta tipo mensualidad */}
                     <View style={{marginBottom:20 , marginTop:50}} >                     
                   {   services.map((service) =>{
                        return <TouchableOpacity key={service.id} onPress={() => listVehicles(service) }> 
                            <View style={{flexDirection:'row'}} >
                                <View style={{flexDirection:'row',marginRight:10, marginTop:5, alignSelf: 'center'}}>
                                    <FontAwesomeIcon size={25} icon={service.vehicle_type_id_1 === 1 ? faCarSide : service.vehicle_type_id_1 === 2 ? faMotorcycle : faBicycle} color={secondary} /> 
                                    <Text> + </Text> 
                                    <FontAwesomeIcon size={25} icon={service.vehicle_type_id_2 === 1 ? faCarSide : service.vehicle_type_id_2 === 2 ? faMotorcycle : faBicycle} color={secondary} /> 
                                </View>
                                <View style={{marginRight:30}}>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:18, color:secondary}}>Prueba Mayo juridica</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>Juridica - Tipo 1</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>Carro - Moto{/* {service.vehicle_type_id_1 === 1 ? "Carro" : service.vehicle_type_id_1 === 2 ? "Moto" : "Bicicleta" } - {service.vehicle_type_id_2 === 1 ? "Carro" : service.vehicle_type_id_2=== 2 ? "Moto" : "Bicicleta" } */}</Text>
                                </View>
                                <View style={{marginTop:5,alignSelf: 'center', }}>
                                        <Text style={{ fontSize:18, color:secondary, fontFamily:'Gotham-Bold'}}>$150.000</Text>
                                        {/* <Text style={{ fontSize:14, color:'#1C7A00', fontFamily:'Gotham-Bold'}}>VER MAS</Text> */}
                                </View>
                             </View>
                        </TouchableOpacity>
                         })

                    }
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15, marginTop:40 }}>Vehiculos vinculados</Text>
                   </View>
                        {/* <Text style={styles.text}>Vehiculos vinculados</Text>                  */}
                    <TouchableOpacity style={{justifyContent:'center'}}>
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 5,
                            width:width-38,
                            marginLeft: 0,
                            marginTop:10,
                            marginBottom:10,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:0,
                            paddingTop:5,
                            paddingHorizontal:10,
                        }}
                        >
                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                               {/*  {
                                    <FontAwesomeIcon size={30} icon={typeVehicleId1 === '1' ? faCarSide : typeVehicleId1 === '2' ? faMotorcycle : faBicycle } color={secondary} style={{marginLeft:10,marginTop:10,marginRight:20}}/>
                                }
                                 */}<FontAwesomeIcon size={30} color={secondary} icon={faCarSide} style={{marginLeft:10,marginTop:10,marginRight:20}}/>
                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                   {/*  {
                                        typeVehicleId1 === '1' ? 'Automovil' : typeVehicleId1 === '2' ? 'Moto' : "Bicicleta"
                                    } */} Automovil
                                </Text>
                                <Text style={{ textTransform: 'uppercase', fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,justifyContent:'flex-end',marginLeft:'auto' }}>
                                     {/* {licensePlate1} */}   AAA000 
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{justifyContent:'center'}}>
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 5,
                            width:width-38,
                            marginLeft: 0,
                            marginTop:10,
                            marginBottom:10,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:0,
                            paddingTop:5,
                            paddingHorizontal:10,
                        }}
                        >
                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                                {
                                    <FontAwesomeIcon size={30} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:10,marginRight:20}}/>
                                }
                                
                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                   Moto
                                </Text>
                                <Text style={{ textTransform: 'uppercase', fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,justifyContent:'flex-end',marginLeft:'auto' }}>
                                    { /*licensePlate1 */} VBO09D
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    {
                        licensePlate2 ?

                            <TouchableOpacity style={{justifyContent:'center'}}>
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 5,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:0,
                            paddingTop:5,
                            paddingHorizontal:10,
                        }}
                        >
                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                                {
                                    <FontAwesomeIcon size={30} icon={typeVehicleId2 === '1' ? faCarSide : typeVehicleId2 === '2' ? faMotorcycle : faBicycle } color={secondary} style={{marginLeft:10,marginTop:10,marginRight:20}}/>
                                }
                                
                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                    {
                                        typeVehicleId2 === '1' ? 'Automovil' : typeVehicleId2 === '2' ? 'Moto' : "Bicicleta"
                                    } 
                                </Text>
                                <Text style={{ textTransform: 'uppercase', fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,justifyContent:'flex-end',marginLeft:'auto' }}>
                                    {licensePlate2}
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    :

                    <Text></Text>
                    }

                {
                    global.benName ?
                    <TouchableOpacity onPress={() => listarVehiculos()}>
                    
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15, marginTop:40 }}>Beneficiario agregado</Text>

                        <View style={{flexDirection: 'row', alignItems: 'center',}}>
                            <View>
                                <FontAwesomeIcon size={24} icon={faUserPlus} style={{ color:'#1C7A00', marginTop:5, marginRight:20 }} />
                            </View>
                            <View>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>{global.benName}</Text>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>{global.benDocumentType} -- {global.benDocument}</Text>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>{global.benEmail}</Text>
                            </View>
                            <View style={{marginLeft:50}}>
                                <FontAwesomeIcon Size={24} icon={faCheck} style={{ color:'#1C7A00',justifyContent:'flex-end',marginLeft:'auto' }} />
                            </View>
                        </View>
                        
                        
                        {/* <view style={{flexDirection: 'row',marginBottom:10,marginLeft:15, color:"#D8F2F8"}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}>{global.benName}</Text>
                        </view> */}
                    </TouchableOpacity>

                    : 


                    <Text></Text>
                }


                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15, marginTop:40 }}>Beneficios</Text>

{/*                         <TouchableOpacity onPress={() => listarVehiculos()}>
                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15, color:"#D8F2F8"}}>
                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                    {
                                        <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                    }
                                        <View>
                                             <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                                Recibe mas sin costo adicional            
                                             </Text>
                                            <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                                Inscribe una segunda placa
                                            </Text>
                                            <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                            </Text>
                                        </View>
                            </View>
                        </TouchableOpacity> */}


                       <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                {
                                    <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                }
                                    <View>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                             Su descuento le permite ahorrar          
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                             $ 20.000
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                        </Text>
                                    </View>
                        </View>

                       

                        <View style={styles.container2}>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Vas a inscribir un beneficiario para tu mensualidad</Text>
                                <Text style={styles.text2}>Esto podria variar el precio final de tu compra</Text>
                                <View style={{marginBottom:10, flexDirection:'row', }}>
                                    <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: 172, marginRight:20, borderColor: secondary }} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: 172}} onPress={ () => beneficiario() }>
                                        <Text style={{color: '#F2FAE0', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* medio de pago */}
                <View style={{ display: 'flex', justifyContent: 'center'}}>
                     <NativeBaseProvider>
                        <Center flex={1}>   
                            <ScrollView>
                                <Box m={3} style={{border:0 ,left: -25 }}>
                                    <Accordion allowMultiple _text={{border:0}} border={0} onTouchStart={_handlePress}>
                                        <Accordion.Item _text={{color:secondary}} >
                                            <Accordion.Summary  _expanded={{backgroundColor:surface,}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary}}>Medio de pago</Text>
                                                    <View style={{marginLeft:160}}>
                                                        <MaterialIcons name={expanded ? "keyboard-arrow-down":"keyboard-arrow-up"} size={30} color={secondary} />
                                                    </View> 
                                                </View>
                                            </Accordion.Summary>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="card-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:30}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Nombre del titular</Text>
                                                        </View>
                                                            <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                                                    </View> 
                                                </Accordion.Details>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="sync-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:30}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Renovación automática</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Mensualidad</Text>
                                                        </View>
                                                        <View style={{right:20, marginTop: 5}}>
                                                       {/*  <Switch
                                                            trackColor={{ false: "#767577", true: "rgba(145, 212, 0, 0.38)" }}
                                                            thumbColor={isEnabled ? "#90D400" : "#f4f3f4"}
                                                            ios_backgroundColor="#3e3e3e"
                                                            onValueChange={toggleSwitch}
                                                            value={isEnabled}
                                                            onPress={modal}
                                                            /> */}
                                                            <Text style={{ fontSize: 14, marginTop: 10,alignContent:'flex-end',justifyContent:'flex-end',marginLeft:80,fontFamily:'Gotham-Medium', color:primary800}}>ACTIVA</Text>
                                                        </View>                                             
                                                           {/*  <Ionicons name="toggle-outline" size={24} color={secondary} style={{ fontSize: 24,right:20, marginTop: 5,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}} /> */}
                                                    </View> 
                                                </Accordion.Details>
                                        </Accordion.Item>
                                    </Accordion>
                                </Box>
                            </ScrollView>
                        </Center>
                    </NativeBaseProvider>
                     </View>
                     {/* modal cancelacion mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModalVisible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas cancelar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal cancelacion automatica */}
                {/* modal habilitar mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModal1Visible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas habilitar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal renovación automatica */}        
                <View style={{marginVertical:20,alignItems:'center',}}>
                    {
                        isSelected ? 
                        <View style={{ flexDirection: 'row'}}>
                          {/*    <TouchableOpacity  style={{backgroundColor: "#FFFFFF",padding: 14,borderRadius: 15,width: 190, height:48}} onPress={terms}>
                                <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                    <Text style={{color: "#005A6D",fontSize: 14,fontFamily:'Gotham-Bold',}}>SU descuento esta ahorrando: $20.000</Text>
                                </View>
                            </TouchableOpacity> */}
                            <TouchableOpacity  style={{backgroundColor: "#005A6D",padding: 14,borderRadius: 15,width: 350, height:48}} onPress={() => navigation.navigate('LegalActive', {"listMonthlys" : listMonthlys})}>
                                <View>
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: "#F2FAE0",fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{ flexDirection: 'row'}}>
                            <TouchableOpacity disabled style={{backgroundColor: "#FFFFFF",padding: 14,borderRadius: 15,width: 190, height:48}}>
                                <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                    <Text style={{color: OnSurfaceDisabled,fontSize: 14,fontFamily:'Gotham-Bold',}}>BENEFICIARIO</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity disabled style={{backgroundColor: "rgba(0, 45, 51, 0.15)",padding: 14,borderRadius: 15,width: 172, height:48}}>
                                <View>
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        
                    }
                </View>

            </View>



            </View>
        
        </ScrollView>

    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-300,
        paddingTop:10,
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    box: {
        width:120,
        height:127,
        //borderColor: 'black',
        //  borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginRight: 10,
        left: -20,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.49,
        shadowRadius: 4.65,
        elevation: 3,
    },
    box1: {
        flexDirection:'row',
        padding: 16,
        width: 440,
        height: 200,
        backgroundColor: "#FFFFFF"    
    },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
        color: secondary,
       textAlign: 'center'
    },
    title2: {
        color: secondary,
        fontSize: 23    ,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        alignContent: 'center',
        justifyContent: 'center',
        marginLeft:80
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },

})