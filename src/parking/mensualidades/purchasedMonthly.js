import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function PurchasedMonthly ({navigation}){
    const [spinner, setSpinner] = useState(false);
    const [expanded, setExpanded] = useState(true);
    const [token, setToken] = useState(null);

    const getToken = async () => {
        try {
        const value = await AsyncStorage.getItem("token");
        if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
        }
        } catch (error) { }
    };

    const _handlePress = () => {
        setExpanded(!expanded);
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        //getParkingList();
        });
        return unsubscribe;
    }, [navigation]);

    return(
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface, marginBottom:15}}>
                    <TouchableOpacity onPress={() => navigation.navigate('SumaryMonthly')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>

            <View style={styles.container}>  

                <View style={{ marginBottom: 15 }}>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize: 24, color: secondary }}>Mensualidades</Text>
                </View>

                <View>
                    <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                            {
                                <FontAwesomeIcon size={30} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/>
                            }
                                <View>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: "#002D33",fontSize:12,marginTop:10 }}>
                                        Mensualidad
                                    </Text>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: "#005A6D",fontSize:16,marginTop:5}}>
                                        $000.000 
                                    </Text>
                                </View>
                        </View>
                </View>

                <NativeBaseProvider>
                        <Center flex={1}>
                            <ScrollView>
                                <Box m={3} style={{border:0 ,left: -25 }}>
                                    <Accordion allowMultiple _text={{border:0}} border={0} onTouchStart={_handlePress}>
                                        <Accordion.Item _text={{color:secondary}} >
                                            <Accordion.Summary  _expanded={{backgroundColor:surface,}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary}}>Medio de pago</Text>
                                                    <View style={{marginLeft:160}}>
                                                        <MaterialIcons name={expanded ? "keyboard-arrow-down":"keyboard-arrow-up"} size={30} color={secondary} />
                                                    </View> 
                                                </View>
                                            </Accordion.Summary>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="card-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:30}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Nombre del titular</Text>
                                                        </View>
                                                            <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                                                    </View> 
                                                </Accordion.Details>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="card-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:30}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Renovación automática</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Mensual</Text>
                                                        </View>
                                                            <Ionicons name="card-outline" size={24} color={secondary} style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}} />
                                                    </View> 
                                                </Accordion.Details>
                                        </Accordion.Item>
                                    </Accordion>
                                </Box>
                            </ScrollView>
                        </Center>
                    </NativeBaseProvider>
                         
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('ActiveMonthly')}>
                        <View>
                            <Text style={{ fontFamily: 'Gotham-Bold', fontSize: 14, color: '#F2FAE0',marginTop:15, marginLeft:10 }}>PAGAR</Text>
                            <Text style={{ fontFamily: 'Gotham-Bold', fontSize: 14, color: '#F2FAE0', marginLeft:280, bottom: 20}}>$ 000.000</Text>
                        </View>
                    </TouchableOpacity>
            
            </View>

        </ScrollView>


    )


}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-170,
        paddingTop:10,
    },
    button: {
        width: 379,
        height: 48,
        borderRadius: 15,
        backgroundColor: "#005A6D"
    }


})