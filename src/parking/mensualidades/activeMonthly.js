import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faUserPlus, faCheck} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import QRCode from 'react-native-qrcode-svg';
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
import ServiceReserve from '../reserve/serviceReserve';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../../utils/api';
import { FACILITY } from '../../utils/endpoints';

export default function ActiveMontly({navigation}){
    const [spinner, setSpinner] = useState(false);
    const [qrValue, setQrValue] = useState(null);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [monthly, setMonthly] = useState([]);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [open, setOpen] = useState(null);
    const [vehicles, setVehicles] = useState([]);
    const [typeVehicleId1, setTypeVehicleId1] = useState(null);
    const [licensePlate1, setLicensePlate1] = useState(null);
    const [typeVehicleId2, setTypeVehicleId2] = useState(null);
    const [licensePlate2, setLicensePlate2] = useState(null);

    // const getToken = async () => {
    //     try {
    //     const value = await AsyncStorage.getItem("token");
    //     if (value !== null) {
    //         global.token = value;
    //         setToken(value);
    //         // global.monthlyId =  await AsyncStorage.getItem('newMonthlyId');
    //         // await Promise.all(getInfoMonthly(value));            
    //         // getParkingInfo(value);
    //     }
    //     } catch (error) { }
    // };

    // const getInfoMonthly = async (token) => {
    //     let config = {
    //         headers: { Authorization: token }
    //         };
    //     setSpinner(true);
    
    //     axios.get(`${API_URL}customer/app/getInfoMonthlyByUser/${global.monthlyId}`,config).then( async (response) => {
            
            
    //         const cod = response.data.ResponseCode;
    //         setSpinner(false);
    //         if(cod === 0){
    //              console.log(`Archivo config __________ ${JSON.stringify(response.data)}`)
    //              global.parking_id = response.data.ResponseMessage[0].parking_id
    //              setMonthly(response.data.ResponseMessage[0]);
    //               setVehicles(response.data.ResponseMessage[0].vehicles);
    //             //getParkingInfo();
    //             //navigation.navigate('SumaryPasadia');
    //         }else{
    //           Alert.alert("ERROR","No se pudo traer los vehiculos");
    //         }
    //       }).catch(error => {
    //         setSpinner(false);
    //         Alert.alert("",error.response.data.ResponseMessage);
    //         //console.log(error.response.data.ResponseMessage);
    //       })
    // }

    //Método para traer la info del parking
    const getParkingInfo = async (parkingId) => {
        setSpinner(true)
        const data = {id:'877caabb-fb63-4c76-96dd-91f870595087'}
        const request = new Request();
        const result = await request.request(FACILITY.infoFacility,'POST',data)
        if(result === 0){
            setSpinner(false)
            Alert.alert('Error',"No es posible consultar la información del parqueadero");                      
        }else{
            setSpinner(false)
            setParking(result.ResponseData)
            // setQrValue(JSON.stringify(result.ResponseData))
            checkOpening(result.ResponseData)
        }
    }

    const checkOpening = async (parking) => {
        const currentDate = moment();
        const dayOfWeekNumber = currentDate.day();

        await parking.schedule.standard.map((day)=>{
            if(day.day == dayOfWeekNumber){
                setOpenHour(day.open_hours)
                setCloseHour(day.close_hours)

                const now = moment();

                const horaAperturaMoment = moment(day.open_hours, 'HH:mm', true);
                const horaCierreMoment = moment(day.close_hours, 'HH:mm', true);

                if (now.isBetween(horaAperturaMoment, horaCierreMoment)) {
                    setOpen(1)
                }
            }
        })
    }


    const unidad = (cant) => {

        if (cant) {
            if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
            }else{
                return cant
            }   
        }
        else{
            return "0"
        }
        
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            // getToken();
            getParkingInfo()
            //getParkingList();
        });
        return unsubscribe;
    }, [navigation]);

    return(
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface, marginBottom:15}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>

            <View style={styles.container}>

                <View style={{ marginBottom:20 }}>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:15, alignSelf: 'center' }}>Mensualidad natural adquirida</Text>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color: "#005A6D", alignSelf: 'center' }}>Presenta este codigo a la salida del punto de servicio para hacer valida tu mensualidad</Text>
                </View>

                <View style={{alignItems:'center', marginLeft: -25, marginBottom:30}}>
                    <QRCode
                        value={ qrValue ? qrValue : 'NA' }
                        size={ 120 }
                        color= 'white'
                        backgroundColor='#519B00'
                    >
                    </QRCode>
                </View> 

                <View>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:15 }}>Detalles</Text>
                    
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                            {
                                parking !== null ?
                                parking.name
                                :
                                ""
                            }
                        </Text>
                </View>

                <View style={{flexDirection: 'row', marginBottom:15}} >
                    {
                        parking !== null &&
                        open == 1 ? 
                        
                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                            :
                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                    }    
                            
                    {
                        parking !== null ?
                            parking.finalHour !== "" && parking.initialHour !== "" ? 
                                <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            }
                 </View>

                 <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color:secondary, marginBottom:15 }}>Placas vinculadas</Text>
                 <View style={{flexDirection: 'row'}}>
                    <View style={{marginRight:50}}>
                        <FontAwesomeIcon size={30} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/><Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color:secondary, marginBottom:15 }}>KJT764</Text>
                    </View>
                    <View>
                        <FontAwesomeIcon size={30} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/><Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color:secondary, marginBottom:15 }}>JHW196</Text>
                    </View>
                 </View>
                 
                {

                     vehicles.map((vehicle) => {
                     return <TouchableOpacity key={vehicle.id}>
                        <View style={{
                        backgroundColor: surface,
                        borderRadius: 5,
                        width:width-40,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        paddingTop:5,
                        }}
                    >
                    <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                            {
                                <FontAwesomeIcon size={30} icon={ vehicle.vehicle_type_id === 1 ? faCarSide : vehicle.vehicle_type_id === 2 ? faMotorcycle : faBicycle } color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/>
                            }
                                <View>
                                    <Text style={{ textTransform: 'uppercase',fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                        {vehicle.plate}
                                    </Text>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:5}}>
                                        { vehicle.vehicle_type_id === 1 ? 'Automovil' :vehicle.vehicle_type_id === 2 ? 'Moto' : "Bicicleta" }
                                    </Text>
                                </View>
                        </View>
                    </View>
                </TouchableOpacity>
                


                 })
                 
                }

                <TouchableOpacity >
                    
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15, marginTop:40 }}>Beneficiario agregado</Text>

                        <View style={{flexDirection: 'row', alignItems: 'center',}}>
                            <View>
                                <FontAwesomeIcon size={24} icon={faUserPlus} style={{ color:'#1C7A00', marginTop:5, marginRight:20 }} />
                            </View>
                            <View>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>Carlos Jaramillo</Text>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>CC -- 1087116281</Text>
                                <Text style={{textTransform: 'uppercase',fontFamily:'Gotham-Bold', fontSize:14, color:secondary}}>3175345577</Text>
                            </View>
                            <View style={{marginLeft:50}}>
                                <FontAwesomeIcon Size={24} icon={faCheck} style={{ color:'#1C7A00',justifyContent:'flex-end',marginLeft:'auto' }} />
                            </View>
                        </View>
                        
                        
                        {/* <view style={{flexDirection: 'row',marginBottom:10,marginLeft:15, color:"#D8F2F8"}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}>{global.benName}</Text>
                        </view> */}
                    </TouchableOpacity>

                <View style={{marginTop:20,flexDirection:'row'}}>
                     <View>
                        <Text style={{ fontFamily:'Gotham-Light', fontSize:14, color:secondary, marginRight:50, marginBottom:5}}>Fecha de compra</Text>
                        <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                            23-01-2024
                        </Text>
                    </View>
                    <View>
                        <Text style={styles.textTittle}>Fecha de expiración</Text>
                        <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                            23-02-2024
                        </Text>
                    </View>
                </View>

                <View style={{ marginTop:20}}>
                    <View>
                        <Text style={styles.textTittle}>Costo</Text>
                        <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>${unidad(150000)}</Text>
                    </View>
                </View>
                
                <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                    <View style={{ marginTop:40}}>
                        <Text style={{alignSelf:'center', fontSize:14, color:'rgba(0, 45, 51, 0.6)'}}>CERRAR</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </ScrollView>


    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-350,
        paddingTop:10,
    },
    textTittle: {
        fontFamily: 'Gotham-Light',
        fontSize: 12,
        color: secondary,
        marginBottom:5
    }

})