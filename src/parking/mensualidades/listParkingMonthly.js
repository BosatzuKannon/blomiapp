import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import { TextInput as PaperTextInput } from 'react-native-paper';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../../utils/api';
import { FACILITY } from '../../utils/endpoints';

export default function ListParkingMonthly({route, navigation}){

    const [spinner, setSpinner] = useState(false);
    const [listParking, setListParking] = useState([]);
    const [filterList , setFilterList] = useState('');
    const [cant, setCant] = useState(0);
    const [selectedCar, setselectedCar] = useState(false);
    const [selectedMot, setSelectedMot] = useState(false);
    const [selectedByci, setSelectedByci] = useState(false);

    const changeFirst = () => {
        setselectedCar(true); 
        setSelectedMot(false); 
        setSelectedByci(false);
    }
     const changeSecond = () => {
        setselectedCar(false); 
        setSelectedMot(true); 
        setSelectedByci(false);   
    }
    const changeThird = () => {
        setselectedCar(false); 
        setSelectedMot(false); 
        setSelectedByci(true);  
    }

    //Metodo para enrutar a la lista de mensualidades por parqueadero
    const setMonthly = async (parkingId) =>{
        navigation.navigate('SelectMonthly', {'parkingId' : parkingId})
    }

    //metodo consulta parqueaderos general
    const getParkingList =  async () =>{
        const data = {
            product_id:"1"
          }
        setSpinner(true);
        const request = new Request();
        const result = await request.request(FACILITY.listFacilityPerProductMovile,'POST',data)
        if(result === 0){
            setSpinner(false);
        }else{
            setSpinner(false);
            setListParking(result.ResponseData);
            const amountResult = Object.keys(result.ResponseData).length
            setCant(amountResult)
        }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getParkingList();
        });
        return unsubscribe;
    }, [navigation]);

  
    return(

        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
            <View style={ styles.container }>
                <View >
                    <Text style={{ fontFamily: "Gotham-Bold", fontSize: 24, color:secondary }}>Selecciona un parqueadero para tu mensualidad</Text>
                    <Text style={{ fontFamily: "Gotham-Bold", fontSize: 14, color: "rgba(0, 0, 0, 0.6)" , marginTop: 15, marginBottom:25}}>Filtra los parqueaderos segun tu necesidad.</Text>
                </View>

               <View style={{flexDirection: 'row',marginBottom:30,marginLeft:10,overflow:'scroll'}}>
                    <Chip  textStyle={{fontSize:15}} style={ selectedCar ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={selectedCar} selectedColor={secondary} mode='flat' onPress={changeFirst}>Automovil</Chip>
                    <Chip textStyle={{fontSize:15}} style={ selectedMot ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={selectedMot} selectedColor={secondary} mode='flat' onPress={changeSecond}>Motocicleta</Chip>
                    <Chip textStyle={{fontSize:15}} style={ selectedByci ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor: 'rgba(0, 45, 51, 0.08)', marginRight:6,}}  selected={selectedByci} selectedColor={secondary} mode='flat' onPress={changeThird}>bicicleta</Chip>
                </View>

                <View style={{flexDirection: 'row', marginTop:10, marginBottom:15}}>
                    <View style={{ marginRight:70 }}> 
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color: secondary}}>Parqueaderos</Text>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#005A6D"}}>Disponibles para mensualidad</Text>
                    </View>
                    <View style={{ marginRight:15 }}>
                        <TouchableOpacity>
                            <MaterialIcons name="search" size={35} color={secondary} />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity style={{ marginTop:5, marginLeft:60}}
                        onPress={() => navigation.navigate('FilterList')}>
                          <Feather name="sliders" size={24} color={secondary} />
                        </TouchableOpacity>
                    </View>     
                </View> 

                <Text style={{ color:"rgba(0, 45, 51, 0.15)" , marginBottom: 20}}>_____________________________________________________________________</Text>

                <View style={{ flexDirection: 'row', marginBottom:20 }}>
                    <View>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize: 12, color:"#002D33", marginRight:30}}>Cerca de {cant} resultados</Text>
                    </View>
                    <View style={{ marginRight:10 }}>
                        <Ionicons name="md-location-outline" size={20} color="#002D33" />      
                    </View>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize: 10, color:"#002D33" }}>TODAS LAS CIUDADES</Text>
                </View>


                {
                    listParking.length !== 0 &&

                    listParking.filter((parking) =>{
                          if(filterList == ""){
                            return parking
                          }else if(parking.name.toLowerCase().includes(filterList.toLowerCase())){
                              return parking
                          }
                        }).map( (parking) => {
                    return  <TouchableOpacity key={parking.id} onPress={ () => setMonthly(parking.id)}>
                            <View style={{
                            backgroundColor: surface,
                            borderRadius: 5,
                            width:width-40,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            paddingTop:5,
                                            }}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{marginLeft: 10, fontFamily: 'Gotham-Bold', fontSize:10, color: "#1C7A00" }}>{parking.name}</Text>
                            </View>
                            
                            <View>
                                <Text style={{marginLeft: 10, fontFamily: 'Gotham-Bold', fontSize:16, color: secondary}}>{parking.name}</Text>
                            </View>         
            
                            <View style={{marginBottom:10,}}>
                                <Text style={{marginLeft: 10, fontFamily: 'Gotham-Bold', fontSize:14, color: "rgba(0, 45, 51, 0.6)" }}>
                                    {parking.address }
                                </Text>
                            </View>

                        </View>
                    </TouchableOpacity>

                            
                })
            }
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-170,
        paddingTop:10,
    },
    buttons: {
        borderRadius: 16,
       // borderColor: 'red', 
        //borderWidth: 1,  //borderBottomWidth
        padding: 4, 
        backgroundColor: "rgba(0, 45, 51, 0.08)",
        width: 140,
        height: 32,  
        marginRight: 15 ,
        left: -15     
    },

})