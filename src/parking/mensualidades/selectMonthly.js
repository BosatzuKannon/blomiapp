import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert, Modal, TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight,faTrash} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../../utils/api';
import { FACILITY, PRODUCT } from '../../utils/endpoints';

export default function SelectMonthly ({route, navigation}){
    const [spinner, setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [open, setOpen] = useState(0);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null); 
    const [services, setservices] = useState([]);
    const [dialogVisible, setDialogVisible] = useState(false);

    //Método para obtener la información de parqueadero
    const getParkingInfo = async (parkingId) => {
        setSpinner(true)
        const data = {id:parkingId}
        const request = new Request();
        const result = await request.request(FACILITY.infoFacility,'POST',data)
        if(result === 0){
            setSpinner(false)
            Alert.alert('Error',"No es posible consultar la información del parqueadero");                      
        }else{
            setSpinner(false)
            setParking(result.ResponseData)
            checkOpening(result.ResponseData)
        }
    }

    const checkOpening = async (parking) => {
        const currentDate = moment();
        const dayOfWeekNumber = currentDate.day();

        await parking.schedule.standard.map((day)=>{
            if(day.day == dayOfWeekNumber){
                setOpenHour(day.open_hours)
                setCloseHour(day.close_hours)

                const now = moment();

                const horaAperturaMoment = moment(day.open_hours, 'HH:mm', true);
                const horaCierreMoment = moment(day.close_hours, 'HH:mm', true);

                if (now.isBetween(horaAperturaMoment, horaCierreMoment)) {
                    setOpen(1)
                }
            }
        })
    }

    //metodo consulta mensualidades de parqueadero
    const getParkingServices =  async (parkingId) =>{
        const data = {
            product_id: "1",
            product_type_id : "1",
            facility_id: parkingId
          }
        setSpinner(true);
        const request = new Request();
        const result = await request.request(PRODUCT.listProduct,'POST',data)
        if(result === 0){
            setSpinner(false);
        }else{
            setSpinner(false);
            setservices(result.ResponseData)            
        }
    }
    
    //Metodo para enrutar a la lista de vehiculos
    const listVehicles = async (service) => {
        await AsyncStorage.removeItem("vehicle");
        await AsyncStorage.removeItem("beneficiary");
        navigation.navigate('ListVehicleMonthly', {'parking' : parking, 'monthly':service});
    }

    const evalResponse =  async (resp) => {
        console.log(resp);
        if (resp == 0) {
            setDialogVisible(!dialogVisible);
        }
        else{
            await AsyncStorage.setItem("vehicleId", global.vehicleId);
            navigation.navigate('ListVehicleMonthly');
        }
    }
    const unidad = (cant) => {
        if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
        }else{
                return cant
        }
    }   
    

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            const { params } = route;
            const parkingId = params ? params.parkingId : null;
            getParkingServices(parkingId);
            getParkingInfo(parkingId)
        });
        return unsubscribe;
    }, [navigation]);

    return(
       <ScrollView>
             <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ListParkingMonthly')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
            <View style={styles.container}>
                <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:20 }}>
                    <View style={{ marginTop: 15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                        {
                            parking !== null ?
                            parking.name
                            :
                            ""
                        }
                        </Text>
                        {
                            parking !== null ?
                                parking.address !== null ? 
                                    <Text style={styles.text}>{parking.address}</Text>
                                :
                                <Text style={styles.text}>No hay dirección disponible</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        }
                        
                        <View style={{flexDirection: 'row'}} >
                        {
                            parking !== null &&
                                open == 1 ? 
                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                :
                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                        }    
                            
                            {
                                parking !== null ? 
                                        <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                    :
                                    <Text style={styles.text}>No hay horario disponible</Text>
                                
                            }
                        </View>
                    </View>
                    <View style={{alignItems: 'center',justifyContent: 'center',}}>
                        <View style = {styles.lineStyle} />
                    </View>
                    <View>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}>Tarifa base / Precio</Text>
                    </View>
                    <View style={{flexDirection:'row',}}>
                        {
                            parking !== null && parking.rates !== null ?
                                parking.rates.standard !== 0 ?
                                
                                    parking.rates.standard.map( (tarifa) => {
                                        return <View style={{flexDirection:'row',marginTop:10, marginRight:10}} key={tarifa.id}>
                                                    <FontAwesomeIcon size={20} 
                                                    icon={tarifa.vehicle_type === 1 ? faCar
                                                        :tarifa.vehicle_type === 2 ? faMotorcycle :tarifa.vehicle_type === 3 && faBicycle } color={primary700} />
                                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}>{tarifa.rate_type} / <Text style={{color: surfaceMediumEmphasis}}> ${unidad(tarifa.rate_price)}</Text></Text>
                                                </View>
                                        })
                                :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                            :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                        }
                    </View>
                </View>      

                    {/* tarjeta tipo mensualidad */}
                     <View style={{marginBottom:50}} >
                   {   services.map((service) =>{
                        return <TouchableOpacity key={service.id} onPress={() => listVehicles(service) }> 
                            <View style={{flexDirection:'row', marginBottom:20}} >
                                <View style={{flexDirection:'row',marginRight:10, marginTop:18}}>
                                    <FontAwesomeIcon size={25} icon={service.vehicleType.split(" / ")[0] === 'Carro' ? faCarSide : service.vehicleType.split(" / ")[1] === 'Moto' ? faMotorcycle : faBicycle} color={secondary} /> 
                                    <Text> + </Text> 
                                    <FontAwesomeIcon size={25} icon={service.vehicleType.split(" / ")[0] === 'Carro' ? faCarSide : service.vehicleType.split(" / ")[1] === 'Moto' ? faMotorcycle : faBicycle} color={secondary} /> 
                                </View>
                                <View style={{marginRight:30}}>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:18, color:secondary}}>Mensualidad</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{service.name}</Text>
                                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{service.vehicleType.split(" / ")[0] === 'Carro' ? "Carro" : service.vehicleType.split(" / ")[0] === 'Moto' ? "Moto" : "Bicicleta" } - {service.vehicleType.split(" / ")[1] === 'Carro' ? "Carro" : service.vehicleType.split(" / ")[1] === 'Moto' ? "Moto" : "Bicicleta" }</Text>
                                </View>
                                <View style={{marginTop:10,justifyContent:'flex-end',marginLeft:'auto', alignContent: 'center',marginBottom:18,right:10 }}>
                                        <Text style={{ fontSize:18, color:secondary, fontFamily:'Gotham-Bold'}}>${unidad(service.price)}</Text>
                                        <Text style={{ fontSize:14, color:'#1C7A00', fontFamily:'Gotham-Bold'}}>VER MAS</Text>
                                </View>
                             </View>
                        </TouchableOpacity>
                         })

                    }
                   </View>                  
                    
                    

                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:12, color: 'rgba(0, 45, 51, 0.6)', marginBottom:15 }}>Beneficios</Text>

                        <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                {
                                    <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                }
                                    <View>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                             Ahorro en dinero hasta           
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                              50% en tarifa regular
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                        </Text>
                                    </View>
                        </View>

                        <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                                {
                                    <FontAwesomeIcon size={25} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:30,marginRight:20}}/>
                                }
                                    <View>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#008293",fontSize:12,marginTop:15}}>
                                             Legal          
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Bold", color: "#1C7A00",fontSize:16,marginTop:5}}>
                                              Terminos y condiciones
                                        </Text>
                                        <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                        </Text>
                                    </View>
                        </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogVisible}
                >
                    <TouchableWithoutFeedback >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminar</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => evalResponse(0)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => evalResponse(1)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>

            </View>


                
            
       </ScrollView>

    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-350,
        paddingTop:10,
        height:height
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    box: {
        width:120,
        height:140,
        //borderColor: 'black',
        //  borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginRight: 10,
        paddingTop: 15,
        left: -20,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.49,
        shadowRadius: 4.65,
        elevation: 3,
    },
    box1: {
        flexDirection:'row',
        padding: 16,
        width: 440,
        height: 200,
        backgroundColor: "#FFFFFF"    
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
     modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    
})