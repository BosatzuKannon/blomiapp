import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { TextInput as PaperTextInput } from 'react-native-paper';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function InfoBeneficiaryMonthly ({route, navigation}){
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [nameben, setNameben] = useState('')
    const [documentType, setDocumentType] = useState(0)
    const [document, setDocument] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [documentTypes, setDocumentTypes] = useState([
        {
            id:1,
            name:'Cédula de ciudadanía'
        },
        {
            id:2,
            name:'Pasaporte'
        },
        {
            id:3,
            name:'Tarjeta de identidad'
        }
      
    ]);


    const saveBeneficiary = async () => {

        if(nameben.length === 0 || documentType.length === 0 || document.length === 0 || email.length === 0 || phone.length === 0){
            Alert.alert("Información","Debe diligenciar todos los campos para agregar al beneficiario.");
        }else{
            const data = {
                name : nameben,
                document_type: documentType,
                document: document,
                email: email,
                phone: phone
            }

            let ben = []
            ben.push(data)
            await AsyncStorage.setItem("beneficiary", JSON.stringify(ben));            
            navigation.goBack()
        }
    }


    return(

        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>

            <View style={styles.container}>

                <View style={{marginTop: 20}}>
                    <Text style={{fontFamily:'Gotham-Bold', fontSize:24, color:secondary}}>Información del beneficiario</Text>
                </View> 
                {/* <View style={{marginBottom: 30}}>
                    <Text style={{fontFamily:'Gotham-Bold', fontSize:14, color:'#1C7A00', marginTop: 30, textAlign:'right', marginRight:30}}>LLENAR CON AGENDA</Text>
                </View> */}
                {/*form*/}
                <View>
                    <View style={{marginBottom:10}}>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,fontSize:14,}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Nombre" outlineColor={colorInputBorder} onChangeText={text => setNameben(text)}/>
                    </View>
                    <View style={styles.pickerStyle}>
                                <Picker 
                                    selectedValue={documentType}
                                    style={{height: 40, width: width-60,borderColor: secondary,borderWidth: 1}} 
                                    onValueChange={(itemValue, itemIndex) => setDocumentType(itemValue)}>
                                        {
                                            documentTypes.map( (documentType) => {
                                                return <Picker.Item label={documentType.name} color={colorInput} value={documentType.id} key={documentType.id}/>
                                            })
                                        } 
                                </Picker>
                            </View>
                    <View style={{flexDirection:'row', marginBottom:10, width: width-60}}>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,fontSize:14,}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="N° documento" outlineColor={colorInputBorder} onChangeText={text => setDocument(text)}/>
                    </View>
                    <View style={{marginBottom:10}}>
                        <PaperTextInput style={{width: width-50,backgroundColor:surface,color:colorInput,fontSize:14,marginRight:20}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Correo" outlineColor={colorInputBorder} onChangeText={text => setEmail(text)}/>
                    </View>
                    <View>
                            <PaperTextInput style={{width: width-50,backgroundColor:surface,color:colorInput,fontSize:14,marginRight:20}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Télefono" outlineColor={colorInputBorder} onChangeText={text => setPhone(text)}/>
                    </View>
                </View>

            </View>

            <TouchableOpacity style={styles.button} onPress={ () => saveBeneficiary() }>      
                    <Text style={{fontFamily:'Gotham-Bold', color:'#F2FAE0', fontSize: 14, textAlign:'center'}}>CONTINUAR</Text>                
            </TouchableOpacity>
        
        </ScrollView>

    )

}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-270,
        paddingTop:10,
    },
    button: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10, 
        width: 380, 
        alignSelf: 'center', 
        bottom: 20
    },
    pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 60,
        marginTop:10,
        width:width-50,
        marginBottom:10,
        marginRight:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },



})