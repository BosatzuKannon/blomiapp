import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, Alert,Modal,TouchableWithoutFeedback, Switch} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faTrash, faRotate} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
import SumaryPasadia from '../pasadia/sumaryPasadia';

import WebView from 'react-native-webview'
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../../utils/api';
import { PRODUCT } from '../../utils/endpoints';

export default function PurchaseMonthly ({route, navigation}){

    const [isSelected, setSelection] = useState(false);
    const [vehicles, setVehicles] = useState([]);
    const [beneficiary, setBeneficiary] = useState([]);
    const [vehicleId1, setvehicleId1] = useState(null);
    const [typeVehicleId2, setTypeVehicleId2] = useState(null);
    const [licensePlate2, setLicensePlate2] = useState(null);
    const [vehicleId2, setvehicleId2] = useState(null);
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [dialogModalVisible, setDialogModalVisible] = useState(false);
    const [dialogModal1Visible, setDialogModal1Visible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    // const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [dialogReserveVisible,setDialogReserveVisible] = useState(false);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [services, setServices] = useState([]);
    const [price, setPrice] = useState('0');
    const [expanded, setExpanded] = useState(true);
    const [time, setTime] = useState(1);
    const [totalListMonthly, setTotalListMonthly] = useState([]) 
    const [isEnabled, setIsEnabled] = useState(true);   
    const [isEnabledWebView, setIsEnabledWebView] = useState(false); 

    const   { parking, monthly } = route.params

    

    const toggleSwitch =  () => {
        if (isEnabled == true) {
            setIsEnabled(previousState => !previousState);  
            modal()
        }else if (isEnabled == false) {
            setIsEnabled(previousState => !previousState);
            modal1()
        }     
    }

    const terms = () => {
        setDialogTermsVisible(true);
        
    }

    const modal = () => {
        setDialogModalVisible(true);
        console.log('modal desactivado');
    }

    const modal1 = () => {
        setDialogModal1Visible(true);
        console.log('modal activado');
    }

    const _handlePress = () => {
        setExpanded(!expanded);
    }

    let paymentWayInfo = {
        merchant_id: "276",
        form_id: "238",
        terminal_id: "293",
        order_number: "0",
        amount: "0",
        currency: "cop",
        order_description: "prueba blomi",
        client_email: "carlos87jaramillo@gmail.com",
        client_phone: "3175345577",
        client_firstname: "carlos",
        client_lastname: "jaramillo",
        client_doctype: "4",
        client_numdoc: "1087116281",
        response_url: "http://www.test.com/response"
    }

    const formattedPostData = Object.entries(paymentWayInfo)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');

    const htmlContent = `
        <html>
        <body>
            <form id="paymentForm" method="POST" action="https://merchant.paymentsway.co/cartaspago/redirect">
            
                      <input name="merchant_id" type="hidden"  value="1423">
                      <input name="form_id" type="hidden"  value="1565">
                      <input name="terminal_id" type="hidden"  value="1108">
                      <input name="order_number" type="hidden"  value="103">
                      <input name="amount" type="hidden"  value="1">
                      <input name="currency" type="hidden"  value="cop">
                      <input name="order_description" type="hidden"  value="Test de compra blomi">
                      <input name="client_email" type="hidden" value="carlos87jaramillo@gmail.com">
                      <input name="client_phone" type="hidden" value="3175345577">
                      <input name="client_firstname" type="hidden" value="carlos">
                      <input name="client_lastname" type="hidden" value="jaramillo">
                      <input name="client_doctype" type="hidden" value="4">
                      <input name="client_numdoc" type="hidden" value="1087116281">
                      <input name="response_url" type="hidden" value="http://www.test.com/response">
                      <input name="Submit" type="submit"  value="Enviar">
            </form>
            <script>
            document.getElementById('paymentForm').submit();
            </script>
        </body>
        </html>
    `;
        
    const saveMonthly = async () =>{

        
        console.log('pagar mensualidad');
        setIsEnabledWebView(true)

        // setSpinner(true)
        // const data = {
        //     product_id: '1',
        //     product_type_id:'MENSUALIDAD NATURAL',
        //     facility: parking,
        //     vehicles: vehicles,
        //     automatic_suscription:true,
        //     time_bougth:monthly.monthly_cant,
        //     beneficiary: beneficiary
        // }

        // console.log(JSON.stringify(data))
        // navigation.navigate('ActiveMonthly')
        // const request = new Request();
        // const result = await request.request(PRODUCT.createProduct,'POST',data)
        // if(result === 0){
        //     setSpinner(false)
        //     Alert.alert('Error',"No fue registrar la mensualidad");                      
        // }else{
        //     setSpinner(false)
        //     Alert.alert('Exito',result.ResponseMessage);
        //     navigation.goBack(null)

        // }
        // _________________________________________________________________________________________
        // const config = {
        // headers: { Authorization: global.token },  
        // };
        // //setSpinner(true);
        // //const car =  await AsyncStorage.getItem("vehicleTypesId") 
        // //var id = parseInt(global.monthlyId);

        // let data;

        // console.log(listMonthlys);
        // Alert.alert("INFORMACIÓN","Mensualidad creada correctamente");
        // navigation.navigate('ListMonthly');

        // // listMonthlys.map((monthly) => {
            

        // //     data = {
        // //         "vehicles": [],
        // //         "months": 1,
        // //         "subProductId": monthly.monthly_id,
        // //         "parkingId": monthly.parking_id
        // //     }

        // //     monthly.vehicles.map((vehicle) => {
        // //         const val = { id : vehicle.vehicle_id }
        // //         data.vehicles.push(val)
        // //     })

        // //     if(monthly.beneficiaries){

        // //         const dat = {'id': '4', 'beneficiaries': monthly.beneficiaries}

        // //         console.log(`guardar beneficiarios ${JSON.stringify(dat)}`);
        // //     }else{
        // //         console.log(`no guarda beneficiarios`);
        // //     }

        //     //console.log(data);
        //     // axios.post(`${API_URL}customer/app/naturalMonthlyPayment`,data, config).then(async (response) => {
        //     //     setSpinner(false);
        //     //     const cod = response.data.ResponseCode;
        //     //     //console.log(response.data.Data);
        //     //     if(cod === 0){
        //     //         console.log(response.data.ResponseMessage);
        //     //         await payMonthly(response.data.Data.id);
        //     //     }else{
        //     //         Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
        //     //     }  
        //     // }).catch(error => {
        //     //     setSpinner(false);
        //     //     console.log(error.response  );
        //     //     Alert.alert("error prueba",error.response.data.ResponseMessage);
        //     //     console.log(error);
        //     // })   
        // })

        //Alert.alert("Se creó la mensualidad correctamente");
        //navigation.navigate('ListMonthly');
    }

    //método para pagar mensualidad.
    const payMonthly = (id) =>{

        // const config = {
        // headers: { Authorization: global.token },  
        // };
        // setSpinner(true);
        
        // axios.put(`${API_URL}customer/app/naturalMonthlyPayment/pay/${id}`,{}, config).then(response => {
        //     setSpinner(false);
        //     const cod = response.data.ResponseCode;
            
        //     if(cod === 0){

        //         //newMonthly(id);
        //         return ''
        //     }else{
        //         Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
        //     }   
        //     }).catch(error => {
        //     setSpinner(false);
        //     console.log(error.response  );
        //     Alert.alert("error prueba",error.response.data.ResponseMessage);
        //     console.log(error);
        //     }) 
    }

    const saveBeneficiary = () => {
        const config = {
        headers: { Authorization: global.token },  
        };
        setSpinner(true);
        
        axios.put(`${API_URL}customer/app/naturalMonthlyPayment/pay/${id}`,{}, config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            
            if(cod === 0){

                //newMonthly(id);
                return ''
            }else{
                Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
            }   
            }).catch(error => {
            setSpinner(false);
            console.log(error.response  );
            Alert.alert("error prueba",error.response.data.ResponseMessage);
            console.log(error);
            })
    }
        

    const tiempo = (op) =>{
        let cantidad = monthly.monthly_cant
        if( op === 0){
            if(cantidad != 1){
                cantidad = cantidad - 1
                monthly.monthly_cant = cantidad
            }
        }
        else{
            cantidad = parseInt(cantidad) + 1
            monthly.monthly_cant = cantidad
        }
        cargarPrecio()
        // navigation.navigate('PurchaseMonthlys' , {"listMonthlys": totalListMonthly});
    }

    const cargarPrecio = () => {
        let precio = 0

        const sub_total = parseInt(monthly.monthly_cant) * parseInt(monthly.price)
        precio = precio + sub_total

        if(precio.toString().length > 3){
            const start = precio.toString().substring(0,(precio.toString().length -3))
            const end = precio.toString().substring((start.length), precio.toString().length)        
            setPrice(`${start}.${end}`)
        }
        else{
            setPrice(precio)
        }
       
    }

    const unidad = (cant) => {
        if(cant.toString().length > 3){
                const start = cant.toString().substring(0,(cant.toString().length -3))
                const end = cant.toString().substring((start.length), cant.toString().length)        
                return `${start}.${end}`
        }else{
                return cant
        }
    }

    const handleNavigation = (event) => {
        // Verificar si la solicitud es una respuesta de la URL que estás esperando
        console.log('Intento de navegación:', event.url);
        if (event.url.includes('https://www.google.com')) {
          // Aquí puedes manejar la respuesta como desees
          console.log('Respuesta del WebView:', event.url);
          // Puedes extraer información adicional según tus necesidades
          // ...
    
          // Devuelve `false` para evitar que la URL cargue en el WebView
          return false;
        }
    
        // Devuelve `true` para permitir la navegación normal
        return true;
    };

    const toggleModal = () => {
        setIsEnabledWebView(!isEnabledWebView);
      };
    
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus",async () => {
            monthly.monthly_cant = 1
            // const vehicle = await AsyncStorage.getItem("vehicle");

            // if (vehicle !== null) {
            //     setVehicles(JSON.parse(vehicle))
            // }

            // const ben = await AsyncStorage.getItem("beneficiary");
            // setBeneficiary(ben !== null ? JSON.parse(ben) : [])
            // if (ben !== null) {
            //     setBeneficiary(JSON.parse(ben))
            // }
        });
        return unsubscribe;
    }, [navigation]);

    return(
        <ScrollView>
            {/* {
                true ? 
                <View style={{width:width-80, height:height-80}}>
                    <WebView
                        source={{ uri: 'https://www.google.com' }}
                        onShouldStartLoadWithRequest={handleNavigation}
                    />
                </View> : <View></View>
            } */}
            
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:0,flexDirection:'row' ,alignContent:'center',backgroundColor:surface,marginTop:0}}>
                <TouchableOpacity onPress={() => navigation.navigate('Mensualidad')}>
                    <MaterialIcons style={{marginTop:5}} name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
        
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:40, marginTop:20}}>Mensualidades</Text>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:18, color:secondary, marginBottom:40, marginTop:20}}>Selecciona la cantidad de meses</Text>

                   {/*  tarjeta mensualidades */}

                   <View style={{marginBottom:20}} >
                   {/* {   totalListMonthly.map((monthly, index) =>{ */}
                        <TouchableOpacity style={{marginTop:20}}>
                                    <View style={{flexDirection:'row'}} >
                                        <View style={{flexDirection:'row',marginRight:10, marginTop:18}}>
                                            <FontAwesomeIcon size={25} icon={monthly.vehicleType.split(" / ")[0] === 'Carro' ? faCarSide : monthly.vehicleType.split(" / ")[0] === 'Moto' ? faMotorcycle : faBicycle } color={secondary} /> 
                                            <Text> + </Text> 
                                            <FontAwesomeIcon size={25} icon={monthly.vehicleType.split(" / ")[0] === 'Carro' ? faCarSide : monthly.vehicleType.split(" / ")[0] === 'Moto' ? faMotorcycle : faBicycle } color={secondary} /> 
                                        </View>
                                        <View style={{marginRight:30}}>
                                        
                                            <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>{monthly.name}</Text>
                                            <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color:secondary}}>${unidad(monthly.price)}</Text>
                                            <Text style={{ fontFamily: 'Gotham-Bold', fontSize:14, color:'#1C7A00'}}>Eliminar  <FontAwesomeIcon size={15} icon={faTrash} color={'#1C7A00'}/> </Text>
                                        </View>
                                        <View style={{flexDirection:'row', backgroundColor:'#FFF', marginTop:12, height:32, padding:5, borderRadius:16,borderWidth:1, borderColor:'#90D400',justifyContent:'flex-end',marginLeft:'auto' ,right:15,}}>
                                            
                                                <Text style={{ fontSize:20, color:secondary, fontFamily:'Gotham-Bold'}} onPress={()=> tiempo(0)}> - </Text>
                                                <Text style={{ fontSize:18, color:secondary, fontFamily:'Gotham-Bold'}}> {monthly.monthly_cant ? monthly.monthly_cant : 1} </Text>
                                                <Text style={{ fontSize:18, color:secondary, fontFamily:'Gotham-Bold'}} onPress={()=> tiempo(1)}> + </Text>
                                                
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            {/* }) */}

                    {/* } */}
                   </View>

                     {/* medio de pago */}
                     <View style={{ display: 'flex', justifyContent: 'center'}}>
                     <NativeBaseProvider>
                        <Center flex={1}>   
                            <ScrollView>
                                <Box m={3} style={{border:0 ,left: -25 }}>
                                    <Accordion allowMultiple _text={{border:0}} border={0} onTouchStart={_handlePress}>
                                        <Accordion.Item _text={{color:secondary}} >
                                            <Accordion.Summary  _expanded={{backgroundColor:surface,}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary}}>Medio de pago</Text>
                                                    <View style={{marginLeft:160}}>
                                                        <MaterialIcons name={expanded ? "keyboard-arrow-down":"keyboard-arrow-up"} size={30} color={secondary} />
                                                    </View> 
                                                </View>
                                            </Accordion.Summary>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="card-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:20}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Nombre del titular</Text>
                                                        </View>
                                                            <Text style={{right:40, fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                                                    </View> 
                                                </Accordion.Details>
                                                <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ionicons name="sync-outline" size={24} color={secondary} />
                                                        <View style={{marginLeft:20}}>
                                                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Renovación automática</Text>
                                                            <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Mensual</Text>
                                                        </View>
                                                        <View style={{right:40, marginTop: 5,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                                        <Switch
                                                            trackColor={{ false: "#767577", true: "rgba(145, 212, 0, 0.38)" }}
                                                            thumbColor={isEnabled ? "#90D400" : "#f4f3f4"}
                                                            ios_backgroundColor="#3e3e3e"
                                                            onValueChange={toggleSwitch}
                                                            value={isEnabled}
                                                            onPress={modal}
                                                            />
                                                        </View>                                             
                                                           {/*  <Ionicons name="toggle-outline" size={24} color={secondary} style={{ fontSize: 24,right:20, marginTop: 5,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}} /> */}
                                                    </View> 
                                                </Accordion.Details>
                                        </Accordion.Item>
                                    </Accordion>
                                </Box>
                            </ScrollView>
                        </Center>
                    </NativeBaseProvider>
                     </View>                    
                    <View style={styles.container2}>
                            <View style={{flexDirection:'row'}}>
                                <Checkbox
                                    value={isSelected}
                                    onValueChange={setSelection}
                                    style={{ alignSelf: "center",marginLeft:10,color:primary800, marginRight:10}}
                                    color={isSelected ? primary600 : surfaceMediumEmphasis}
                                />
                                <TouchableOpacity onPress={terms}>
                                    <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Bold',marginTop:10,textDecorationLine:'underline',bottom:4}}>Acepto terminos y condiciones</Text>
                                </TouchableOpacity>
                        </View>

                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={dialogReserveVisible}
                        >
                    <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title2}>Descripcion opcional</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center',marginTop:10}}>
                                    <Text style={styles.text2}>Descripción opcional.....</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={()=> navigation.navigate('SumaryMonthly')}>
                                        <Text style={styles.titleButton1}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>



                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminos y condiciones</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* modal cancelacion mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModalVisible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas cancelar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal cancelacion automatica */}
                {/* modal habilitar mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModal1Visible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas habilitar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal renovación automatica */}
               
                <View style={{marginVertical:20,alignItems:'center',}}>
                    {
                        isSelected ? 
                        <View style={{ flexDirection: 'row'}}>
                           
                            <TouchableOpacity  style={{backgroundColor: "#005A6D",padding: 14,borderRadius: 15,width: 379, height:48}} onPress={() =>  saveMonthly()}>
                                <View>
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: "#F2FAE0",fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR $ {price}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{ flexDirection: 'row'}}>
                            
                            <TouchableOpacity disabled style={{backgroundColor: "rgba(0, 45, 51, 0.15)",padding: 14,borderRadius: 15,width: 379, height:48}}>
                               
                                    <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                        <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR $ {price}</Text>
                                    </View>
                                      
                            </TouchableOpacity>
                        </View>
                        
                    }
                </View>



                    </View>
            </View>
            <Modal
                animationType="slide"
                transparent={false}
                visible={isEnabledWebView}
                onRequestClose={toggleModal}
                style={{width:width-80,height:height-80}}
            >
                <View style={{ flex: 1,   }}>
                    <TouchableOpacity onPress={() => setIsEnabledWebView(!isEnabledWebView)}>
                        <MaterialIcons style={{marginTop:10,marginLeft:10,marginBottom:10}} name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                    <WebView
                        source={{ html: htmlContent }}
                        onShouldStartLoadWithRequest={handleNavigation}
                    />
                </View>
            </Modal>
            
        </ScrollView>

    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-50,
        paddingTop:0,
        height:height-30
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    box: {
        width:120,
        height:127,
        //borderColor: 'black',
        //  borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginRight: 10,
        left: -20,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.49,
        shadowRadius: 4.65,
        elevation: 3,
    },
    box1: {
        flexDirection:'row',
        padding: 16,
        width: 440,
        height: 200,
        backgroundColor: "#FFFFFF"    
    },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        alignContent: 'center',
        justifyContent: 'center',
        marginLeft:80
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },

})