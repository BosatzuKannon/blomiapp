import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../../utils/api';
import { VEHICLE } from '../../utils/endpoints';

export default function ListVehicleMonthly ({route, navigation}){
    const [spinner, setSpinner] = useState(false);
    const [vehicles, setVehicles] = useState([]);
    // const [where, setWhere] = useState('0')

    const   { parking, monthly } = route.params
    
    //Obtener vechiculos de usuario 
    const getVehicles = async () => {
        setSpinner(true)
        const request = new Request();
        const result = await request.request(VEHICLE.listVehiclePerCustomer,'POST',null)
        if(result === 0){
            setSpinner(false)
            Alert.alert('Error',"No es posible consultar la información del parqueadero");                      
        }else{
            setSpinner(false)
            setVehicles(result.ResponseData)
        }        
    }

    //Guardar vehiculo seleccionado y enrutar al resumen de la mensualidad
    const saveVehicle = async (vehicle) =>{
        const temp = await AsyncStorage.getItem("vehicle");

        if (temp !== null) {
            let vehiclestg = JSON.parse(temp)
            vehiclestg.push(vehicle)
            await AsyncStorage.setItem("vehicle", JSON.stringify(vehiclestg));
        } else {
            let vehiclestg = []
            vehiclestg.push(vehicle)
            await AsyncStorage.setItem("vehicle", JSON.stringify(vehiclestg));
        }
        navigation.navigate('SumaryMonthly' , {'parking' : parking, 'monthly':monthly });
    }   

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getVehicles();
        });
        return unsubscribe;
    }, [navigation]);

    return(

       <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => { navigation.goBack()}}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>

            <View style={styles.container}>

                <View style={{ marginBottom:15 }}> 
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginTop:15 }}>Selecciona Vehículo(s)</Text>
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "color: rgba(0, 45, 51, 0.6)"}}>Descripción opcional</Text>
                </View> 
            
            {
                vehicles.map((vehicle) =>{
                    return <TouchableOpacity key={vehicle.id} onPress={() => saveVehicle(vehicle)}>
                     <View style={{
                        backgroundColor: surface,
                        borderRadius: 5,
                        width:width-40,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        paddingTop:5,
                        }}
                    >
                    <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}></Text>
                            {
                                <FontAwesomeIcon size={30} icon={vehicle.vehicle_type === 'CARRO' ? faCarSide : vehicle.vehicle_type === 'MOTO' ? faMotorcycle : faBicycle } color={secondary} style={{marginLeft:10,marginTop:20,marginRight:20}}/>
                            }
                                <View>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                        {vehicle.vehicle_type === 'CARRO' ? "Carro" : vehicle.vehicle_type === 'MOTO' ? "Moto" : "Bicicleta" }
                                    </Text>
                                    <Text style={{textTransform: 'uppercase', fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5}}>
                                        {vehicle.vehicle_plate}   
                                    </Text>
                                </View>
                        </View>
                    </View>
                </TouchableOpacity> 


                })
                }
                <View style={{ flexDirection: 'column', width:410, height:134, color:"#FFFFFF", padding: 16, }}>
                    <TouchableOpacity onPress={ () => navigation.navigate('AddVehicle')}>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:14, color:"rgba(0, 45, 51, 0.6)", alignSelf: 'center', marginTop:15, marginRight:30}}>REGISTRAR NUEVO VEHÍCULO</Text>
                    </TouchableOpacity>
                </View>
            </View>

       </ScrollView>



    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        
        paddingTop:10,
    },
    button: {
        width: 378,
        height: 48,
        backgroundColor: '#005A6D',
        borderRadius: 15,
        left: -20,
        marginBottom: 10   
    }
 })  