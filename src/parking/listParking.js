import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { TextInput as PaperTextInput } from 'react-native-paper';
import {productIdReserve,productIdPasadia,productIdCortesia,productIdBeParking} from '../utils/varService';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListParking({navigation}) {
const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
const [spinner,setSpinner] = useState(false);
const [token, setToken] = useState(null);
const [listParking, setListParking] = useState([]);
const [filterList , setFilterList] = useState('');


const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        global.token = value;
        setToken(value);
      }
    } catch (error) { }
  };
  ///Método para traer los parqueaderos activos
  const getParkingList = () =>{
      var idProduct = null;
    if(global.listParkingCourtesy){
        idProduct = productIdCortesia;
    }
    if(global.listParkingBeParking){
        idProduct = productIdBeParking;
    }
    setSpinner(true);

    let arrayParking = {
        "productId":21
    }

    axios.get(`${API_URL}user/parking/list`, arrayParking).then(response => {
    setSpinner(false);
      const cod = response.data.ResponseCode;
      if(cod === 0){
        const list = response.data.ResponseMessage;
        setListParking(list);
      }else{
        Alert.alert("ERROR","No se pudo traer la lista de parqueaderos");
      }
    }).catch(error => {
      setSpinner(false);
      Alert.alert("",error.response.data.ResponseMessage);
      console.log(error);
    })
  }
const searchTextList = (text) => {
    setFilterList(text);
}
useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getToken();
      //getParkingList();
    });
    return unsubscribe;
  }, [navigation]);


    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.goBack(null)}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ flex: 1, paddingTop: 20,backgroundColor: surface,paddingLeft:16,paddingRight:16,marginBottom:10,paddingBottom:20}}>
            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>Lista parqueaderos habilitados</Text>
            <PaperTextInput style={{width:width-40,backgroundColor:surface,color:colorInput,borderRadius:10}} 
                        right={<PaperTextInput.Icon 
                                name={() => <MaterialIcons name="search" size={30} color={secondary} />} />}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Buscar" outlineColor={colorInputBorder} onChangeText={(text) => searchTextList(text)}/>
            {
                listParking.length !== 0 &&

                listParking.filter((parking) =>{
                          if(filterList == ""){
                            return parking
                          }else if(parking.commercial_name.toLowerCase().includes(filterList.toLowerCase())){
                              return parking
                          }
                        }).map( (parking) => {
                    return <TouchableOpacity style={{justifyContent:'center'}} key={parking.id} disabled >
                                <View
                                style={{
                                    backgroundColor: surface,
                                    borderRadius: 5,
                                    width:width-40,
                                    marginLeft: 0,
                                    marginRight: 1,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                    width: 0,
                                    height: 1,
                                    },
                                    shadowOpacity: 0.49,
                                    shadowRadius: 4.65,
                                    elevation: 3,
                                    paddingBottom:10,
                                    marginTop:10,
                                    paddingTop:5,
                                }}
                                >
                                    <View style={{marginBottom:10,marginLeft:15}}>
                                        <View>
                                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize' }}>
                                                {parking.commercial_name}
                                            </Text>
                                            {
                                                parking.address !== null ? 
                                                <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                    {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}
                                                </Text>
                                                :
                                                <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                    Este parqueadero no tiene dirección
                                                </Text>

                                            }
                                            
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                    })
            }
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingHorizontal:20,
      paddingBottom:20,
      paddingTop:10,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 10,
        borderRadius: 12,
        width: width-180,
        marginTop:10,
        alignItems: "center",
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })