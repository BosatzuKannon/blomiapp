import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,Image,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { TextInput as PaperTextInput } from 'react-native-paper';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function RegisterVehicle ({navigation}) {
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [document, setDocument] = useState(null);
    const [modalCancel, setModalCancel] = useState(false);
    const [typeDocumentId, setTypeDocumentId] = useState(null);
    const [code, setCode] = useState('');
    const [typeDocument, setTypeDocument] = useState([
        {
            name:'Cédula de ciudadania',
            id:1,
        },
        {
            name:'Documento extranjero',
            id:2,
        },
        {
            name:'NIT',
            id:3,
        },
        {
            name:'Pasaporte',
            id:4,
        },
        {
            name:'Rut',
            id:5,
        },
    ]);
    
    //validacion de código
    const goToValet = (token) => {
        if (code === null || code === '') {
            Alert.alert('Debe digitar un código')
            
        } else if (code == '1111') {
            Alert.alert('codigo no existente')
        } else if (code == '1234') {

            Alert.alert('Código valet correcto')
            navigation.navigate('ConsolidatedValet')
        }
        
        let config = {
            headers: { Authorization: token }
            };
        axios.get(`${API_URL}customer/app/valet/verifyValetCode/${code}`, config).then(response => {
            
            const cod = response.data.ResponseCode;
            console.log(`Archivo config __________ ${JSON.stringify(response.data.ResponseMessage)}`)
            setSpinner(false);
            if(cod === 0){
                const valetId = response.data.ResponseMessage.id
                AsyncStorage.setItem("valetId", valetId.toString())
                console.log(`${valetId} -- prueba`);
                console.log('envio de informacion a consolidated');  
                // navigation.navigate('ConsolidatedValet', {valetId})
                getInfoValet(valetId)
            }else{
              Alert.alert("ERROR",response.data.ResponseMessage);
            } 
            }).catch(error => {
            setSpinner(false);
            console.log(error);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            console.log(error);
            })

        //console.log('envio a valet');
    }
    //trae la informacion de valet
    const  getInfoValet = (valetId) => {

        //const valetId = await AsyncStorage.getItem("valetId")
        //console.log('pre '+ valetId);
         axios.get(`${API_URL}customer/app/valet/getCustomerValetInfo/${valetId}`).then(response => {
              
            const cod = response.data.ResponseCode;
            //console.log(response.data.ResponseCode);
            //console.log(`Archivo config __________ ${JSON.stringify(response.data.ResponseMessage)}`)
            setSpinner(false);
            if(cod === 0){
                let infor = response.data.ResponseMessage
                console.log(`respuesta --- ${JSON.stringify(infor)}`) 
                navigation.navigate('ConsolidatedValet', {infoValet:infor})
            }else{
              Alert.alert("ERROR",'error');
            } 
            }).catch(error => {
            setSpinner(false);
            console.log(error);
            Alert.alert("error11",);
            console.log(error.response.data.ResponseMessage);
            console.log(error);
            }) 
    }
    //Metodo para obtener el token
    const getToken = async () => {
        try {
            const value = await AsyncStorage.getItem("token");
            if (value !== null) {
                global.token = value;
                setToken(value);
                // global.parkId =  await AsyncStorage.getItem('parkingId');
                // global.monthlyId =  await AsyncStorage.getItem('monthlyId');
                // setLicensePlate1(await AsyncStorage.getItem('vehiclePlate1'));
                // setTypeVehicleId1(await AsyncStorage.getItem('vehicleTypeId1'));
                // setLicensePlate2(await AsyncStorage.getItem('vehiclePlate2'));
                // setTypeVehicleId2(await AsyncStorage.getItem('vehicleTypeId2'));
                // getParkingInfo();
                // getMonthlyInfo(value);

                console.log(`Llega listmonthly -- ${JSON.stringify(listmonthlys)}`);
            }
        } catch (error) { }
    };
    const goToActive = () => {

        //const valetId = await AsyncStorage.getItem("valetId")
        //console.log('pre '+ valetId);
        axios.get(`${API_URL}customer/app/valet/getCustomerValetInfo/427`).then(response => {
              
            const cod = response.data.ResponseCode;
            //console.log(response.data.ResponseCode);
            //console.log(`Archivo config __________ ${JSON.stringify(response.data.ResponseMessage)}`)
            setSpinner(false);
            if(cod === 0){
                let infor = response.data.ResponseMessage
                console.log(`respuesta --- ${JSON.stringify(infor)}`) 
                navigation.navigate('ActiveValet1', {infoValet:infor})
            }else{
              Alert.alert("ERROR",'error');
            } 
            }).catch(error => {
            setSpinner(false);
            console.log(error);
            Alert.alert("error11",);
            console.log(error.response.data.ResponseMessage);
            console.log(error);
            }) 

        //navigation.navigate('ActiveValet1')       
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
        });
        return unsubscribe;
    }, [navigation]);
    return (
        <ScrollView>
             <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Valet')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
            <View style={styles.container}>
                <Text style={{color:secondary,fontSize:30,fontFamily:'Gotham-Bold',marginTop:10, marginBottom:30}}>Bienvenido a valet parking</Text>
                <Text style={{color:'rgba(0, 45, 51, 0.8)', fontFamily:'Gotham-Medium', fontSize:18}}>Para comenzar</Text>
                <Text style={{ marginBottom:30, fontFamily:'Gotham-Medium', fontSize:16, color:'rgba(0, 45, 51, 0.6)' }}>ingresa el código enviado a tu celular</Text>
                <View>
{/*                     <View style={styles.pickerStyle}>
                          <Picker 
                                selectedValue={typeDocumentId}
                                style={{height: 40, width: width-60,borderColor: secondary,borderWidth: 1}} 
                                onValueChange={(itemValue, itemIndex) => setTypeDocumentId(itemValue)}>
                                    <Picker.Item label="documento" color={colorInput} value={null} enabled={false}/>
                                    {
                                    typeDocument.map( (type) => {
                                    return <Picker.Item label={type.name} color={colorInput} value={type.id} key={type.id}/>
                                    })
                                    }  
                            </Picker>
                    </View> */}
                    <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:20,fontSize:14}} 
                        onChangeText={(text) => {setCode(text)}} 
                        keyboardType='text'
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Código" outlineColor={colorInputBorder}/>   
                </View>
{/*                 <TouchableOpacity onPress={() =>navigation.navigate('ConsolidatedValet')}>
                    <Text>prueba</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() =>navigation.navigate('ValetCompleted')}>
                    <Text>complete</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() =>navigation.navigate('ActiveValet')}>
                    <Text>Activate</Text>
                </TouchableOpacity>
                  <TouchableOpacity onPress={() =>navigation.navigate('RequestVehicle')}>
                    <Text>requets</Text>
                </TouchableOpacity> */}
                <View style={styles.container2}>
                    <View style={{marginVertical:10,alignItems:'center',alignContent:'center',justifyContent:'center'}}>  
                        <TouchableOpacity style={styles.button1} onPress={() => goToValet()}>
                                <Text style={styles.titleButton1}>VALIDAR</Text>
                        </TouchableOpacity>
                        {/*  <TouchableOpacity onPress={() => setModalCancel(true)}>
                            <Text style={{color: surfaceMediumEmphasis,fontSize: 16,fontFamily:'Gotham-Bold',textAlign:'center',marginTop:20,fontStyle:'italic'}}>CANCELAR</Text>   
                        </TouchableOpacity> */}
                    </View>
                    
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalCancel}
                    >
                        <TouchableWithoutFeedback onPress={() => setModalCancel(!modalCancel)}>
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <Text style={styles.title}>¿Estas seguro de cancelar el servicio?</Text>
                                    <Text style={styles.text2}>Si existen inconsistencias en la informacion enviada, puedes solicitar el servicio de nunevo.</Text>
                                    <View style={{marginBottom:5}}>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setModalCancel(!modalCancel)}>
                                            <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR SERVICIO</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </Modal>
                </View>
                <View style={{ justifyContent: 'center', alignItems:'center' }}>

                    <Image source={require('../../../assets/reserve/ilustracion_paseo1.png')}
                        style={{height:180}}
                    ></Image>
                    
                </View>
                <View style={{

    backgroundColor: surface,
    borderRadius: 10,
    width:width-38,
    marginLeft: 0,
    marginRight: 1,
    shadowColor: "#000",
    shadowOffset: {
    width: 0,
    height: 1,
    },
    shadowOpacity: 0.49,
    shadowRadius: 4.65,
    elevation: 2,
    marginBottom:5,
    marginTop:10,
    padding:15,
}}
            >
                    <Text onPress={() => goToActive()} style={{fontSize:18,fontFamily:'Gotham-Bold',color:secondary }}>Tienes 1 valet activo</Text>
                </View>
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-200,
        paddingTop:10,
        paddingRight: 20,
    },
     pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 56,
        marginVertical:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
        width:360
    },
     container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:10,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      titleButton1: {
        color: '#fff',
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
      },
      centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
       modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 15,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text2: {
        color: surfaceHighEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:20,
        marginTop:20,
        textAlign:'center',
    },
    title: {
        color: secondary,
        fontSize: 17,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
    },
})