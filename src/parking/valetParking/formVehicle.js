import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { TextInput as PaperTextInput } from 'react-native-paper';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { log } from 'react-native-reanimated';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function FormVehicle ({navigation}) {
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [document, setDocument] = useState(null);
    const [modalCancel, setModalCancel] = useState(false);
    const [typeDocumentId, setTypeDocumentId] = useState(null);
    const [typeDocument, setTypeDocument] = useState([
        {
            name:'C.C',
            id:1,
        },
        {
            name:'Documento extranjero',
            id:2,
        },
        {
            name:'NIT',
            id:3,
        },
        {
            name:'Pasaporte',
            id:4,
        },
        {
            name:'Rut',
            id:5,
        },
    ])
    
    return (
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('RegisterVehicle')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <Text style={{color:secondary,fontSize:24,fontFamily:'Gotham-Bold',marginTop:10}}>Ingresar vehículo</Text>
                    <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Medium',marginTop:10}}>Diligencie los siguientes campos para ingreso de vehículo</Text>
                    <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Medium',marginTop:10}}>Datos del cliente</Text>
                    <View style={{marginTop:10}}>
                         <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:10,fontSize:14}} 
                            onChangeText={(text) => {setDocument(text)}} 
                            value={document} keyboardType='default'
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Nombres" outlineColor={colorInputBorder}/>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:5,fontSize:14}} 
                            onChangeText={(text) => {setDocument(text)}} 
                            value={document} keyboardType='default'
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Apellidos" outlineColor={colorInputBorder}/>
                        <View style={{flexDirection:'row'}}>
                            <View style={styles.pickerStyle}>
                                <Picker 
                                    selectedValue={typeDocumentId}
                                    style={{height: 40, width:150,borderColor: secondary,borderWidth: 1}} 
                                    onValueChange={(itemValue, itemIndex) => setTypeDocumentId(itemValue)}>
                                        <Picker.Item label="documento" color={colorInput} value={null} enabled={false}/>
                                        {
                                        typeDocument.map( (type) => {
                                        return <Picker.Item label={type.name} color={colorInput} value={type.id} key={type.id}/>
                                        })
                                        }  
                                </Picker>
                            </View>
                            <View>
                                <PaperTextInput style={{width:width-210,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:5,fontSize:14, marginTop:13}} 
                                    onChangeText={(text) => {setDocument(text)}} 
                                    value={document} keyboardType='numeric'
                                    theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                    mode='outlined' label="Número" outlineColor={colorInputBorder}/>
                            </View> 
                        </View>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:10,fontSize:14}} 
                            onChangeText={(text) => {setDocument(text)}} 
                            value={document} keyboardType='numeric'
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Número de celular" outlineColor={colorInputBorder}/>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:10,fontSize:14}} 
                            onChangeText={(text) => {setDocument(text)}} 
                            value={document} keyboardType='email-address'
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Correo electronico" outlineColor={colorInputBorder}/>                      
                    </View>
                    <Text style={{color:secondary,fontSize:16,fontFamily:'Gotham-Medium',marginTop:10}}>información del vehículo</Text>
                    <Text style={{color:'rgba(0, 45, 51, 0.6)',fontSize:14,fontFamily:'Gotham-Medium',marginTop:10}}>Verifica el estado del vehículo y selecciona los posibles elementos encontrados</Text>
                    <View style={{marginTop:10}}>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:10,fontSize:14}} 
                            onChangeText={(text) => {setDocument(text)}} 
                            value={document} keyboardType='default'
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Ingrese número de placas" outlineColor={colorInputBorder}/>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,borderRadius:10,marginBottom:10,fontSize:14}} 
                            onChangeText={(text) => {setDocument(text)}} 
                            value={document} keyboardType='default'
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Desprendible de llaves" outlineColor={colorInputBorder}/>
                    </View>
                    <Text style={{color:secondary,fontSize:10,fontFamily:'Gotham-Medium',marginTop:10}}>ESTADO DEL VEHÍCULO</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('VehicleImperfections')}>
                        <View
                            style={{
                                backgroundColor: 'rgba(0, 45, 51, 0.15)',
                                borderRadius: 10,
                                width:width-38,
                                marginLeft: 0,
                                marginRight: 1,
                                shadowColor: "rgba(0, 45, 51, 0.15)",
                                shadowOffset: {
                                width: 0,
                                height: 1,
                                },
                                shadowOpacity: 0.49,
                                shadowRadius: 4.65,
                                elevation: 2,
                                marginBottom:10,
                                marginTop:10,
                                padding:15,
                            }}
                            >
                            <View style={{flexDirection: 'row'}}>
                                {/* <MaterialCommunityIcons style={{marginTop:10}} name="alert-circle-outline" size={24} color={errorColor} /> */}
                                <MaterialIcons style={{marginTop:10}} name="warning" size={24} color={'black'} />
                                <View style={{marginLeft:10}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Registra estado vehículo</Text>
                                    <Text style={styles.text}>No hay registros</Text>
                                </View>
                                <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>MODIFICAR</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <Text style={{color:secondary,fontSize:10,fontFamily:'Gotham-Medium',marginTop:10}}>ELEMENTOS DENTRO DEL VEHÍCULO</Text>
                </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:width-100,
        paddingTop:10,
        paddingRight: 20,
    },
     pickerStyle: {
        borderRadius:10,
        padding:5,
        marginRight:10,
        height: 56,
        marginVertical:20,
        borderColor: colorInputBorder,
        borderWidth: 1,
        width:150
    },
})