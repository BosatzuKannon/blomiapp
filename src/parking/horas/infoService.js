import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,LogBox,Image,TouchableWithoutFeedback, Switch} from 'react-native';
import CountDown from 'react-native-countdown-component';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import QRCode from 'react-native-qrcode-svg';
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500,onSurfaceOverlay8 } from '../../utils/colorVar';
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import { marginTop } from 'styled-system';
LogBox.ignoreLogs(['Non-serializable values were found in the navigation state'],["Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function."]);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function InfoService({route, navigation}) {
    const [isSelected, setSelection] = useState(false);
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [expanded, setExpanded] = useState(true);
    const [hour, setHour] = useState();    
    const [hourFinal, setHourFinal] = useState();
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [plate, setPlate] = useState(null);
    const [duration, setDuration] = useState(null);
    const [price, setPrice] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [qrInfo, setQrInfo] = useState(null);
    const [date, setDate] = useState(null);
    const [timeElapsed, setTimeElapsed] = useState(null);
    const [dialogReserveVisible, setDialogReserveVisible] = useState(false);
    const [isEnabled, setIsEnabled] = useState(false);
    const [dialogModalVisible, setDialogModalVisible] = useState(false);
    const [dialogModal1Visible, setDialogModal1Visible] = useState(false);

    const {service} = route.params;

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        //var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/20`).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            console.log(`${JSON.stringify(response.data.ResponseMessage)}`);
            if(cod === 0){
                
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    //Mostrar acordion
    const _handlePress = () => {
        setExpanded(!expanded);
    }
    //Obtener tiempo trasncurrido
    const getTime = (until) =>{
        setTimeElapsed(until);
    }
    const toggleSwitch =  () => {

        if (isEnabled == true) {
            setIsEnabled(previousState => !previousState);  
            modal()
        }else if (isEnabled == false) {
            setIsEnabled(previousState => !previousState);
            modal1() 
            
        }     
    }
    const modal = () => {
        setDialogModalVisible(true);
        console.log('modal desactivado');
    }
    const modal1 = () => {
        setDialogModal1Visible(true);
        console.log('modal activado');
    }

    ////Ir tiempo extendido
    const goToTimeExtension = (min) => {
        global.timeElapsed = timeElapsed;
        global.attachedTime = min;
        console.log(global.idReserva)
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/validate/schedule/${global.idReserva}/${min}`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setDialogReserveVisible(true);
            }else{
              Alert.alert("ERROR","No se en extender el tiempo");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }
    const goToExtension = () => {
        setDialogReserveVisible(false);
        navigation.navigate('TimeExtensionReserve');
    }
    //Metodo para traer data de la reserva
    const getDataReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/get/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                var date = moment(response.data.ResponseMessage.initial_date).format('MMMM DD YYYY');
                var timeI = moment(response.data.ResponseMessage.initial_date).format('HH:mm');
                setHour(timeI);
                var timeF = moment(response.data.ResponseMessage.final_date).format('HH:mm');
                setDuration(response.data.ResponseMessage.duration);
                setHourFinal(timeF);
                setDate(date);
                setPrice(response.data.ResponseMessage.price);
                setTypeVehicleId(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.vehicle_type_id);
                setPlate(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.plate);
            }else{
              Alert.alert("ERROR","No se en traer datos de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
        
      };

      const getTimeReserve = () =>{
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/reservation/time/left/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setTotalDuration(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo que queda de la reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }
    //Finalizar reserva
    const finishReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.put(`${API_URL}customer/app/reservation/finish/`+global.idReserva,{},config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                Alert.alert("",response.data.ResponseMessage);
                navigation.navigate('ReserveCompleted');
            }else{
              Alert.alert("ERROR","No se pudo traer el tiempo que queda de la reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            // console.log(`servicio ${JSON.stringify(service)}`)
            const dataQr = {
                facility_id : service.facility.id,
                product_id: service.product_id.toString(),
                product_type_id: service.product_type_id.toString(),
                vehicle: service.vehicles,
                customer: service.customer,
                created: service.create
            }
            console.log(`servicio ${JSON.stringify(dataQr)}`)
            setQrInfo(dataQr)
        
        });

      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, padding:20, paddingTop:10,}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'center'}}>Servicio de parqueo activo</Text>
                    <View style={{alignItems:'center'}}>
                        <Image
                        source={require("../../../assets/reserve/ilustracion_paseo2.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <Text style={{color: secondary,fontSize: 15,fontFamily: 'Gotham-Light',textAlign:'center'}}>Presenta este codigo en la  salida del punto de servicio para realizar la facturación</Text>
                    <View style={{alignItems:'center', marginTop:20, marginBottom:10}}>
                        {/* <Image
                        source={require("../../../assets/reserve/qrcode1.png")}
                        style={{ marginVertical:20 }}></Image> */}
                        <QRCode
                                value={ JSON.stringify(qrInfo) }
                                size={ 180 }
                                color= 'white'
                                backgroundColor='#519B00'
                            >
                        </QRCode>
                    </View>    
                    <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <View >
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                        {
                                            service !== null ?
                                            service.facility.name
                                            :
                                            ""
                                        }
                                    </Text>
                                    {
                                        service !== null ?
                                            service.address !== null ? 
                                                <Text style={styles.text}>{service.facility.address}</Text>
                                            :
                                            <Text style={styles.text}>No hay dirección disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    }
                                    <View style={{flexDirection: 'row'}} >
                                        {
                                            parking !== null &&
                                                parking.open == 1 ? 
                                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                                :
                                                // <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                        }
                                    </View>
                                </View>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                    <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                                </View>
                            </View>
                        </View> 
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:22, paddingTop:10,marginTop:16}}> 
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:10}}>Detalles del servicio</Text> 
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Fecha</Text>
                        <Text style={styles.textBold}>{moment(service.created).format('YYYY-MM-DD HH:mm:ss')}</Text>
                    </View>
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Tipo de servicio</Text>
                        <Text style={styles.textBold}>Horas</Text>
                    </View>                    
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Tipo de vehículo</Text>
                            <Text style={styles.textBold}>
                                {service.vehicles.vehicle_type} | {service.vehicles.vehicle_plate.toUpperCase()}
                            </Text>
                        </View>                        
                    </View>
                </View>
                {/* modal cancelacion mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModalVisible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas cancelar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModalVisible(!dialogModalVisible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal cancelacion automatica */}
                {/* modal habilitar mensualidad automatica */}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogModal1Visible}
                    >
                    <TouchableWithoutFeedback onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={{color: secondary, fontFamily:'Gotham-Bold',fontSize:18, textAlign: 'center'}}>¿Deseas habilitar la renovación automatica?</Text>
                                <Text>Descripción opcional</Text>
                                <View style={{marginBottom:10, marginTop:20, flexDirection:'row'}}>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: '#fff',padding: 14,borderRadius: 10, width: width-230,marginRight:5, borderWidth: 1, borderColor: secondary}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: secondary, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CANCELAR</Text>   
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-230,}} onPress={() => setDialogModal1Visible(!dialogModal1Visible)}>
                                            <Text style={{color: '#fff', fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>CONFIRMAR</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* fin modal renovación automatica */}
                <Modal
                animationType="slide"
                transparent={true}
                visible={dialogReserveVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title2}>Tienes 10 minutos para completar tu extensión</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.text2}>Descripción opcional.....</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={goToExtension}>
                                        <Text style={styles.titleButton1}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* renovación automatica */}
                {/* <View style={{ backgroundColor: '#F2FAE0',
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 20,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,flexDirection: 'row'}}>
                    <Ionicons name="sync-outline" size={24} color={secondary} />
                    <View style={{marginLeft:20}}>
                        <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Renovación automática</Text>
                        <Text style={{ fontFamily:'Gotham-Bold', fontSize:14, color: "rgba(3, 25, 27, 0.75)" }}>Mensual</Text>
                    </View>
                    <View style={{right:40, marginTop: 5,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                    <Switch
                        trackColor={{ false: "#767577", true: "rgba(145, 212, 0, 0.38)" }}
                        thumbColor={isEnabled ? "#90D400" : "#f4f3f4"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={toggleSwitch}
                        value={isEnabled}
                        onPress={modal}
                        />
                    </View>
                </View>  */}
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        // textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign: "center",
        justifyContent:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-50,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })