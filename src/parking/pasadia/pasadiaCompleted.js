import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,Image} from 'react-native';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import CountDown from 'react-native-countdown-component';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function PasadiaCompleted({navigation}) {
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [nowDate, setNowDate] = useState(null);
    const [date, setDate] = useState(new Date());
    const [validity, setValidity] = useState(null);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };

////Método para calcular fechas
    const dates = () => {
        const date = moment(String(new Date())).format('MMMM DD YYYY');
        setNowDate(date.toString());
        
        const dateV1 = new Date();
        dateV1.setDate(dateV1.getDate()+30);
        const dateV2 = moment(String(dateV1)).format('DD/MM/YYYY');
        setValidity(dateV2.toString());
    }
    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          //getParkingInfo();
          getToken();
          dates();
        });
        var date = new Date();
        var expirydate = new Date().setHours(24,0,0);
        var diffr = moment.duration(moment(expirydate).diff(moment(date)));

        var hours = parseInt(diffr.asHours());
        var minutes = parseInt(diffr.minutes());
        var seconds = parseInt(diffr.seconds());
        
        var d = hours * 60 * 60 + minutes * 60 + seconds;
        setTotalDuration(d);
        const newDuration = d;
        var variable = 1;
        setInterval(()=> {
                    variable = variable - 1/newDuration;
                    setProgress(variable);
                }, 1000);
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Pasadia')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, paddingTop:10,justifyContent:'center',padding:20,}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'center'}}>Pasadía Finalizada</Text>
                    <View style={{flex: 1, backgroundColor: surface, paddingTop:10}}>

                        <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:20}}>Detalles del servicio</Text> 
                        <View style={{marginTop:20}}>
                            <Text style={styles.text}>Fecha de compra</Text>
                            <Text style={styles.textBold}>{nowDate}</Text>
                        </View>
                        <View style={{marginTop:20,flexDirection:'row'}}>
                            <View>
                                <Text style={styles.text}>Fecha de activación</Text>
                                <Text style={styles.textBold}>
                                    {nowDate}
                                </Text>
                            </View>
                            <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto'}}>
                                <Text style={styles.text}>Hora de activación</Text>
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>
                                {date.getHours()} : {date.getMinutes()}
                                </Text>
                            </View>
                        </View>
                        <View style={{marginTop:20,flexDirection:'row'}}>
                            <View>
                                <Text style={styles.text}>Fecha de culminación</Text>
                                <Text style={styles.textBold}>
                                    {nowDate}
                                </Text>
                            </View>
                            <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto'}}>
                                <Text style={styles.text}>Hora de culminación</Text>
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>
                                {date.getHours()} : {date.getMinutes()}
                                </Text>
                            </View>
                        </View>
                        <View style={{marginTop:20}}>
                            <Text style={styles.text}>Vigencia</Text>
                            <Text style={styles.textBold}>{validity}</Text>
                        </View>
                        <View style={{marginTop:20,flexDirection:'row'}}>
                            <View>
                                <Text style={styles.text}>Tipo de vehículo</Text>
                                <Text style={styles.textBold}>
                                    {
                                    global.vehicle.typeVehicleId === 1 ? 'Automóvil' : global.vehicle.typeVehicleId === 2 ? 'Motocicleta' : global.vehicle.typeVehicleId === 3 ? 'Bicicleta':''
                                    
                                    }
                                </Text>
                            </View>
                            <View style={{marginLeft:40}}>
                                <Text style={styles.text}>Placas del vehículo</Text>
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>{
                                global.vehicle.licensePlate && global.vehicle.licensePlate
                                }</Text>
                            </View>
                        </View>
                        <View style={{marginTop:20,paddingBottom:20}}>
                            <Text style={styles.text}>Costo</Text>
                            <Text style={styles.textBold}>$ 30.000</Text>
                        </View>
                    </View>

                </View>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
        padding:20
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 16,
        fontFamily: 'Gotham-Medium',
        marginBottom:50,
        marginTop:20,
        alignItems:'center',
        textAlign:'center'
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        marginLeft:10,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-100,
        flexDirection:'row',
        alignItems: "center",
        alignContent:'center',
        justifyContent:'center',
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })