import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import moment from "moment";
import 'moment/locale/es';
import { List } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { Accordion, Box, NativeBaseProvider, Center } from 'native-base';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
import { G } from 'react-native-svg';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function SumaryPasadia({navigation}) {
    const [isSelected, setSelection] = useState(false);
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [nowDate, setNowDate] = useState(null);
    const [validity, setValidity] = useState(null);
    const [expanded, setExpanded] = useState(true);
    const [price, setPrice] = useState(null);
    const [dialogReserveVisible,setDialogReserveVisible] = useState(true);

    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
            
          }
        } catch (error) { }
      };

    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("vehicleTypeId");
          const value2 = await AsyncStorage.getItem("licensePlate");
          console.log(`${value1} -- ${value2}`);
          if (value1 !== null && value2 !== null) {
            var id = parseInt(value1);  
            setTypeVehicleId(id);
            setLicensePlate(value2);
            AsyncStorage.removeItem("vehicleTypeId");
            AsyncStorage.removeItem("licensePlate");
          }
        } catch (error) {
            console.log(error);
         }
      };
////Método para calcular fechas
    const dates = () => {
        const date = moment(String(new Date())).format('MMMM DD YYYY');
        setNowDate(date.toString());
        
        const dateV1 = new Date();
        dateV1.setDate(dateV1.getDate()+30);
        const dateV2 = moment(String(dateV1)).format('DD/MM/YYYY');
        setValidity(dateV2.toString());
    }
    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    const terms = () => {
        setDialogTermsVisible(true);
    }
    const _handlePress = () => {
        setExpanded(!expanded);
    }

    //Ir purchasedPasadia

    const goToPurchased = async (token)  => {


        
        const pasadiaId = await AsyncStorage.getItem("pasadiaId");
        //console.log(pasadiaId);
        global.vehicle = {
            typeVehicleId:typeVehicleId,
            licensePlate:licensePlate,
        }
    
        let config = {
        headers : {Authorization: token}
    }

        setSpinner(true);
        console.log(`Archivo config __________ ${JSON.stringify(config)}`);

        console.log(global.token)
        axios.put(`${API_URL}customer/app/dayPass/pay/`+ pasadiaId,{},config).then(response => {
            
            
            const cod = response.data.ResponseCode;
            setSpinner(false);
            console.log(`Archivo config __________ ${JSON.stringify(response.data)}`)
            if(cod === 0){
                //
                //setVehicles(response.data.ResponseMessage);
                Alert.alert("",response.data.ResponseMessage);
                navigation.navigate('PurchasedPasadia');
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
             }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getDataStorage();
          //getParkingInfo();
          getToken();
          dates();
          
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Pasadia')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, paddingLeft:20, paddingTop:10,}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',}}>Pasadía</Text>
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Fecha de compra</Text>
                        <Text style={styles.textBold}>{nowDate}</Text>
                    </View>
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Vigencia hasta</Text>
                        <Text style={styles.textBold}>{validity}</Text>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Tipo de vehículo</Text>
                            <Text style={styles.textBold}>
                                {typeVehicleId === 1 ? 'Automóvil' : typeVehicleId === 2 ? 'Motocicleta' : typeVehicleId === 3 ? 'Bicicleta':''}
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Placas del vehículo</Text>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>{licensePlate}</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,paddingBottom:20}}>
                        <Text style={styles.text}>Costo</Text>
                        <Text style={styles.textBold}>
                            {typeVehicleId === 1 ? '$ 30.000' : '$ 15.000' }
                        </Text>
                    </View>
                </View>
                <View style={styles.containerAccordion}>
                    <NativeBaseProvider>
                        <Center flex={1}>
                            <ScrollView>
                                <Box m={3} style={{border:0}}>
                                    <Accordion allowMultiple _text={{border:0}} border={0} onTouchStart={_handlePress}>
                                        <Accordion.Item _text={{color:secondary}} >
                                            <Accordion.Summary  _expanded={{backgroundColor:surface,}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary}}>Beneficios</Text>
                                                    <View style={{marginLeft:160}}>
                                                        <MaterialIcons name={expanded ? "keyboard-arrow-down":"keyboard-arrow-up"} size={30} color={secondary} />
                                                    </View> 
                                                </View>
                                            </Accordion.Summary>
                                            <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                                <Text style={{color:secondary,fontFamily:'Gotham-Light',fontSize:16}}><Text style={{fontFamily:'Gotham-Bold'}}>1.</Text> Puedes cambiar de parqueadero hasta 10 veces en el día</Text> 
                                            </Accordion.Details>
                                            <Accordion.Details _text={{color:secondary,fontFamily:'Gotham-Light'}}>
                                            <Text style={{color:secondary,fontFamily:'Gotham-Light',fontSize:16}}><Text style={{fontFamily:'Gotham-Bold'}}>2.</Text> Estaciona el tiempo que quieras hasta el cierre del parqueadero, o hasta media noche si éste es 24 horas</Text>
                                            </Accordion.Details>
                                        </Accordion.Item>
                                    </Accordion>
                                </Box>
                            </ScrollView>
                        </Center>
                    </NativeBaseProvider>
                </View>
                <View style={{backgroundColor: surface, padding:20, marginTop:16,}}>
                    <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <Ionicons name="card-outline" size={24} color={secondary} />
                                <View style={{marginLeft:30}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                                    <Text style={styles.text}>Pepito Perez</Text>
                                </View>
                                <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                            </View>
                        </View> 
                    </TouchableOpacity>           
                </View>
                <View style={styles.container}>
                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Resumen</Text>
                    <View style={{flexDirection:'row',marginTop:20}}>
                        <View>
                            <Text style={styles.text}>Costo del servicio</Text>
                        </View>
                        <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',marginRight:-10}}>{/* {parking !== null ? 
                                        typeVehicleId == 1 ? parking.tariff[0].price : typeVehicleId == 2 ? parking.tariff[1].price : typeVehicleId == 3 ? parking.tariff[2].price : '0'
                                        :'0'
                                        } */} {typeVehicleId === 1 ? '$ 30.000' : '$ 15.000' }  </Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',marginTop:20}}>
                        <View>
                            <Text style={styles.text}>Valor de descuento</Text>
                        </View>
                        <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>-${parking !== null && price ? 
                                        typeVehicleId == 1 ? parking.tariff[0].price-price : typeVehicleId == 2 ? parking.tariff[1].price-price : typeVehicleId == 3 ? parking.tariff[2].price-price : '0'
                                        :'0'
                                        }</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.container2}>
                <View style={{flexDirection:'row'}}>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ alignSelf: "center",marginLeft:10,color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    />
                    <TouchableOpacity onPress={terms}>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Light',marginTop:10,textDecorationLine:'underline'}}>Acepto terminos y condiciones</Text>
                    </TouchableOpacity>
                </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogReserveVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title2}>Tienes 10 minutos para completar tu pasadia</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center',marginTop:10}}>
                                    <Text style={styles.text2}>Descripción opcional.....</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={()=> navigation.navigate('SumaryPasadia')}>
                                        <Text style={styles.titleButton1}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>



                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminos y condiciones</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                <View style={{marginVertical:20,alignItems:'center',}}>
                    {
                        isSelected ? 
                        <TouchableOpacity style={styles.button1} onPress={() => goToPurchased(global.token)}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.titleButton1}>PAGAR</Text>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                    <Text style={styles.titleButton1}>
                                        {typeVehicleId === 1 ? '$ 30.000' : '$ 15.000' }
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity disabled style={{backgroundColor: OnSurfaceOverlay15,padding: 14,borderRadius: 10,width: width-30,}}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR</Text>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                    <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>Total {typeVehicleId === 1 ? '$ 30.000' : '$ 15.000' }</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                </View>

            </View>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: 'center',
        justifyContent:'center',
        marginBottom:20,
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign: 'center'
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })