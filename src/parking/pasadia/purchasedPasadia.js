import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,Image} from 'react-native';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import CountDown from 'react-native-countdown-component';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import QRCode from 'react-native-qrcode-svg';
//import RNQRGenerator from 'rn-qr-generator';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
import Pasadia from './pasadia';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function PurchasedPasadia({navigation}) {
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [nowDate, setNowDate] = useState(null);
    const [validity, setValidity] = useState(null);
    const [viewQR, setViewQR] = useState(false);
    const [totalDuration, setTotalDuration] = useState(null);
    const [progress, setProgress] = useState(null);
    const [qrValue, setQrValue] = useState('');
    const [infoPasadia, setInfoPasadia] = useState([]);
    const [vehicle, setVehicle] = useState([]);
    const [pasadiaId, setPasadiaId] = useState('')
        
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          const value1 = await AsyncStorage.getItem("pasadiaId");
          setPasadiaId(value1)
          if (value !== null) {
            global.token = value;
            setToken(value);
            getPasadia(value1, value);
          }
        } catch (error) { }
      };

    ////Método para calcular fechas
    const dates = () => {
        const date = moment(String(new Date())).format('MMMM DD YYYY');
        setNowDate(date.toString());
        
        const dateV1 = new Date();
        dateV1.setDate(dateV1.getDate()+30);
        const dateV2 = moment(String(dateV1)).format('DD/MM/YYYY');
        setValidity(dateV2.toString());
    }

    const getPasadia =  (value1, token) =>{

        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        //console.log(`Archivo pasadia __________ ${JSON.stringify(config)}`);

        axios.get(`${API_URL}customer/app/dayPass/get/${value1}`, config).then(response => {
            
            const cod = response.data.ResponseCode;
            setSpinner(false);
             if(cod === 0){
                 const x = response.data.Data.pasadia;
                setInfoPasadia(x);
                setVehicle(response.data.Data.vehicle.vehicle)
                //console.log(`Archivo config __________ ${JSON.stringify(response.data.Data)}`) 
                //navigation.navigate('SumaryPasadia')
            }else{
              Alert.alert("ERROR","No se pudo traer las pasadias");
            } 
            
          }).catch(error => {
            setSpinner(false);
            console.log(error);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
            console.log(error);
          })



    }

    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    ///Método para activar pasadía

    const pasadiaActived = async () => {
        
        let config = {
            headers : {Authorization: global.token}
        } 
        console.log(`pasadia: ${pasadiaId}`);
        axios.post(`${API_URL}customer/app/activePass/${pasadiaId}`,{}, config).then(response => {
            
            const cod = response.data.ResponseCode;
            setSpinner(false);
            if(cod === 0){
                console.log(`Archivo config __________ ${JSON.stringify(response.data)}`)
                Alert.alert("",response.data.ResponseMessage);
                navigation.navigate('ActivePasadia');
            }else{
                console.log(`Archivo config __________ ${JSON.stringify(response.data)}`)
            }
        }).catch(error => {
            setSpinner(false);
            console.log(`este es el error: ${JSON.stringify(error)}`);
            if(error.message === 'Request failed with status code 403'){
                Alert.alert("Error",'No se ha podido activar la pasadía');
            }else{
                Alert.alert("Error",error.message);
            }
        })
        
    }


    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          //getParkingInfo();
          getToken();
          dates();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Pasadia')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, paddingTop:10,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',textAlign:'center'}}>Pasadía adquirida</Text>
                    <View style={{alignItems:'center'}}>
                        <Image
                        source={require("../../../assets/register/ilustracion1.png")}
                        style={{ marginVertical:20 }}></Image>
                    </View>
                    <View style={{paddingHorizontal:20}}>
                        <Text style={{color: secondary,fontSize: 15,fontFamily: 'Gotham-Light',textAlign:'center'}}>Para usar <Text style={{fontFamily:'Gotham-Bold'}}>PASADIA</Text> presenta este codigo a la entrada y salida del punto de servicio.</Text>
                    </View>
                    <View style={{marginTop:20,alignContent:'center',alignItems:'center',justifyContent:'center',marginBottom:10}}>
                        
                        {
                            !viewQR ?
                                <TouchableOpacity style={styles.button1} onPress={pasadiaActived}>
                                    <FontAwesome name="qrcode" size={24} color={colorPrimaryLigth} />
                                    <Text style={styles.titleButton1}>ACTIVAR PASADÍA</Text>
                                </TouchableOpacity>
                            :
                                <View>



                                    <View style={{alignItems:'center'}}>
                                        
                                        <QRCode
                                            value={ qrValue ? qrValue : 'NA' }
                                            size={ 120 }
                                            color= 'white'
                                            backgroundColor='#519B00'
                                            
                                        >
                                        </QRCode>

                                    </View>

                                    <View
                                    style={{
                                        backgroundColor: surface,
                                        borderRadius: 10,
                                        width:width-38,
                                        marginLeft: 0,
                                        marginRight: 1,
                                        shadowColor: "#000",
                                        shadowOffset: {
                                        width: 0,
                                        height: 1,
                                        },
                                        shadowOpacity: 0.49,
                                        shadowRadius: 4.65,
                                        elevation: 3,
                                        marginBottom:10,
                                        marginTop:10,
                                        padding:10,
                                    }}
                                    >
                                        <View style={{flexDirection:'row'}}>
                                            <View >
                                                <Text style={{color: secondary,fontSize: 14,fontFamily: 'Gotham-Light',marginTop:12}}>Tu pasadía vence en:</Text>
                                            </View>
                                            <View style={{alignItems:'center',alignContent:'center',justifyContent:'center',marginLeft:'auto'}}>
                                                { totalDuration &&
                                                    <CountDown
                                                        until={totalDuration}
                                                        style={{fontFamily:'Gotham-Bold'}}
                                                        digitTxtStyle={{color:secondary,fontFamily:'Gotham-Bold'}}
                                                        separatorStyle={{color:secondary,fontSize:15}}
                                                        digitStyle={{backgroundColor:'transparent',marginLeft:0,marginRight:0}}
                                                        showSeparator={true}
                                                        timeLabels={{ h: '', m: '', s: ''}}
                                                        timetoShow={['H','M','S']}
                                                        size={16}
                                                    />
                                                }
                                            </View>
                                        </View>
                                        <View style={{paddingRight:10}}>
                                            <ProgressBar progress={progress} color={primary500} />
                                        </View>
                                    </View>

                                    <TouchableOpacity onPress={() => navigation.navigate('ActivePasadia')}>
                                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:10}}>Activar</Text> 
                                    </TouchableOpacity>
                                </View>
                        }
                    </View>

                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:20,paddingTop:10,marginTop:16}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',marginTop:10}}>Detalles del servicio</Text> 
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Fecha de compra</Text>
                        <Text style={styles.textBold}>{nowDate}</Text>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Tipo de vehículo</Text>
                            <Text style={styles.textBold}>
                                {
                                vehicle.vehicle_type_id === 1 ? 'Automóvil' : vehicle.vehicle_type_id === 2 ? 'Motocicleta' : vehicle.vehicle_type_id === 3 ? 'Bicicleta':''
                                
                                }
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Placas del vehículo</Text>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>{
                              vehicle.plate && vehicle.plate
                            }</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Vigencia</Text>
                        <Text style={styles.textBold}>{validity}</Text>
                    </View>
                    <View style={{marginTop:20,paddingBottom:20}}>
                        <Text style={styles.text}>Costo</Text>
                        <Text style={styles.textBold}>{ vehicle.vehicle_type_id === 1 ? '$30.000' : '$15.000'}</Text>
                    </View>
                </View>
                <View style={styles.containerAccordion}>
                    <View>
                        <Text style={{fontFamily:'Gotham-Bold',fontSize:24,color:secondary}}>Beneficios</Text>
                    </View>
                    <Text style={{color:secondary,fontFamily:'Gotham-Light',fontSize:16,marginVertical:10}}><Text style={{fontFamily:'Gotham-Bold'}}>1.</Text> Puedes cambiar de parqueadero hasta 10 veces en el día</Text> 
                    <Text style={{color:secondary,fontFamily:'Gotham-Light',fontSize:16}}><Text style={{fontFamily:'Gotham-Bold'}}>2.</Text> Estaciona el tiempo que quieras hasta el cierre del parqueadero, o hasta media noche si éste es 24 horas</Text>
                </View>
                <View>
                    <TouchableOpacity
                        onPress={()=> navigation.navigate('Pasadia')}
                    >
                        <Text style={styles.text3}>CERRAR</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
        padding:20
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        marginLeft:10,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-100,
        flexDirection:'row',
        alignItems: "center",
        alignContent:'center',
        justifyContent:'center',
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    text3: {
        flex: 1,
        backgroundColor: surface,
        padding:5,
        fontSize:15,
        marginTop:16,
        marginBottom: 16,
        textAlign: 'center',
        fontFamily:'Gotham-Light',
        fontStyle: 'italic'
        

    }
  })