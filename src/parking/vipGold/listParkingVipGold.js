import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert} from 'react-native';
import { TextInput as PaperTextInput } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,FontAwesome5 } from '@expo/vector-icons';
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListParkingVipGold ({navigation}) {
const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
const [spinner,setSpinner] = useState(false);
const [token, setToken] = useState(null);
const [listParking, setListParking] = useState([]);
const [filterList , setFilterList] = useState('');


const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        global.token = value;
        setToken(value);
        getParkingList(value);
      }
    } catch (error) { }
  };
  ///Método para traer los parqueaderos activos
  const getParkingList =  async (token) =>{

    const vehicleId = await AsyncStorage.getItem('vehicleTypesId')
    
      setSpinner(false);
      axios.post(`${API_URL}customer/app/valet/getListParkingApp`,{
          filter: 22,
          vehicle_type_id_1: vehicleId

      }).then(response => {
        setSpinner(false);
        const cod = response.data.ResponseCode;
        setListParking(response.data.ResponseMessage)

        console.log(`${JSON.stringify(response.data)}`);
      }).catch(error => { 
        setSpinner(false);
       if(error) {
           console.log(error);
       }
      })
  }
const searchTextList = (text) => {
    setFilterList(text);
}
useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getToken();
      getParkingList();
    });
    return unsubscribe;
  }, [navigation]);


    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.goBack(null)}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ flex: 1, paddingTop: 20,backgroundColor: surface,paddingLeft:16,paddingRight:16,marginBottom:10,paddingBottom:20}}>
            <PaperTextInput style={{width:width-40,backgroundColor:surface,color:colorInput,borderRadius:10}} 
                        right={<PaperTextInput.Icon 
                                name={() => <MaterialIcons name="search" size={30} color={secondary} />} />}
                        theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                        mode='outlined' label="Buscar" outlineColor={colorInputBorder} onChangeText={(text) => searchTextList(text)}/>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:18, marginBottom:5, marginTop:15 ,textTransform:'capitalize' }}>Listado de parqueaderos habilitados</Text>

            {
                listParking.length !== 0 &&

                listParking.filter((parking) =>{
                          if(filterList == ""){
                            return parking
                          } else if(parking.commercial_name.toString().toLowerCase().includes(filterList.toLowerCase())){
                              return parking
                          } 
                        }).map( (parking) => {
                    return <TouchableOpacity style={{justifyContent:'center'}} key={parking.id} disabled >
                                <View
                                style={{
                                    backgroundColor: surface,
                                    borderRadius: 5,
                                    width:width-40,
                                    marginLeft: 0,
                                    marginRight: 1,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                    width: 0,
                                    height: 1,
                                    },
                                    shadowOpacity: 0.49,
                                    shadowRadius: 4.65,
                                    elevation: 3,
                                    paddingBottom:10,
                                    marginTop:10,
                                    paddingTop:5,
                                }}
                                >
                                    <View style={{marginBottom:10,marginLeft:15}}>
                                        <View>
                                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:14,marginTop:10,textTransform:'capitalize',fontStyle:'italic' }}>
                                                {parking.commercial_name}
                                            </Text>
                                            {
                                                parking.address !== null ? 
                                                <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                   {parking.address}
                                                </Text>
                                                :
                                                <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:12,marginTop:5 }}>
                                                    Este parqueadero no tiene dirección
                                                </Text>

                                            }
                                            
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            
                    })
                   

            }
          </View>
          <View>
            <TouchableOpacity onPress={() => navigation.navigate('MembershipActivated')} >
              <Text style={{ color: "#005A6D", fontSize:14, fontFamily: "Gotham-Bold", textAlign:'center'}}>Cerrar vista</Text>
            </TouchableOpacity>   
          </View>


        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingHorizontal:20,
      paddingBottom:20,
      paddingTop:10,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 10,
        borderRadius: 12,
        width: width-180,
        marginTop:10,
        alignItems: "center",
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })