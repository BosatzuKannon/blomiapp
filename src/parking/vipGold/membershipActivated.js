import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import QRCode from 'react-native-qrcode-svg';
import Spinner from "react-native-loading-spinner-overlay";
import {productIdReserve} from "../../utils/varService";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,
    surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, OnSurfaceOverlay15,OnSurfaceDisabled,
    colorInputBorder, colorPrimaryLigth, primary700,statesPrimaryOverlaySelected, surfaceHighEmphasis } from '../../utils/colorVar';
import { justifyContent } from 'styled-system';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function MembershipActivated ({navigation}){

    const [spinner,setSpinner] = useState(false);
    const [qrValue, setQrValue] = useState(null);
    const [token, setToken] = useState(null);
    const [documentType, setDocumentType] = useState(0);
    const [document, setDocument] = useState('');
    const [nameben, setNameben] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [city, setCity] = useState('');
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [isSelected, setSelection] = useState(false);
    const [typeDocumentId, setTypeDocumentId] = useState(null);
    const [info, setInfo] = useState('');
    const [dataVip, setDataVip] = useState('');
    const [dataVip1, setDataVip1] = useState('');
    const [dataVip2, setDataVip2] = useState('');
    const [vipId, setVipId] = useState('');
    const [vehicles, setVehicles] =  useState([]);
    const [typeDocument, setTypeDocument] = useState([
        {
            name:'Cédula de ciudadania',
            id:1,
        },
        {
            name:'Documento extranjero',
            id:2,
        },
        {
            name:'NIT',
            id:3,
        },
        {
            name:'Pasaporte',
            id:4,
        },
        {
            name:'Rut',
            id:5,
        },
    ]);
    const getToken =  async () => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                global.token = value
                setToken(value);
                const value1 = await AsyncStorage.getItem("vipId");
                setVipId(value1);
                console.log(`id del vip -- ${value1}`) 
                getDataVip(value1)
                
                //getInfo(value1)
            }
          } catch (error) {
            console.log(error);
          }
    }
    //Metodo para traer la data del servicio vip
        const getDataVip =  (value1) => {
            setSpinner(true);
          
            axios.get(`${API_URL}customer/app/vip/getInfoCustomer/${value1}`).then(async (response) => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                console.log(`info vip service -- ${JSON.stringify(response.data.ResponseMessage)}`);
                if(cod == 0){
                    setDataVip(response.data.ResponseMessage[0].customer)
                    setDataVip1(response.data.ResponseMessage[0])
                    //setDataVip2(response.data.ResponseMessage[0].vehicles)
                    console.log(` datas -- ${JSON.stringify(dataVip)}`);
                }else{
                  alert("No se puede traer  los datos del servicio vip");
                } 
              }).catch(error => {
                setSpinner(false);
                console.log(error);
              })
            
          }; 
  /*         const getInfo = async () => {
            try {
              const value1 = await AsyncStorage.getItem("vehicle"); 
            } catch (error) {
                console.log(error);
             }
          }; */
/*           const getDataStorage = async () => {
            try {
              const value1 = await AsyncStorage.getItem("vehicle");
              if (value1 !== null) { 
                const value = JSON.parse(value1);
                setVehicles(value);
                AsyncStorage.removeItem("vehicle");
              }
    
            } catch (error) {
                console.log(error);
             }
          }; */
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
            global.vehicles = []
            //getDataStorage();
            //getDataVip();       
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <View style={{ marginBottom:20 }}>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:15, alignSelf: 'center' }}>Membresia GOLD adquirida</Text>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color: "#005A6D", alignSelf: 'center' }}>Presenta este codigo a la entrada y salida del punto de servicio para disfrutar tus beneficios</Text>
                    </View>

                    <View style={{alignItems:'center', marginLeft: -25, marginBottom:30}}>
                        <QRCode
                            value={ qrValue ? qrValue : 'NA' }
                            size={ 130 }
                            color= 'white'
                            backgroundColor='#519B00'
                        >
                        </QRCode>
                    </View> 

                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:20, color:secondary, marginBottom:15 }}>Detalles del servicio</Text>

                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginRight:150, marginBottom:5}}>Estado</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                Activo
                            </Text>
                        </View>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginRight:50, marginBottom:5}} >Fecha de expiración</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                15-02-2024
                            </Text>
                        </View>
                    </View>
                    {/* Botón lista parqueaderos */}
                    <TouchableOpacity style={styles.button} onPress={ () => navigation.navigate('ListParkingVipGold') }>      
                        <Text style={{fontFamily:'Gotham-Bold', color:'#005A6D', fontSize: 14, textAlign:'center'}}>CONSULTAR PARQUEADEROS</Text>                
                    </TouchableOpacity>
                    {/* boton para agregar otro vehiculo */}
                   
                        
                            <TouchableOpacity style={styles.button1} onPress={ () => navigation.navigate('ListVehicles') }>      
                                <Text style={{fontFamily:'Gotham-Bold', color:'#005A6D', fontSize: 14, textAlign:'center'}}>AGREGAR VEHICULO</Text>                
                            </TouchableOpacity>
                   
                    
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:20, color:secondary, marginBottom:15, marginTop:20 }}>Detalles del Beneficiario</Text>
                    
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary,  marginBottom:5}}>Nombre</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                Carlos Jaramillo
                            </Text>
                        </View>
                        <View style={{marginLeft:'auto', marginRight:10}}>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >Tipo de documento</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {dataVip.document_type_id == 2 ? 'CC' : 'CC'}
                            </Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}}>N° Documento</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                1087116281
                            </Text>
                        </View>
                        <View style={{marginLeft:'auto', marginRight:50}}>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >N° Télefono</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                3175345577
                            </Text>
                        </View>
                    </View> 
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}}>Correo</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                carlos.jaramillo@parking.net.co
                            </Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View >
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >Placas</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                
                                VBO09D -- AAA123
                                     {/*    <View style={{flexDirection:'row',justifyContent:'center', alignItems:'center'}}>
                                            { dataVip2 ? 
                                            dataVip2.map((vehicle) => {
                                                return  
                                                <View style={{backgroundColor:'rgba(0, 45, 51, 0.08)',borderRadius:20,flexDirection:'row',padding:5,marginLeft:5, marginRight:5, borderWidth: 1, borderColor:secondary}} key={placa.plate}> 
                                                    <Text style={{marginRight:10, color:secondary,fontFamily:'Gotham-Medium', marginLeft:5}}>{vehicle.plate}</Text>
                                                </View>
                                                })
                                                :
                                                <View></View>
                                            }
                                    </View> */}
                                
                            </Text>
                        </View>
                    </View>
                    
                
                    <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                        <View style={{ marginTop:40}}>
                            <Text style={{alignSelf:'center', fontSize:14, color:'rgba(0, 45, 51, 0.6)'}}>CERRAR</Text>
                        </View>
                    </TouchableOpacity>
                </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: surface,
        paddingBottom:width-270,
        paddingTop:10,
        paddingHorizontal: 20
    },
    button: {
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor: '#005A6D',
        padding: 14,
        borderRadius: 10, 
        width: 380, 
        alignSelf: 'center', 
        bottom: 20,
        marginTop:50
    },
    button1: {
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor: '#005A6D',
        padding: 14,
        borderRadius: 10, 
        width: 380, 
        alignSelf: 'center', 
        bottom: 20,
        marginTop:20
    }
})