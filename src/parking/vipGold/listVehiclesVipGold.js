import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {productIdReserve} from "../../utils/varService";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,
    surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, OnSurfaceOverlay15,OnSurfaceDisabled,
    colorInputBorder, colorPrimaryLigth, primary700,statesPrimaryOverlaySelected, surfaceHighEmphasis } from '../../utils/colorVar';
import { justifyContent } from 'styled-system';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListVehiclesVipGold ({navigation}) {

    const [vehicles, setVehicles] = useState([]);
    const [spinner,setSpinner] = useState(false);
    const [vehiclesSend, setVehiclesSend] = useState([]);
    const [select1, setSelect1] = useState(false);
    const [select2, setSelect2] = useState(false);
    const [select3, setSelect3] = useState(false);

    const selectVehicle = (vehicle,index) => {

        if(vehiclesSend.length === 0){
            setVehiclesSend(vehiclesSend.concat({id:vehicle.id,typeId:vehicle.vehicle_type_id,license:vehicle.plate}));
            const array1 = vehicles.map(v => {
                if(vehicle.id === v.id){
                    v.select = true;
                    return v;
                }
                return v;
            });
            setVehicles(array1);
        }
        if(vehiclesSend.length === 1){
            setVehiclesSend(vehiclesSend.concat({id:vehicle.id,typeId:vehicle.vehicle_type_id,license:vehicle.plate}));
            const array1 = vehicles.map(v => {
                if(vehicle.id === v.id){
                    v.select = true;
                    return v;
                }
                return v;
            });
            const array2 = array1.map(v => {
                if(v.select === false){
                    v.disabled = false;
                    return v;
                }
                return v;
            });
            setVehicles(array2);
        }
        if(vehiclesSend.length === 2){
            setVehiclesSend(vehiclesSend.concat({id:vehicle.id,typeId:vehicle.vehicle_type_id,license:vehicle.plate}));
            const array2 = vehicles.map(v => {
                if(vehicle.id === v.id){
                    v.select = true;
                    return v;
                }
                return v;
            });
            const array3 = array2.map(v => {
                if(v.select === false){
                    v.disabled = true;
                    return v;
                }
                return v;
            });
            setVehicles(array3);
            
        }
    }
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            getVehicles(value);
          }
        } catch (error) { }
      };

    //metodo para registrar los vehiculos seleccionados por el usuario
    const save = async (vehicle) => {
        const value1 = await AsyncStorage.getItem("code");
        console.log(`arreglo vehiculos -- ${JSON.stringify(vehiclesSend)}`);
        console.log(`code -- ${JSON.stringify(value1)}`);
         try {

            axios.post(`${API_URL}customer/app/vip/addCarToVip/${value1}`,vehiclesSend)
            .then(response => {
                setSpinner(false);
                const cod = response.data.ResponseCode;
                //console.log(`reps -- ${JSON.stringify(response.data.ResponseMessage)}`);
                alert(response.data.ResponseMessage);
            
                }).catch(error => {
                    setSpinner(false);
                    //Alert.alert("",error.response.data.ResponseMessage);
                    console.log(`error --${JSON.stringify(error)}`);
                })       

            await AsyncStorage.setItem("vehicle", JSON.stringify(vehicle));
            navigation.navigate('MembershipActivated');
        } catch (e) {
          // saving error
          console.log('Fallo en guardar storage');
        } 
      };

    ///////////verificar si tiene vehiculos registrados
    const getVehicles = (token) => {
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            const cod = response.data.ResponseCode;
            setSpinner(false);
            if(cod === 0){
                var ve = []; 
                response.data.ResponseMessage.vehicles.map((vehi)=>{
                    vehi.disabled = false;
                    vehi.select = false;
                });
                setVehicles(response.data.ResponseMessage.vehicles);
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    const getDataStorage = async () => {
        try {
          const value1 = await AsyncStorage.getItem("code");
          console.log(`code vip -- ${JSON.stringify(value1)}`);
        
        } catch (error) {
            console.log(error);
         }
      };
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
            getDataStorage();
        });
        return unsubscribe;
    }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('FormVipGold')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Vehículo</Text>
                <Text style={styles.text2}>Selecciona el vehículo...................</Text>
                <View >
                    {
                        vehicles.length !== 0 ?
                        vehicles.map( (vehicle,index) => {
                            return <View style={{justifyContent:'center'}} key={vehicle.id} >
                                        <TouchableOpacity
                                        disabled={vehicle.disabled}
                                        onPress={() => selectVehicle(vehicle,index)}
                                        style={[
                                            vehicle.disabled ?
                                            {
                                                backgroundColor: surface,
                                                borderRadius: 15,
                                                width:width-40,
                                                marginLeft: 0,
                                                marginRight: 1,
                                                marginBottom:10,
                                                marginTop:10,
                                                paddingHorizontal:10,
                                                paddingVertical:5,
                                            }
                                            :
                                            vehicle.select ?
                                            {
                                                backgroundColor: statesPrimaryOverlaySelected,
                                                borderColor:statesPrimaryOverlaySelected,
                                                borderRadius:5,
                                                shadowColor:statesPrimaryOverlaySelected,
                                                width:width-40,
                                                marginLeft: 0,
                                                marginRight: 1,
                                                elevation: 1,
                                                marginBottom:10,
                                                marginTop:10,
                                                paddingHorizontal:10,
                                                paddingVertical:5,
                                            }
                                            :
                                            {
                                                backgroundColor: surface,
                                                borderRadius: 15,
                                                width:width-40,
                                                marginLeft: 0,
                                                marginRight: 1,
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                width: 0,
                                                height: 1,
                                                },
                                                shadowOpacity: 0.49,
                                                shadowRadius: 4.65,
                                                elevation: 3,
                                                marginBottom:10,
                                                marginTop:10,
                                                paddingHorizontal:10,
                                                paddingVertical:5,
                                            }
                                        ]}
                                        >
                                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                                                {
                                                    vehicle.vehicle_type_id === 1 &&
                                                    <FontAwesomeIcon size={20} icon={faCarSide} color={vehicle.disabled ? surfaseDisabled : surfaceHighEmphasis} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 2 &&
                                                    <FontAwesomeIcon size={20} icon={faMotorcycle} color={vehicle.disabled ? surfaseDisabled : surfaceHighEmphasis} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === 3 &&
                                                    <FontAwesomeIcon size={20} icon={faBicycle} color={vehicle.disabled ? surfaseDisabled : surfaceHighEmphasis} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                <View>
                                                    <Text style={[{ fontFamily: "Gotham-Bold",fontSize:16,marginTop:10 },vehicle.disabled ? {color:surfaseDisabled}:{color:surfaceHighEmphasis}]}>
                                                        {
                                                            vehicle.vehicle_type_id === 1 &&
                                                            "Automóvil"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 2 &&
                                                            "Motocicleta"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === 3 &&
                                                            "Bicicleta"
                                                        }
                                                                
                                                    </Text>
                                                    <Text style={[{ fontFamily: "Gotham-Light",fontSize:14,marginTop:5 },vehicle.disabled ? {color:surfaseDisabled}:{color:surfaceHighEmphasis}]}>
                                                        {vehicle.plate}
                                                    </Text>
                                                </View>
                                                {
                                                    vehicle.select &&
                                                        <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'center',marginLeft:'auto',}}>
                                                            <MaterialIcons name="check-circle" size={24} color={primary800} />
                                                        </View>
                                                }
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                            })
                        :
                        <Text style={{fontFamily:'Gotham-Bold',color: secondary,fontSize: 24,marginRight:10}}>
                            No tiene vehículos registrados
                        </Text> 
                    }
                    
                </View>
                
            </View>
            <View style={{backgroundColor:surface,paddingVertical:10,alignItems:'center',alignContent:'center'}}>
                    {
                        vehiclesSend.length > 0?
                        <TouchableOpacity style={styles.button1} onPress={() => save(vehiclesSend)}>
                                <Text style={styles.titleButton1}>ACTIVAR MEMBRESIA</Text>
                        </TouchableOpacity> 
                        :

                        <TouchableOpacity disabled={true}  style={{backgroundColor: OnSurfaceOverlay15,padding: 14,borderRadius: 10,width: width-50,alignItems:'center'}} disabled={true}>
                                <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>ACTIVAR MEMBRESIA</Text>
                        </TouchableOpacity> 

                    }
                </View>
            <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 20}}>
                <TouchableOpacity onPress={() => navigation.navigate('AddVehicle')} >
                    <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center", color:surfaceMediumEmphasis}}>
                        REGISTRAR NUEVO VEHÍCULO
                    </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: surface,
        paddingLeft:20,
        paddingBottom:20,
        paddingTop:30,
      },
      title: {
          color: '#000',
          fontSize: 18,
          fontFamily:'Gotham-Medium',
      },
      title2: {
          color: secondary,
          fontSize: 23,
          fontFamily:'Gotham-Bold',
      },
      text: {
          color: surfaceMediumEmphasis,
          fontSize: 14,
          fontFamily: 'Gotham-Light',
          marginBottom:10,
      },
      text2: {
          color: '#000',
          fontSize: 13,
          fontFamily: 'Gotham-Light',
          marginBottom:30,
      },
      lineStyle:{
          marginTop:5,
          marginBottom:10,
          backgroundColor: colorGrayOpacity,
          height: 2,
          width: 320,
      },
      input:{
          height:50,  
          color:colorInput, 
          borderRadius:10,
          width:width-30,
          marginTop:10,
          marginBottom:60,
          paddingHorizontal:25,
          borderColor: colorInputBorder,
          borderWidth: 1,
          fontFamily:'Gotham-Light',
          textDecorationLine:'none'
        },
        titleButton1: {
          color: '#fff',
          fontSize: 15,
          fontFamily:'Gotham-Bold',
        },
        button1: {
          backgroundColor: secondary,
          padding: 14,
          borderRadius: 10,
          width: width-30,
          alignItems:'center',
        },
        button3: {
          height:50, 
          backgroundColor: "#fff",
          padding: 14,
          borderRadius: 10,
          width: 320,
          alignItems: "center",
          marginTop: 10,
          marginBottom: 10,
          borderColor: secondary,
          borderWidth: 1.2,
        },
        titleButton3: {
          color: secondary,
          fontFamily:'Gotham-Bold',
          fontSize: 15,
        },
        pickerStyle: {
          borderRadius:10,
          padding:5,
          height: 50,
          marginTop:10,
          marginBottom:10,
          marginRight:15,
          borderColor: colorInputBorder,
          borderWidth: 1,
      },
})