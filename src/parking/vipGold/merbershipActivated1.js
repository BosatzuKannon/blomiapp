import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import QRCode from 'react-native-qrcode-svg';
import Spinner from "react-native-loading-spinner-overlay";
import {productIdReserve} from "../../utils/varService";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,
    surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, OnSurfaceOverlay15,OnSurfaceDisabled,
    colorInputBorder, colorPrimaryLigth, primary700,statesPrimaryOverlaySelected, surfaceHighEmphasis } from '../../utils/colorVar';
import { justifyContent } from 'styled-system';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import Toast from 'react-native-simple-toast';
import { Request } from '../../utils/api';
import { PRODUCT } from '../../utils/endpoints';

export default function MembershipActivated1 ({route,navigation}){

    const [spinner,setSpinner] = useState(false);
    const [qrValue, setQrValue] = useState(null);
    const [info, setInfo] = useState('');
    const [vehicles, setVehicles] =  useState([]);
    const [customer, setCustomer] = useState('');
    
    const {id} = route.params;

    const getServiceInfo = async () => {

        setSpinner(true)
        const request = new Request();
        const result = await request.request(PRODUCT.infoCustomerProduct,'POST',{id:id})
        if(result === 0){
            setSpinner(false)
            Toast.showWithGravity('Código no valido', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        }else{
            setSpinner(false)
            setInfo(result.ResponseData)
            setVehicles(result.ResponseData.vehicles)
            setCustomer(result.ResponseData.customer)
            const data = {
                customer_product:result.ResponseData.product_type_id,
                customer_id:result.ResponseData.customer_id,
                status:result.ResponseData.status
            }
            setQrValue(JSON.stringify(data))
        }
    }
    
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getServiceInfo();     
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <View style={{ marginBottom:20 }}>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:24, color:secondary, marginBottom:15, alignSelf: 'center' }}>Membresia VIP/GOLD adquirida</Text>
                        <Text style={{ fontFamily: 'Gotham-Bold', fontSize:16, color: "#005A6D", alignSelf: 'center' }}>Presenta este codigo a la entrada y salida del punto de servicio para disfrutar tus beneficios</Text>
                    </View>

                    <View style={{alignItems:'center', marginLeft: -25, marginBottom:30}}>
                        <QRCode
                            value={ qrValue ? qrValue : 'NA carajo' }
                            size={ 130 }
                            color= 'white'
                            backgroundColor='#519B00'
                        >
                        </QRCode>
                    </View> 

                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:20, color:secondary, marginBottom:15 }}>Detalles del servicio</Text>

                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginRight:150, marginBottom:5}}>Estado</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {info.status === 1 ? 'Activo' : 'Inactivo' }
                            </Text>
                        </View>
                        {/* <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginRight:50, marginBottom:5}} >Fecha de expiración</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {dataVip1.final_date}
                            </Text>
                        </View> */}
                    </View>
                    {/* Botón lista parqueaderos */}
                    {/* <TouchableOpacity style={styles.button} onPress={ () => navigation.navigate('ListParkingVipGold') }>      
                        <Text style={{fontFamily:'Gotham-Bold', color:'#005A6D', fontSize: 14, textAlign:'center'}}>CONSULTAR PARQUEADEROS</Text>                
                    </TouchableOpacity> */}
                    {/* boton para agregar otro vehiculo */}
                    {/* {
                        vehicles.length  > 1 ? 
                            <TouchableOpacity style={styles.button1} onPress={ () => navigation.navigate('ListParkingVipGold') }>      
                                <Text style={{fontFamily:'Gotham-Bold', color:'#005A6D', fontSize: 14, textAlign:'center'}}>AGREGAR VEHICULO</Text>                
                            </TouchableOpacity>
                        :
                            <View></View>
                    } */}
                    
                    <Text style={{ fontFamily: 'Gotham-Bold', fontSize:20, color:secondary, marginBottom:15, marginTop:20 }}>Detalles del Beneficiario</Text>
                    
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary,  marginBottom:5}}>Nombre</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.name} {customer.lastname} 
                            </Text>
                        </View>
                        <View style={{marginLeft:'auto', marginRight:10}}>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >Tipo de documento</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.document_type_id == 2 ? 'CC' : 'CC'}
                            </Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}}>N° Documento</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.document}
                            </Text>
                        </View>
                        <View style={{marginLeft:'auto', marginRight:50}}>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >N° Télefono</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.phone}
                            </Text>
                        </View>
                    </View> 
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}}>Correo</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                {customer.email}
                            </Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View >
                            <Text style={{ fontFamily:'Gotham-Medium', fontSize:14, color:secondary, marginBottom:5}} >Placas</Text>
                            <Text style={{fontFamily:'Gotham-Bold', fontSize:16, color:secondary }}>
                                <View style={{flexDirection:'row',justifyContent:'center', alignItems:'center'}}>
                                <Text style={{marginRight:10, color:secondary,fontFamily:'Gotham-Medium'}}>{Object.entries(vehicles).length > 0 ? `${vehicles[0].vehicle_plate}` : ''} {Object.entries(vehicles).length > 1 ? ` - ${vehicles[1].vehicle_plate}` : ''}</Text>
                                </View>
                            </Text>
                        </View>
                    </View>
                    
                
                    <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                        <View style={{ marginTop:40}}>
                            <Text style={{alignSelf:'center', fontSize:14, color:'rgba(0, 45, 51, 0.6)'}}>CERRAR</Text>
                        </View>
                    </TouchableOpacity>
                </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: surface,
        paddingBottom:width-270,
        paddingTop:10,
        paddingHorizontal: 20
    },
    button: {
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor: '#005A6D',
        padding: 14,
        borderRadius: 10, 
        width: 380, 
        alignSelf: 'center', 
        bottom: 20,
        marginTop:50
    },
    button1: {
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor: '#005A6D',
        padding: 14,
        borderRadius: 10, 
        width: 380, 
        alignSelf: 'center', 
        bottom: 20,
        marginTop:20
    }
})