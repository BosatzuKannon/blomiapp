import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput,Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../../utils/colorVar';
import { productIdPasadia } from '../../utils/varService';
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function ListVip ({navigation}) {
    const [vips, setVips] = useState([]);
    const [spinner,setSpinner] = useState(false);

   


    const selectVip = (vipId) => {
        save(vipId);
        console.log(`vip id-- ${JSON.stringify(vipId)}`);
        navigation.navigate('MembershipActivated')
    }
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;

            getVehicles(value);
            //selectVehicle(value);
          }
        } catch (error) { }
      };
    const save = async (vipId) => {
        try {
        console.log(` vip id -- ${vipId}--`)
          await AsyncStorage.setItem("vipId", vipId.toString());

        } catch (e) {
          // saving error
          console.log('Fallo en guardar storage vip');
        }
        
      };
      //metodo que trae el listado de vips por ususario
      const getInfo = () => {

        const config = {

            headers: { Authorization: global.token }
        };

        axios.get(`${API_URL}customer/app/vip/getInfoCustomerProduct`,config).then(response => {
        const cod = response.data.ResponseCode;
        console.log(` data -- ${JSON.stringify(response.data.ResponseMessage)}`);
        if(cod === 0){ 
             setVips(response.data.ResponseMessage)
             console.log(` data -- ${JSON.stringify(response.data.ResponseMessage[0].sub_product.name)}`);
        }else{
            Alert.alert("ERROR","No se pudo traer la informacion del usuario");
        } 
        }).catch(error => {
        setSpinner(false);
        Alert.alert("",error.response.data.ResponseMessage);
        console.log(error.response.data.ResponseMessage);
        }) 

    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
            getInfo();
        });
        return unsubscribe;
    }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate('CodeVipGold')}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>VIP</Text>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Seleccione Vip</Text>
                <Text style={styles.text2}>Consulta tuvip y accede a los beneficios</Text>
                <View >
                    {
                        vips.map( (vip) => {
                            return <TouchableOpacity style={{justifyContent:'center'}} key={vip.id} onPress={() => selectVip(vip.id)}>
                                        <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >   
                                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:5,paddingRight:10}}>
                                                {/* {
                                              
                                                   <FontAwesomeIcon size={25} icon={faCarSide}/>
                                                } */}
                                                <View>
                                                    <Text style={{ fontFamily: "Gotham-Bold", color: "rgba(0, 45, 51, 0.8)",fontSize:14,marginTop:10,textTransform:'uppercase' }}>
                                                    Nombre: {vip.sub_product.product_id == 29 ? 'Vip Natural' : 'Vip Corporativo'}            
                                                </Text>
                                                <Text style={{ fontFamily: "Gotham-Bold", color: "rgba(0, 45, 51, 0.8)",fontSize:14, textTransform:'uppercase' }}>
                                                    Expiracion: {vip.sub_product.eventDateEnd}
                                                </Text>
                                                <Text style={{ fontFamily: "Gotham-Bold", color: "rgba(0, 45, 51, 0.8)",fontSize:14}}>
                                                   
                                                </Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            })
                    }
                    
                </View>
                
            </View>
           
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:20,
      paddingTop:20,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: "#000000",
        fontSize: 18,
        fontFamily:'Gotham-Bold',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })