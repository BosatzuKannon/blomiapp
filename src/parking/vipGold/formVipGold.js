import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView, ScrollView, Alert,Dimensions,Modal, Image,TouchableWithoutFeedback} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import { surfaceMediumEmphasis,surfaceHighEmphasis,primary800,secondary,secondary50,surface, 
    colorGray, colorGrayOpacity,colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,primary50 } from '../../utils/colorVar';
import { TextInput as PaperTextInput, HelperText} from 'react-native-paper';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { ProgressBar, Colors, Card } from 'react-native-paper';
import Checkbox from 'expo-checkbox';
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default function FormVipGold({navigation}){

    const [spinner, setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [documentType, setDocumentType] = useState(0);
    const [document, setDocument] = useState('');
    const [nameben, setNameben] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [city, setCity] = useState('');
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [isSelected, setSelection] = useState(false);
    const [typeDocumentId, setTypeDocumentId] = useState(null);
    const [typeDocument, setTypeDocument] = useState([
        {
            name:'Cédula de ciudadania',
            id:1,
        },
        {
            name:'Documento extranjero',
            id:2,
        },
        {
            name:'NIT',
            id:3,
        },
        {
            name:'Pasaporte',
            id:4,
        },
        {
            name:'Rut',
            id:5,
        },
    ]);
    const getToken =  async () => {
        try {
            const value =  await AsyncStorage.getItem('token');
            if (value !== null) {
                global.token = value
                setToken(value);
            }
          } catch (error) {
            console.log(error);
          }
    }

    const terms = () => {
        setDialogTermsVisible(true);       
    }
    const validateUser = () => {
        if (document == "" || nameben == "" || email == "" || phone == "" || city == "" ){
            alert('complete todos los campos');
        }
        else {
            navigation.navigate('ListVehiclesVipGold')
        }
        
    }
    const getInfo = () => {

        const config = {

            headers: { Authorization: global.token }
        };

        axios.get(`${API_URL}customer/app/getInfoCustomer`,config).then(response => {
        const cod = response.data.ResponseCode;
        console.log(`info usuario -- ${JSON.stringify(response.data.ResponseMessage)}`);
        if(cod === 0){
            if (response.data.ResponseMessage.document_number.length > 0) {
                setDocument(response.data.ResponseMessage.document_number)
                setNameben(response.data.ResponseMessage.name)
                setEmail(response.data.ResponseMessage.email)
                setPhone(response.data.ResponseMessage.phone_1)
                setCity(response.data.ResponseMessage.city)
                if (response.data.ResponseMessage.document_type_id === 1 ||
                    response.data.ResponseMessage.document_type_id === 2 ||
                    response.data.ResponseMessage.document_type_id === 3 ||
                    response.data.ResponseMessage.document_type_id === 4 ||
                    response.data.ResponseMessage.document_type_id === 5
                ) {
                    setTypeDocumentId(response.data.ResponseMessage.document_type_id)
                }
                //navigation.navigate('FormBeParking')
            } 
        }else{
            Alert.alert("ERROR","No se pudo traer la informacion del usuario");
        } 
        }).catch(error => {
        setSpinner(false);
        Alert.alert("",error.response.data.ResponseMessage);
        console.log(error.response.data.ResponseMessage);
        }) 

    }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            getToken();
            getInfo();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView>
            <Spinner visible={spinner}  color={primary600} />
                <View style={{ paddingLeft: 20,paddingTop:20,paddingBottom:10,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <Text style={{fontFamily:'Gotham-Bold', color:secondary, fontSize:24, marginTop:20}} >Diligencia tus datos</Text>
                    <Text style={{fontFamily:'Gotham-Medium', color:secondary, fontSize:16, marginTop:10}} >Para activar tu membresia Vip</Text>
                    {/* form */}
                    <View style={{flexDirection:'row', marginBottom:10, marginTop:20}}>
                        <View style={styles.pickerStyle}>
                            <Picker 
                                selectedValue={typeDocumentId}
                                style={{width:width-60,backgroundColor:surface,color:colorInput,fontSize:14,}}
                                theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                mode='outlined' outlineColor={colorInputBorder}
                                onValueChange={(itemValue, itemIndex) => setTypeDocumentId(itemValue)}>
                                    <Picker.Item label="Tipo Documento" color={colorInput} value={null} enabled={false}/>
                                    {
                                    typeDocument.map( (type) => {
                                    return <Picker.Item label={type.name} color={colorInput} value={type.id} key={type.id}/>
                                    })
                                    }  
                            </Picker>
                        </View> 
                    </View>
                    <View>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,fontSize:14,}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="N° documento" outlineColor={colorInputBorder} value={document} onChangeText={text => setDocument(text)}/>
                    </View>
                    <View style={{marginBottom:10}}>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,fontSize:14,}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Nombre" outlineColor={colorInputBorder} value={nameben} onChangeText={text => setNameben(text)}/>
                    </View>
                    <View style={{marginBottom:10}}>
                        <PaperTextInput style={{width: width-50,backgroundColor:surface,color:colorInput,fontSize:14,marginRight:20}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Correo" outlineColor={colorInputBorder} value={email} onChangeText={text => setEmail(text)}/>
                    </View>
                    <View>
                        <PaperTextInput style={{width: width-50,backgroundColor:surface,color:colorInput,fontSize:14,marginRight:20}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Télefono" outlineColor={colorInputBorder} value={phone} onChangeText={text => setPhone(text)}/>
                    </View>
                    <View style={{marginBottom:10}}>
                        <PaperTextInput style={{width:width-50,backgroundColor:surface,color:colorInput,fontSize:14,}}
                            theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                            mode='outlined' label="Ciudad" outlineColor={colorInputBorder} value={city} onChangeText={text => setCity(text)}/>
                    </View>
                    {/* terminos */}
                    <View style={styles.container2}>
                        <View style={{flexDirection:'row'}}>
                            <Checkbox
                                value={isSelected}
                                onValueChange={setSelection}
                                style={{ alignSelf: "center",marginLeft:10,color:primary800}}
                                color={isSelected ? primary600 : surfaceMediumEmphasis}
                            />
                            <TouchableOpacity onPress={terms}>
                                <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Light',marginTop:10,textDecorationLine:'underline'}}>Acepto terminos y condiciones</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{marginVertical:20}}>
                            {
                                isSelected ? 
                                <View style={{ flexDirection: 'row'}}>
                                
                                    <TouchableOpacity onPress={() => validateUser() }  style={{backgroundColor: "#005A6D",padding: 14,borderRadius: 15,width: 379, height:48}}>
                                        <View>
                                            <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                                <Text  style={{color: "#F2FAE0",fontSize: 15,fontFamily:'Gotham-Bold',}}>CONTINUAR</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                :
                                <View style={{ flexDirection: 'row'}}>
                                    
                                    <TouchableOpacity disabled style={{backgroundColor: "rgba(0, 45, 51, 0.15)",padding: 14,borderRadius: 15,width: 379, height:48}}>
                                    
                                            <View style={{ fontFamily: 'Gotham-Bold', fontSize:14, color: "#F2FAE0", alignSelf: 'center' }}>
                                                <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>CONTINUAR</Text>
                                            </View>
                                            
                                    </TouchableOpacity>
                                </View>
                            
                            }
                        </View>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={dialogTermsVisible}
                            >
                            <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                <View style={styles.centeredView}>
                                    <View style={styles.modalView}>
                                        <Text style={styles.title}>Terminos y condiciones</Text>
                                        <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                        <View style={{marginBottom:10}}>
                                            <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                                <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </View>
                </View>
            </ScrollView>     
        </View>
    )

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:surface,
        paddingHorizontal:20,

    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    box: {
        width:120,
        height:127,
        //borderColor: 'black',
        //  borderWidth: 1,
        borderRadius: 4,
        backgroundColor: "#ffffff",
        marginRight: 10,
        left: -20,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.49,
        shadowRadius: 4.65,
        elevation: 3,
    },
    box1: {
        flexDirection:'row',
        padding: 16,
        width: 440,
        height: 200,
        backgroundColor: "#FFFFFF"    
    },
    button2: {
        borderRadius: 10,
        backgroundColor: "#FFFFFF",
        alignItems: 'center', 
        padding: 6,
        width: 150,
        height:34,
        marginTop:15,
        marginLeft: 150,
        borderWidth: 1,
        borderColor:'#005A6D',
        left:20
        
    },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        bottom:0,
        width:width,
        marginTop:100
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        alignContent: 'center',
        justifyContent: 'center',
        marginLeft:80
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:6,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
})