import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TextInput,Alert,Modal,LogBox,TouchableWithoutFeedback} from 'react-native';
import moment from "moment";
import 'moment/locale/es';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, primary500 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Non-serializable values were found in the navigation state'],["Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function."]);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function SumaryReserve({route, navigation }) {
    const [isSelected, setSelection] = useState(false);
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [token, setToken] = useState(null);
    const [date, setDate] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [hour, setHour] = useState();    
    const [hourFinal, setHourFinal] = useState();
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [plate, setPlate] = useState(null);
    const [duration, setDuration] = useState(null);
    const [price, setPrice] = useState(null);
    const [dialogReserveVisible,setDialogReserveVisible] = useState(true);


    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            getVehicles(value);
          }
        } catch (error) { }
      };
////Método para calcular fechas
    const dates = () => {

    }
    ///Método para traer la info del parking
    const getParkingInfo = () => {
        
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);

            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
    }
    const terms = () => {
        setDialogTermsVisible(true);
    }
    //Metodo para traer data de la reserva
    const getDataReserve = () => {
        setSpinner(true);
        let config = {
            headers: { Authorization: global.token }
            };
        axios.get(`${API_URL}customer/app/reservation/get/`+global.idReserva,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                var date = moment(response.data.ResponseMessage.initial_date).format('MMMM DD YYYY');
                var timeI = moment(response.data.ResponseMessage.initial_date).format('HH:mm');
                setHour(timeI);
                var timeF = moment(response.data.ResponseMessage.final_date).format('HH:mm');
                setDuration(response.data.ResponseMessage.duration);
                setHourFinal(timeF);
                setDate(date);
                setPrice(response.data.ResponseMessage.price);
                setTypeVehicleId(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.vehicle_type_id);
                setPlate(response.data.ResponseMessage.customer_product_has_vehicle.vehicle.plate);
            }else{
              Alert.alert("ERROR","No se en traer datos de reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data);
          })
        
      };

      //metodo para pagar la reserva
      const payReserve = () =>{
          
        let config = {
            headers: { Authorization: global.token }
            };
            
        setSpinner(true);
        axios.put(`${API_URL}customer/app/reservation/pay/`+global.idReserva,{},config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                Alert.alert("",response.data.ResponseMessage);
                navigation.navigate('ScheduledReserve');
            }else{
              Alert.alert("ERROR","No se pudo pagar la reserva");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            if(error.response.data.ResponseCode == 1002){
                navigation.navigate('ServiceReserve');
            }
            console.log(error.response);
          })
      }
    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getParkingInfo();
          getDataReserve();
          getToken();
          dates();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}} showsVerticalScrollIndicator={true}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ServiceReserve')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{flex: 1, backgroundColor: surface, padding:20, paddingTop:10,}}>
                    <Text style={{color: secondary,fontSize: 24,fontFamily: 'Gotham-Bold',}}>Reserva</Text>
                    <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <View >
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>
                                        {
                                            parking !== null ?
                                            parking.name
                                            :
                                            ""
                                        }
                                    </Text>
                                    {
                                        parking !== null ?
                                            parking.address !== null ? 
                                                <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                            :
                                            <Text style={styles.text}>No hay dirección disponible</Text>
                                        :
                                        <Text style={styles.text}>No hay dirección disponible</Text>
                                    }
                                    <View style={{flexDirection: 'row'}} >
                                        {
                                            parking !== null &&
                                                parking.open == 1 ? 
                                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                                :
                                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                                        }    
                                            
                                        {
                                            parking !== null ?
                                                parking.finalHour !== "" && parking.initialHour !== "" ? 
                                                    <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                                :
                                                <Text style={styles.text}>No hay horario disponible</Text>
                                            :
                                            <Text style={styles.text}>No hay horario disponible</Text>
                                        }
                                    </View>
                                </View>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',marginLeft:'auto',marginTop:20}}>
                                    <MaterialIcons name="assistant-direction" size={24} color={secondary} />    
                                </View>
                            </View>
                        </View> 
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1, backgroundColor: surface, padding:22, paddingTop:10,marginTop:16}}> 
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Fecha</Text>
                        <Text style={styles.textBold}>{date}</Text>
                    </View>
                    <View style={{marginTop:20}}>
                        <Text style={styles.text}>Tiempo adquirido</Text>
                        <Text style={styles.textBold}>{duration === 1 ? duration+' Hora' : duration && duration+' Horas' }</Text>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Hora inicial</Text>
                            <Text style={styles.textBold}>
                                {hour}
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Hora final</Text>
                            <Text style={styles.textBold}>{hourFinal}</Text>
                        </View>
                    </View>
                    <View style={{marginTop:20,flexDirection:'row'}}>
                        <View>
                            <Text style={styles.text}>Tipo de vehículo</Text>
                            <Text style={styles.textBold}>
                                {typeVehicleId === 1 ? 'Automóvil' : typeVehicleId === 2 ? 'Motocicleta' : typeVehicleId === 3 ? 'Bicicleta':''}
                            </Text>
                        </View>
                        <View style={{marginLeft:40}}>
                            <Text style={styles.text}>Placas del vehículo</Text>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',textTransform:'uppercase'}}>{plate}</Text>
                        </View>
                    </View>
                </View>
                <View style={{backgroundColor: surface, padding:20, marginTop:16,}}>
                <TouchableOpacity style={{justifyContent:'center'}} >
                    <View
                    style={{
                        backgroundColor: surface,
                        borderRadius: 10,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        padding:15,
                    }}
                    >
                        <View style={{flexDirection: 'row'}}>
                            <Ionicons name="card-outline" size={24} color={secondary} />
                            <View style={{marginLeft:30}}>
                                <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                                <Text style={styles.text}>Pepito Perez</Text>
                            </View>
                            <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>CAMBIAR</Text>
                        </View>
                    </View> 
                </TouchableOpacity>           
                </View>
                <View style={styles.container}>
                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>Resumen</Text>
                    <View style={{flexDirection:'row',marginTop:20}}>
                        <View>
                            <Text style={styles.text}>Costo del servicio</Text>
                        </View>
                        <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>${parking !== null ? 
                                        typeVehicleId == 1 ? parking.tariff[0].price : typeVehicleId == 2 ? parking.tariff[1].price : typeVehicleId == 3 ? parking.tariff[2].price : '0'
                                        :'0'
                                        }</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',marginTop:20}}>
                        <View>
                            <Text style={styles.text}>N valor de descuento</Text>
                        </View>
                        <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                            <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>-${parking !== null && price ? 
                                        typeVehicleId == 1 ? parking.tariff[0].price-price : typeVehicleId == 2 ? parking.tariff[1].price-price : typeVehicleId == 3 ? parking.tariff[2].price-price : '0'
                                        :'0'
                                        }</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.container2}>
                <View style={{flexDirection:'row'}}>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ alignSelf: "center",marginLeft:10,color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    />
                    <TouchableOpacity onPress={terms}>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Light',marginTop:10,textDecorationLine:'underline'}}>Acepto terminos y condiciones</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogReserveVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <Text style={styles.title2}>La reserva se ha creado correctamente</Text>
                                    <Text style={styles.title2}>Tienes 10 minutos para realizar el pago de tu reserva</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center',marginTop:10}}>
                                    <Text style={styles.text2}>Recuerda , tienes 10 minutos para realizar el pago de tu reserva o sera eliminada</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={() => setDialogReserveVisible(!dialogReserveVisible)}>
                                        <Text style={styles.titleButton1}>ACEPTAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminos y condiciones</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                <View style={{marginVertical:20,alignItems:'center',}}>
                    {
                        isSelected ?
                            <TouchableOpacity style={styles.button1} onPress={() => payReserve()}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={styles.titleButton1}>PAGAR</Text>
                                    <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                        <Text style={styles.titleButton1}>Total ${price}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        :
                        <TouchableOpacity disabled style={{backgroundColor: OnSurfaceOverlay15,padding: 14,borderRadius: 10,width: width-30,}}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>PAGAR</Text>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                    <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>Total ${price}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                </View>

            </View>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      paddingBottom:width-200,
      marginTop:16,
    },
    containerAccordion: {
        flex: 1,
        backgroundColor: surface,
        marginTop:16,
      },
    container2: {
        borderTopRightRadius:25,
        borderTopLeftRadius:25,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:10,
        shadowColor:secondary,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: secondary,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign: 'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })