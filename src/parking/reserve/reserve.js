import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions,Alert,Modal,LogBox,TouchableWithoutFeedback} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Checkbox from 'expo-checkbox';
import { API_URL } from '../../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { primary800,secondary,surface, colorGray, colorGrayOpacity,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

export default function Reserve({navigation}) {
    const [isSelected, setSelection] = useState(false);
    const [date, setDate] = useState('Hoy');
    const [dateT, setDateT] = useState(null);
    const [hour, setHour] = useState(null);
    const [dateSend, setDateSend] = useState(null);
    const [showDate, setShowDate] = useState(false);
    const [showTime, setShowTime] = useState(false);
    const [firstHour, setFirstHour] = useState(false);
    const [secondHour, setSecondHour] = useState(false);
    const [thirdHour, setThirdHour] = useState(false);
    const [fourthHour, setFourthHour] = useState(false);
    const [typeVehicleId, setTypeVehicleId] = useState(null);
    const [firstDisabled, setFirstDisabled] = useState(true);
    const [secondDisabled, setSecondDisabled] = useState(true);
    const [thirdDisabled, setThirdDisabled] = useState(true);
    const [fourthDisabled, setFourthDisabled] = useState(true);
    const [licensePlate, setLicensePlate] = useState(null);
    const [times, setTimes] = useState(null);
    const sheetRef = React.useRef(null);
    const [dialogTermsVisible, setDialogTermsVisible] = useState(false);
    const [spinner,setSpinner] = useState(false);
    const [parking,setParking] = useState(null);
    const [vehicles, setVehicles] = useState(false);
    const [token, setToken] = useState(null);
    const [openHour, setOpenHour] = useState(null);
    const [closeHour, setCloseHour] = useState(null);
    const [dateMax, setDateMax] = useState(null);
    const [dateOnly, setDateOnly] = useState(null);
    const [datePlusTime, setDatePlusTime] = useState(null);
    const [price, setPrice] = useState(0);
    const [dateFinal, setDateFinal] = useState(null);
    const [dateInitial, setDateInitial] = useState(null);
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
            //getVehicles(value);
            getParkingInfo();
            
          }
        } catch (error) { }
      };

    const getDataStorage = async () => {
        setTypeVehicleId(global.vehicleTypeId);
        setLicensePlate(global.licensePlate);
      };

    ///Método para traer la info del parking
    const getParkingInfo = async () => {
        setSpinner(true);
        //var id = parseInt(global.parkingId);
        var id =  await AsyncStorage.getItem('parkingId');
        global.parkingId = id;
        //console.log(id);
         axios.get(`${API_URL}user/parking/show/`+id).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                setParking(response.data.ResponseMessage);
                //console.log(response.data.ResponseMessage);
                var capacities = response.data.ResponseMessage.capacity;
                capacities.forEach(capacity => {
                    if(capacity.price_1){
                        setFirstDisabled(false);
                    }
                    if(capacity.price_2){
                        setSecondDisabled(false);
                    }
                    if(capacity.price_3){
                        setThirdDisabled(false);
                    }
                    if(capacity.price_4){
                        setFourthDisabled(false);
                    }
                    
                });
                
                const hourO = response.data.ResponseMessage.initialHour.slice(0,-3);
                const hourF = response.data.ResponseMessage.finalHour.slice(0,-3);
                setOpenHour(hourO);
                setCloseHour(hourF);
            }else{
              Alert.alert("ERROR","No se pudo traer el parqueadero");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            navigation.navigate('BarNavigationRegister');
            console.log(error.response.data.ResponseMessage);
          }) 
    }

    const changeDateAndHour = () => {
        setShowDate(true);
    }
    const onChange = (event, selectedDate) => {
        setShowDate(false);
        if (event.type !== "dismissed") {
            const date = moment(String(selectedDate)).format("YYYY-MM-DD");
            setDate(date);
            setDateOnly(selectedDate);
            setShowTime(true);
            setShowDate(false);
        }
        setShowDate(false);
    }
    const onChangeTime = (selectedDate) => {
        var hours = selectedDate.getHours();
        var minutes = selectedDate.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        const date1 = moment(new Date).add(2,'h');
        const dateNow = new Date(date1);            
        if((dateOnly.getMonth() === new Date().getMonth()) && (dateOnly.getDate() === new Date().getDate())){
            
            if((selectedDate.getHours() >= dateNow.getHours())){
                var minuts = null;
                if(selectedDate.getMinutes() < 10){
                    minuts = "0"+selectedDate.getMinutes();
                }else{
                    minuts = selectedDate.getMinutes();
                }
                const dateSendFinal = new Date(dateOnly.setHours(selectedDate.getHours(),selectedDate.getMinutes(),selectedDate.getSeconds()));
                setDateFinal(date + " "+selectedDate.getHours()+":"+minuts+":"+selectedDate.getSeconds());
                setHour(strTime);
                setDateSend(dateSendFinal);
                setShowTime(false);
            }else{
                Alert.alert("","Debe ingresar una hora que sea mayor a dos horas desde este momento.");
            }
        }else{
                var minuts = null;
                if(selectedDate.getMinutes() < 10){
                    minuts = "0"+selectedDate.getMinutes();
                }else{
                    minuts = selectedDate.getMinutes();
                }
                const dateSendFinal = new Date(dateOnly.setHours(selectedDate.getHours(),minuts,selectedDate.getSeconds()));
                setDateFinal(date + " "+selectedDate.getHours()+":"+minuts+":"+selectedDate.getSeconds());
                setHour(strTime);
                setDateSend(dateSendFinal);
                setShowTime(false);
                
            }
        setShowTime(false);
    }

    const changeFirst = () => {
        setFirstHour(true);
        setSecondHour(false);
        setThirdHour(false);
        setFourthHour(false);
        setTimes(1);
        calcHourFinal(1); 
    }
    const changeSecond = () => {
        setFirstHour(false);
        setSecondHour(true);
        setThirdHour(false);
        setFourthHour(false);
        setTimes(2);
        calcHourFinal(2); 
    }
    const changeThird = () => {
        setFirstHour(false);
        setSecondHour(false);
        setThirdHour(true);
        setFourthHour(false);
        setTimes(3);
        calcHourFinal(3);
    }
    const changeFourth = () => {
        setFirstHour(false);
        setSecondHour(false);
        setThirdHour(false);
        setFourthHour(true);
        setTimes(4);
        calcHourFinal(4);
    }

    const calcHourFinal = (times) =>{
        if(global.vehicleId){
            const hourFinal = moment(dateSend).add(times,'h');
            const datePlusF = new Date(hourFinal);
            setDatePlusTime(datePlusF);
            getPrice(times);
        }else{
            setFirstHour(false);
        setSecondHour(false);
        setThirdHour(false);
        setFourthHour(false);
        Alert.alert("","Debes seleccionar un vehículo");
        }
    }

    const terms = () => {
        setDialogTermsVisible(true);
    }
    ///////////verificar si tiene vehiculos registrados
    const getVehicles = (token) => {
        /* let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        var id = parseInt(global.parkingId);
        axios.get(`${API_URL}customer/app/list/vehicle`,config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage.vehicles.length === 0){
                    setVehicles(false);
                }else{
                    setVehicles(true);
                }
            }else{
              Alert.alert("ERROR","No se pudo traer los vehiculos");
            }
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          }) */
    }
    const goToVehicles = () => {
        /* if(!vehicles){
            navigation.navigate('NoVehicle');
        }else{
            
    } */
        navigation.navigate('ListVehicleReserve');
        console.log('llego');
        }
    const goToSumaryReserve = () => {
            //Alert.alert("","se registro la reserva");
            //console.log(`${price} -- ${times} -- ${global.vehicleId} -- ${global.parkingId} -- ${dateFinal}`);
           //navigation.navigate('SumaryReserve');
        global.dataVehicle = {
            licensePlate:licensePlate,
            typeVehicleId:typeVehicleId,
            times:times,
            dateSend:dateSend,
            datePlusTime:datePlusTime,
        }
        let config = {
            headers: { Authorization: token }
            };
        setSpinner(true);
        axios.post(`${API_URL}customer/app/reservation/create`,{
            initialDate: dateFinal,
            price: price,
            parking_id: global.parkingId,
            vehicle_id: global.vehicleId,
            duration: times,
        },config).then(response => {
            setSpinner(false);
            const cod = response.data.ResponseCode;
            //Alert.alert("","se registro la reserva");
             if(cod == 0){
                //Alert.alert("",response.data.ResponseMessage);
                global.idReserva = response.data.Data.id;
                navigation.navigate('SumaryReserve');
            }else{
              Alert.alert("ERROR","No se pudo registrar la reserva");
            } 
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response.data.ResponseMessage);
          })
        
    }
//Metodo de fecha mayor
    const dateMaxCalc = () => {
        const dateString = moment(String(new Date)).add(2,'M');
        const date = new Date(dateString);
        setDateMax(date);
    }

    ///metodo para traer el precio
    const getPrice = (time) => {
       // console.log(`${global.vehicleId} -- ${time} -- ${global.parkingId}`);
       let config = {
            headers: { Authorization: global.token }
            };
        setSpinner(true);
        axios.post(`${API_URL}customer/app/get/reservation/price`,{
            vehicle_id: global.vehicleId,
            parking_id: global.parkingId,
            duration: time,
        },config).then(response => {
            setSpinner(false);
            //console.log(JSON.stringify(response.data));
             const cod = response.data.ResponseCode;
            if(cod === 0){
                if(response.data.ResponseMessage >= 0){
                    setPrice(response.data.ResponseMessage);
                }else{
                    Alert.alert("",response.data.ResponseMessage);
                }
            }else{
              Alert.alert("ERROR","En traer precio");
            } 
          }).catch(error => {
            setSpinner(false);
            Alert.alert("",error.response.data.ResponseMessage);
            console.log(error.response);
          }) 
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getDataStorage();
          dateMaxCalc();
          getToken();
          global.reserve = true;
          global.pasadia = false;
          global.valet = false;
          var dates = moment(new Date).add(2,'h');
          var dateI = new Date(dates);
          setDateInitial(dateI);
          //console.log(dateI);
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.navigate('ListParkingReserve')}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                    <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Reservas</Text>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:20 }}>
                    <View style={{ marginTop: 15}}>
                        <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                        {
                            parking !== null ?
                            parking.name
                            :
                            ""
                        }
                        </Text>
                        {
                            parking !== null ?
                                parking.address !== null ? 
                                    <Text style={styles.text}> {parking.address.abbreviation} {parking.address.number_1}{parking.address.letter_1}{parking.address.cardinal_point_1} {parking.address.char_1} {parking.address.number_2}{parking.address.letter_2}{parking.address.cardinal_point_2} {parking.address.char_2} {parking.address.number_3}</Text>
                                :
                                <Text style={styles.text}>No hay dirección disponible</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        }
                        
                        <View style={{flexDirection: 'row'}} >
                        {
                            parking !== null &&
                                parking.open == 1 ? 
                                <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                                :
                                <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                        }    
                            
                            {
                                parking !== null ?
                                    parking.finalHour !== "" && parking.initialHour !== "" ? 
                                        <Text style={styles.text}>{openHour} - {closeHour}</Text>
                                    :
                                    <Text style={styles.text}>No hay horario disponible</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            }
                        </View>
                    </View>
                    <View style={{alignItems: 'center',justifyContent: 'center',}}>
                        <View style = {styles.lineStyle} />
                    </View>
                    <View>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Medium',}}>Tarifa base / Disponibilidad</Text>
                    </View>
                    <View style={{flexDirection:'row',}}>
                        {
                            parking !== null ?
                                parking.tariff.length !== 0 ?
                                
                                    parking.tariff.map( (tarifa) => {
                                        return <View style={{flexDirection:'row',marginTop:10, marginRight:10}} key={tarifa.vehicleType}>
                                                    <FontAwesomeIcon size={20} 
                                                    icon={tarifa.vehicleType === 1 ? faCar
                                                        :tarifa.vehicleType === 2 ? faMotorcycle :tarifa.vehicleType === 3 && faBicycle } color={primary700} />
                                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> ${tarifa.price} / <Text style={{color: surfaceMediumEmphasis}}>24</Text></Text>
                                                </View>
                                        })
                                :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                            :
                                <View style={{flexDirection:'row',marginTop:10, marginRight:30}}>
                                    <Text style={{color: secondary,fontSize: 13,fontFamily: 'Gotham-Medium',}}> No hay tarifas disponibles</Text>
                                </View>
                        }
                    </View>
                </View>
                <View style={styles.container}>
                    <Text style={{color: secondary,fontSize: 18,fontFamily: 'Gotham-Bold',}}>Reservar para el día de</Text>
                    <TouchableOpacity style={{justifyContent:'center'}} onPress={changeDateAndHour}>
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 5,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            padding:10,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <FontAwesomeIcon size={20} icon={faCalendar} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                <View>
                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                        {date}
                                    </Text>
                                    <Text style={styles.text}>{hour ? hour : 'Hora'}</Text>
                                </View>
                                <Text style={{ fontSize: 14, marginTop: 20,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800,fontStyle:'italic'}}>CAMBIAR</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    {showDate && (
                        <DateTimePicker
                        minimumDate={new Date()}
                        value={new Date()}
                        mode="date"
                        is24Hour={true}
                        display="calendar"
                        onChange={onChange}
                        maximumDate={dateMax}
                        textColor="black"
                        />
                    )}
                    {showTime && dateInitial ? (
                        // <DateTimePicker
                        // value={dateInitial}
                        // minimumDate={dateInitial}
                        // mode="time"
                        // is24Hour={true}
                        // display="clock"
                        // onChange={onChangeTime}                        
                        // />
                        <DateTimePickerModal
                            isVisible={showTime}
                            mode="time"
                            date={dateInitial}
                            onConfirm={(selectedDate) => onChangeTime(selectedDate)}
                            onCancel={() => {
                            setShowTime(false)
                            }}
                            display="spinner"
                        />
                    )
                :
                <Text> </Text>
                }
                    <TouchableOpacity style={{justifyContent:'center'}} onPress={goToVehicles}>
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 5,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            marginTop:10,
                            paddingTop:5,
                            paddingHorizontal:10,
                        }}
                        >
                            <View style={{flexDirection: 'row',marginBottom:10,}}>
                                {
                                    typeVehicleId === null &&
                                    <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                                {
                                    typeVehicleId === 1 &&
                                    <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                                {
                                    typeVehicleId === 2 &&
                                    <FontAwesomeIcon size={20} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                                {
                                    typeVehicleId === 3 &&
                                    <FontAwesomeIcon size={20} icon={faBicycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                }
                                <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                    Vehículo
                                </Text>
                                <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:10,marginLeft:60 }}>
                                    {licensePlate}
                                </Text>
                                <FontAwesomeIcon size={20} icon={faChevronRight} color={secondary} style={{marginLeft:30,marginTop:10}}/>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View
                    style={{
                        backgroundColor: surface,
                        borderRadius: 5,
                        width:width-38,
                        marginLeft: 0,
                        marginRight: 1,
                        shadowColor: "#000",
                        shadowOffset: {
                        width: 0,
                        height: 1,
                        },
                        shadowOpacity: 0.49,
                        shadowRadius: 4.65,
                        elevation: 3,
                        marginBottom:10,
                        marginTop:10,
                        paddingTop:5,
                        paddingHorizontal:6,
                    }}
                    >
                        <View style={{flexDirection: 'row',marginBottom:10}}>
                            <FontAwesomeIcon size={20} icon={faStopwatch} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                            <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                Duración
                            </Text>
                            <View style={{flexDirection:'row',alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',paddingRight:5}}>
                                <Text style={{ fontFamily: "Gotham-Medium", color: surfaseDisabled,fontSize:12,marginTop:10,marginLeft:50,textDecorationLine:'line-through' }}>
                                    ${parking !== null ? 
                                        typeVehicleId == 1 ? parking.tariff[0].price : typeVehicleId == 2 ? parking.tariff[1].price : typeVehicleId == 3 ? parking.tariff[2].price : '0'
                                        :'0'
                                        }
                                </Text>
                                <Text style={{ fontFamily: "Gotham-Medium", color: primary700,fontSize:14,marginTop:10,marginLeft:5 }}>
                                    ${price}
                                </Text>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row',marginBottom:10,marginLeft:10,overflow:'scroll'}}>
                            <Chip textStyle={{fontSize:12}} style={firstHour ? {backgroundColor:colorPrimarySelect, marginRight:6,}:{backgroundColor:onSurfaceOverlay8, marginRight:6,}}  selected={firstHour} selectedColor={secondary} mode='flat' onPress={changeFirst} disabled={firstDisabled}>1 Hora</Chip>
                            <Chip textStyle={{fontSize:12}} style={secondHour ? {backgroundColor:colorPrimarySelect,marginRight:6}:{backgroundColor:onSurfaceOverlay8, marginRight:6}} selected={secondHour} selectedColor={secondary} mode='flat' onPress={changeSecond} disabled={secondDisabled}>2 Horas</Chip>
                            <Chip textStyle={{fontSize:12}} style={thirdHour ? {backgroundColor:colorPrimarySelect,marginRight:6}:{backgroundColor:onSurfaceOverlay8, marginRight:6}} selected={thirdHour} selectedColor={secondary} mode='flat' onPress={changeThird} disabled={thirdDisabled}>3 Horas</Chip>
                            <Chip textStyle={{fontSize:12}} style={fourthHour ? {backgroundColor:colorPrimarySelect}:{backgroundColor:onSurfaceOverlay8, }} selected={fourthHour} selectedColor={secondary} mode='flat' onPress={changeFourth}disabled={fourthDisabled}>4 Horas</Chip>  
                        </View>
                    </View>
                    
                </View>
                <View>

                </View>
            </ScrollView>
            <View style={styles.container2}>
                <View style={{flexDirection:'row'}}>
                    <Checkbox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ color:primary800}}
                        color={isSelected ? primary600 : surfaceMediumEmphasis}
                    />
                    <TouchableOpacity onPress={terms}>
                        <Text style={{color: surfaceMediumEmphasis,fontSize: 13,fontFamily: 'Gotham-Light',marginTop:10,textDecorationLine:'underline'}}>Acepto terminos y condiciones</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={dialogTermsVisible}
                >
                    <TouchableWithoutFeedback onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Terminos y condiciones</Text>
                                <Text style={styles.text2}>Tortor bibendum pretium lacinia at risus. Suspendisse volutpat, neque felis, dui, sagittis sapien commodo vulputate. Quis eget tortor amet ipsum, morbi mollis semper. Commodo leo imperdiet fermentum lobortis suspendisse. Dictumst eget in sed nibh. Eu volutpat ut vel adipiscing erat gravida maecenas ut vitae. Nunc sodales arcu magna in libero in. Ligula eget integer sed diam elit tristique. </Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => setDialogTermsVisible(!dialogTermsVisible)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>ACEPTAR Y CONTINUAR</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                <View style={{marginVertical:20,alignItems:'center',}}>

                    {
                        date!=='Hoy' && hour!=='Hora' && licensePlate!==null && times!==null && isSelected!==false ?

                        <TouchableOpacity style={styles.button1} onPress={() => goToSumaryReserve()}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.titleButton1}>IR A PAGAR</Text>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                    <Text style={styles.titleButton1}>Total ${price}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={{backgroundColor: OnSurfaceOverlay15,padding: 14,borderRadius: 10,width: width-30,}} disabled={true}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>IR A PAGAR</Text>
                                <View style={{alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto'}}>
                                    <Text style={{color: OnSurfaceDisabled,fontSize: 15,fontFamily:'Gotham-Bold',}}>Subtotal ${price}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>    
                    }
                </View>

            </View>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:width-170,
      paddingTop:10,
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:20,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Bold',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:50,
        marginTop:20,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })