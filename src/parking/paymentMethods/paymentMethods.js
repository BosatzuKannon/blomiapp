import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, TouchableWithoutFeedback,Alert,Modal,LogBox,Image,StatusBar,Animated, Pressable} from 'react-native';
import moment from "moment";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { TextInput as PaperTextInput } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight, faBan,faCheck} from '@fortawesome/free-solid-svg-icons';
import { API_URL } from '../../../url';
import {Picker} from '@react-native-picker/picker';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import { TabView, SceneMap } from 'react-native-tab-view';
import { MaterialIcons,Feather,Ionicons,FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';
// import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { primary800,secondary,surface, colorGray, colorGrayOpacity,errorColor,primary500,
    colorPrimarySelect,onSurfaceOverlay8,OnSurfaceOverlay15, colorInput, colorInputBorder, 
    colorPrimaryLigth, primary700, OnSurfaceDisabled, primary600,surfaceMediumEmphasis, surfaseDisabled, surfaceHighEmphasis, primary50 } from '../../utils/colorVar';
    LogBox.ignoreLogs(['Deprecation warning: value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions.']);
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;
// const Tab = createMaterialTopTabNavigator();

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#ff4081',height:height }]} />
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
  thirt: FirstRoute,
});
export default function PaymentMethods({navigation }) {
    const sheetRef = React.useRef(null);
    const [spinner,setSpinner] = useState(false);
    const [token, setToken] = useState(null);
    const [index, setIndex] = useState(0);
    const [activity, setActivity] = useState(false);
    const [activity2, setActivity2] = useState(false);
    const [typePersonId, setTypePersonId] = useState(null);
    const [modalCancel, setModalCancel] = useState(false);
    const [modalPay, setModalPay] = useState(false)
    const [typePerson, setTypePerson] = useState([
        {
          name:'Persona Natural',
          id:1,
        },
        {
          name:'Persona Juridica',
          id:2,
        },
    ])
      const [typeBankId, setTypeBankId] = useState(null);
      const [typeBank, setTypeBank] = useState([
        {
          name:'Bancolombia',
          id:1,
        },
        {
          name:'Banco de Bogotá',
          id:2,
        },
         {
          name:'Davivienda',
          id:3,
        },
    ])
    const [routes] = useState([
      { key: 'first', title: 'MEDIOS PARKING' },
      { key: 'second', title: 'PAGO PSE' },
      { key: 'thirt', title: 'TARJETA DE CREDITO' },
    ]);


    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
            setToken(value);
          }
        } catch (error) { }
      };

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
          getToken();
        });
        return unsubscribe;
      }, [navigation]);

    return(
        <View>
            <ScrollView style={{backgroundColor:colorGray}}>
                <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                    <TouchableOpacity onPress={() => navigation.goBack(null)}>
                        <MaterialIcons name="arrow-back" size={24} color={secondary} />
                    </TouchableOpacity>
                </View>
                <Spinner visible={spinner}  color={primary600} />
                <View style={{paddingHorizontal: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10 }}>
                  <Text style={{fontFamily:'Gotham-Bold',color:secondary,fontSize:24}}>Medio de pago</Text>
                  <Text style={{fontFamily:'Gotham-Light',color:secondary,fontSize:16,margin:10}}>Seleccione el método de pago</Text>
                </View>
                  {/* <View style={{ marginTop: 10}}>
                  <TabView
                    navigationState={{ index, routes }}
                    renderScene={renderScene}
                    onIndexChange={setIndex}
                    initialLayout={{ width: width }}
                  />
                </View>  */}
                <View style={{flexDirection:'row', marginTop:20, justifyContent:'center', alignItems:'center'}}>
                  <View>
                    <TouchableOpacity style={{marginRight:20}} onPress={() => setActivity(!activity)}>
                      <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>PAGO PSE</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <TouchableOpacity onPress={() => setActivity2(!activity2)}>
                      <Text style={{color:secondary, fontSize:14, fontFamily:'Gotham-Bold'}}>TARJETA CREDITO</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                {/* actividad */}
                <View>
                  {
                    activity ? 
                    <View style={{marginTop:20}}>
                      <View style={{justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                        <View style={styles.pickerStyle}>
                            <Picker 
                                selectedValue={typePersonId}
                                style={{height: 40, width: width-60,borderColor: secondary,borderWidth: 1}} 
                                onValueChange={(itemValue, itemIndex) => setTypePersonId(itemValue)}>
                                    <Picker.Item label="Tipo de documento" color={colorInput} value={null} enabled={false}/>
                                    {
                                    typePerson.map( (type) => {
                                    return <Picker.Item label={type.name} color={colorInput} value={type.id} key={type.id}/>
                                    })
                                    }  
                            </Picker>
                        </View> 
                      </View>
                      <View style={{justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                        <View style={styles.pickerStyle}>
                          <Picker 
                            selectedValue={typeBankId}
                            style={{height: 40, width: width-60,borderColor: secondary,borderWidth: 1}} 
                            onValueChange={(itemValue, itemIndex) => setTypeBankId(itemValue)}>
                                <Picker.Item label="Banco" color={colorInput} value={null} enabled={false}/>
                                {
                                typeBank.map( (type) => {
                                return <Picker.Item label={type.name} color={colorInput} value={type.id} key={type.id}/>
                                })
                                }  
                          </Picker>
                        </View> 
                      </View>
                      <View style={{display:'flex', justifyContent:'center', alignItems: 'center', marginTop:350}}>
                        <TouchableOpacity style={{backgroundColor:secondary, borderRadius:15, width:360,height:48 }}  onPress={() => setModalCancel(true)}>
                          <Text style={{color:'#F2FAE0', textAlign:'center', marginTop:'auto', marginBottom:'auto', fontSize:18,fontFamily:'Gotham-Medium'}}>SIGUIENTE</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                      :
                     activity2 ? 
                     <View>
                        <View style={{backgroundColor: surface, padding:20, marginTop:16,}}>
                      <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginTop:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <Ionicons name="card-outline" size={24} color={secondary} />
                                <View style={{marginLeft:30}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>VISA *111</Text>
                                    <Text style={styles.text}>Pepito Perez</Text>
                                </View>
                                <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>SELECCIONAR</Text>
                            </View>
                        </View> 
                      </TouchableOpacity> 
                     </View>
                    <View style={{backgroundColor: surface, padding:20}}>
                      <TouchableOpacity style={{justifyContent:'center'}} >
                        <View
                        style={{
                            backgroundColor: surface,
                            borderRadius: 10,
                            width:width-38,
                            marginLeft: 0,
                            marginRight: 1,
                            shadowColor: "#000",
                            shadowOffset: {
                            width: 0,
                            height: 1,
                            },
                            shadowOpacity: 0.49,
                            shadowRadius: 4.65,
                            elevation: 3,
                            marginBottom:10,
                            padding:15,
                        }}
                        >
                            <View style={{flexDirection: 'row'}}>
                                <Ionicons name="card-outline" size={24} color={secondary} />
                                <View style={{marginLeft:30}}>
                                    <Text style={{color: secondary,fontSize: 16,fontFamily:'Gotham-Bold',}}>American Express</Text>
                                    <Text style={styles.text}>Pepito Perez</Text>
                                </View>
                                <Text style={{ fontSize: 14, marginTop: 10,alignItems:'flex-end',alignContent:'flex-end',justifyContent:'flex-end',marginLeft:'auto',fontFamily:'Gotham-Medium', color:primary800}}>SELECCIONAR</Text>
                            </View>
                        </View> 
                      </TouchableOpacity> 
                     </View>
                       <View style={{display:'flex', justifyContent:'center', alignItems: 'center', marginTop:240}}>
                        <TouchableOpacity style={{backgroundColor:secondary, borderRadius:15, width:360,height:48 }}  onPress={() => setModalPay(true)}>
                          <Text style={{color:'#F2FAE0', textAlign:'center', marginTop:'auto', marginBottom:'auto', fontSize:18,fontFamily:'Gotham-Medium'}}>SIGUIENTE</Text>
                        </TouchableOpacity>
                      </View>
                     </View>  
                      :
                       <View style={{justifyContent:'center', alignItems:'center', marginTop:50}}>
                        <Text>SELECCIONA</Text>
                      </View>
                    
                  }
                </View>
                {/* modal referente al estado del pago errado*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalCancel}
                >
                    <TouchableWithoutFeedback  onPress={() => setModalCancel(!modalCancel)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                     <FontAwesomeIcon size={40} icon={faBan} color={'#D40059'} style={{marginLeft:10,marginTop:12,marginRight:20, marginBottom:20}}/>
                                    <Text style={styles.title2}>Lo sentimos</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center',marginTop:10}}>
                                    <Text style={styles.text2}>Tu pago no se debito correctamente</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={() => navigation.goBack(null)}>
                                        <Text style={styles.titleButton1}>SELECCIONA OTRO MEDIO DE PAGO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                {/* modal referente al estado del pago exitoso*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalPay}
                >
                    <TouchableWithoutFeedback  onPress={() => setModalPay(!modalPay)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                     <FontAwesomeIcon size={40} icon={faCheck} color={'#A1DA26'} style={{marginLeft:10,marginTop:12,marginRight:20, marginBottom:20}}/>
                                    <Text style={styles.title2}>¡Pago exitoso!</Text>
                                </View>
                                <View style={{alignItems: 'center', alignContent:'center',justifyContent:'center',marginTop:10}}>
                                    <Text style={styles.text2}>Comprobante N° 000000</Text>
                                    <Text style={styles.text2}>13/04/2022 - 15:00</Text>
                                </View>
                                <View style={{marginBottom:10,alignItems: 'center', alignContent:'center',justifyContent:'center'}}>
                                    <TouchableOpacity style={styles.button1} onPress={() => navigation.goBack(null)}>
                                        <Text style={styles.titleButton1}>Regresar</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </ScrollView>
        </View>
        
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      padding:20,
      marginBottom:height-450
    },
    container2: {
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        flex: 1,
        backgroundColor: surface,
        paddingHorizontal:10,
        paddingBottom:5,
        paddingTop:10,
        elevation:5,
        position:'absolute',
        bottom:0,
        width:width,
      },
    title: {
        color: secondary,
        fontSize: 24,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent:'center',
        marginBottom:20
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 12,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: surfaceHighEmphasis,
        fontSize: 13,
        fontFamily: 'Gotham-Medium',
        marginBottom:20,
        marginTop:20,
        textAlign:'center',
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: '#FFF',
        fontSize: 15,
        fontFamily:'Gotham-Bold',
        textAlign:'center',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 15,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
    button2: {
        backgroundColor: surface,
        padding: 10,
        borderRadius: 10,
        width: width-60,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'center'
    },
    titleButton2: {
        color: secondary,
        fontFamily:'Gotham-Medium',
        fontSize: 13,
        textAlign:'center',
    },
    textBold: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'capitalize'
    },
    textBoldUp: {
        color: secondary,
        fontSize: 16,
        fontFamily:'Gotham-Bold',
        textTransform:'uppercase'
    },
    scene: {
      flex: 1,
    },
  })