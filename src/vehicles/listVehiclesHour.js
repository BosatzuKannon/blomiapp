import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,CheckBox,Dimensions, TextInput, Alert} from 'react-native';
import { faArrowLeft,faCar,faMotorcycle,faBicycle,faStopwatch,faCarSide,faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {faHeart,faCalendar} from '@fortawesome/free-regular-svg-icons';
import { faTimes} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import Dialog from "react-native-dialog";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Chip } from 'react-native-paper';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from '../../url';
import axios from "axios";
import Spinner from "react-native-loading-spinner-overlay";
import {productIdReserve} from "../utils/varService";
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import {primary600, primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis,colorPrimarySelect,surfaseDisabled, colorInput, colorInputBorder, colorPrimaryLigth, primary700 } from '../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import Toast from 'react-native-simple-toast';
import { Request } from '../utils/api';
import { VEHICLE, FACILITY, PRODUCT } from '../utils/endpoints';

export default function ListVehiclesHour({route,navigation}) {
    const [vehicles, setVehicles] = useState([]);
    const [parkingInfo, setParkingInfo] = useState(null);
    const [spinner,setSpinner] = useState(false);
    const   { facility_id } = route.params

    const saveProduct = async (vehicle) =>{
        // const temp = await AsyncStorage.getItem("vehicle");
        // await AsyncStorage.setItem("vehicle", JSON.stringify(vehiclestg));
        const data = {
            vehicles:vehicle,
            facility:parkingInfo,
            facility_id:facility_id,
            vehicleType:vehicle.vehicle_type
        }
        // console.log(`data ${JSON.stringify(data)}`)
        setSpinner(true)
        const request = new Request();
        const result = await request.request(PRODUCT.saveHourProduct,'POST',data)
        if(result === 0){
            setSpinner(false)
            Toast.showWithGravity('No es posible registrar el ingreso al parqueadero', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});                      
        }else{
            setSpinner(false)
            Toast.showWithGravity('Ingreso registrado correctamente', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
            navigation.navigate("BarNavigationRegister",{register:true})
        }
    }
      ///Método para traer la info del parking
      const getParkingInfo = async () => {
        setSpinner(true)
        const data = {id:facility_id.toString()}
        const request = new Request();
        const result = await request.request(FACILITY.infoFacility,'POST',data)
        if(result === 0){
            setSpinner(false)
            Alert.alert('Error',"No es posible consultar la información del parqueadero");                      
        }else{
            setSpinner(false)
            setParkingInfo(result.ResponseData)
        }
    }

    const checkOpening = async (parking) => {
        const currentDate = moment();
        const dayOfWeekNumber = currentDate.day();

        await parking.schedule.standard.map((day)=>{
            if(day.day == dayOfWeekNumber){
                setOpenHour(day.open_hours)
                setCloseHour(day.close_hours)

                const now = moment();

                const horaAperturaMoment = moment(day.open_hours, 'HH:mm', true);
                const horaCierreMoment = moment(day.close_hours, 'HH:mm', true);

                if (now.isBetween(horaAperturaMoment, horaCierreMoment)) {
                    setOpen(1)
                }
            }
        })
    }

    ///////////verificar si tiene vehiculos registrados
    const getVehicles = async () => {
        setSpinner(true)
        const request = new Request();
        const result = await request.request(VEHICLE.listVehiclePerCustomer,'POST',null)
        if(result === 0){
            setSpinner(false)
            Toast.showWithGravity('No es posible listar los vehiculos', Toast.SHORT, Toast.TOP, {backgroundColor: 'blue',});
        }else{
            setSpinner(false)
            setVehicles(result.ResponseData)
        }        
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            console.log(`parqueadero ${facility_id}`)
            getParkingInfo()
            getVehicles();
            // getToken();
        });
        return unsubscribe;
    }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.navigate("BarNavigationRegister",{register:true})}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
                <Text style={{fontFamily:'Gotham-Bold',color:secondary,marginLeft:10,fontSize:16}}>Ingreso a parqueadero</Text>
            </View>
            <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:16 }}>
                <View style={{ marginTop: 15}}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                    {
                        parkingInfo !== null ?
                        parkingInfo.name
                        :
                        ""
                    }
                    </Text>
                    {
                        parkingInfo !== null ?
                            parkingInfo.address !== null ? 
                                <Text style={styles.text}> {parkingInfo.address}</Text>
                            :
                            <Text style={styles.text}>No hay dirección disponible</Text>
                        :
                        <Text style={styles.text}>No hay dirección disponible</Text>
                    }
                    
                    {/* <View style={{flexDirection: 'row'}} >
                    {
                        parkingInfo !== null &&
                            parkingInfo.open == 1 ? 
                            <Text style={{color:primary800,fontSize: 13,fontFamily: 'Gotham-Bold',}}>Abierto </Text>
                            :
                            <Text style={{color:'red',fontSize: 13,fontFamily: 'Gotham-Bold',}}>Cerrado </Text>
                    }    
                        
                        {
                            parkingInfo !== null ?
                                parkingInfo.finalHour !== "" && parkingInfo.initialHour !== "" ? 
                                    <Text style={styles.text}>{parkingInfo.initialHour} - {parkingInfo.finalHour}</Text>
                                :
                                <Text style={styles.text}>No hay horario disponible</Text>
                            :
                            <Text style={styles.text}>No hay horario disponible</Text>
                        }
                    </View> */}
                </View>
            </View>
            <View style={styles.container}>
               
                <Text style={styles.title2}>Vehículo</Text>
                <Text style={styles.text2}>Selecciona el vehículo con el que estas ingresando al parqueadero</Text>
                <View >
                    {   
                        
                        vehicles.map( (vehicle) => {
                            return <TouchableOpacity style={{justifyContent:'center'}} key={vehicle.id} onPress={() => saveProduct(vehicle)}>
                                        <View
                                        style={{
                                            backgroundColor: surface,
                                            borderRadius: 5,
                                            width:width-40,
                                            marginLeft: 0,
                                            marginRight: 1,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                            width: 0,
                                            height: 1,
                                            },
                                            shadowOpacity: 0.49,
                                            shadowRadius: 4.65,
                                            elevation: 3,
                                            marginBottom:10,
                                            marginTop:10,
                                            paddingTop:5,
                                        }}
                                        >
                                            <View style={{flexDirection: 'row',marginBottom:10,marginLeft:15}}>
                                                {
                                                    vehicle.vehicle_type_id === '1' &&
                                                    <FontAwesomeIcon size={20} icon={faCarSide} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === '2' &&
                                                    <FontAwesomeIcon size={20} icon={faMotorcycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                {
                                                    vehicle.vehicle_type_id === '3' &&
                                                    <FontAwesomeIcon size={20} icon={faBicycle} color={secondary} style={{marginLeft:10,marginTop:12,marginRight:20}}/>
                                                }
                                                <View>
                                                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:16,marginTop:10 }}>
                                                        {
                                                            vehicle.vehicle_type_id === '1' &&
                                                            "Automóvil"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === '2' &&
                                                            "Motocicleta"
                                                        }
                                                        {
                                                            vehicle.vehicle_type_id === '3' &&
                                                            "Bicicleta"
                                                        }
                                                                
                                                    </Text>
                                                    <Text style={{ fontFamily: "Gotham-Light", color: secondary,fontSize:14,marginTop:5 }}>
                                                        {vehicle.vehicle_plate}
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            })
                    }
                    
                </View>
                
            </View>
            <View style={{backgroundColor:surface,paddingBottom:20,paddingTop: 120}}>
                <TouchableOpacity onPress={() => navigation.navigate('AddVehicle',{productId:productIdReserve})} >
                    <Text style={{ fontSize: 15,fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center", color:primary800}}>
                        REGISTRAR NUEVO VEHÍCULO
                    </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingLeft:20,
      paddingBottom:20,
      paddingTop:30,
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:60,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        marginRight:15,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
  })