import React, { useState,useEffect }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView,Dimensions, Alert,Modal,TouchableWithoutFeedback} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {Picker} from '@react-native-picker/picker';
import Spinner from "react-native-loading-spinner-overlay";
import { API_URL } from '../../url';
import axios from "axios";
import {productIdReserve,productIdPasadia} from "../utils/varService"
import { MaterialIcons,Feather,Ionicons,FontAwesome } from '@expo/vector-icons';
import { TextInput as PaperTextInput } from 'react-native-paper';
import {primary600,primary800,secondary,surface, colorGray, colorGrayOpacity,surfaceMediumEmphasis, colorInput, colorInputBorder, colorPrimaryLigth } from '../utils/colorVar';
var height = Dimensions.get('window').height;
var width = Dimensions.get('window').width;

import { Request } from '../utils/api';
import { VEHICLE } from '../utils/endpoints';

export default function AddVehicle({navigation }) {
    const [spinner,setSpinner] = useState(false);
    const [vehicleTypeId, setVehicleTypeId] = useState(null);
    const [licensePlate, setLicensePlate] = useState(null);
    const [vehicleType, setVehicleType] = useState([
        {
            id:null,
            name:'Tipo de vehículo'
        },
        {
            id:1,
            name:'Automovil'
        },
        {
            id:2,
            name:'Motocicleta'
        },
        {
            id:3,
            name:'Bicicleta'
        }
      
    ]);
    const [parking, setParking] = useState(null);
    const [modalRegister, setModalRegister] = useState(false);
    const getToken = async () => {
        try {
          const value = await AsyncStorage.getItem("token");
          if (value !== null) {
            global.token = value;
          }
        } catch (error) { }
      };

    const registerVehicle =async () => {
        setSpinner(true)
        const data = {
            vehicle_type_id: vehicleTypeId.toString(),
            vehicle_plate: licensePlate,
        }
        const request = new Request();
        const result = await request.request(VEHICLE.registerVehicle,'POST',data)
        if(result === 0){
            setSpinner(false)
            Alert.alert('Error',"No fue posible registrar el vehiculo");                      
        }else{
            setSpinner(false)
            Alert.alert('Exito',result.ResponseMessage);
            navigation.goBack(null)

        }
    }

    const shorModal = () => {
        if(vehicleTypeId === null){
            Alert.alert("Error","El tipo de vehículo es obligatorio.");
        } else if(licensePlate === null || licensePlate === ''){
            Alert.alert("Error","La placa del vehículo es obligatoria.");
        }else{
            if(vehicleTypeId === 1){
                var r = /[a-zA-Z]{3}[0-9]{3}|[a-zA-Z]{3}[0-9]{2}[a-zA-Z]/g
                if(r.test(licensePlate) === false){
                    Alert.alert("Error","La placa es inválida. Debe ser alfanumerica siguiendo el ejemplo: ABC123");
                }else{
                    registerVehicle()
                }
            }else if(vehicleTypeId == 2){
                var r = /[a-zA-Z]{3}[0-9],[a-zA-Z]{3}|[a-zA-Z]{3}[0-9]{2}[a-zA-Z]/g;
                if(r.test(licensePlate) === false){
                    Alert.alert("Error","La placa es inválida. Debe ser alfanumerica siguiendo el ejemplo: ABC12A");
                }else{
                    registerVehicle()
                }
            }else{
                registerVehicle()
            }

        }
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
        });
        return unsubscribe;
      }, [navigation]);
    return(
        <ScrollView style={{backgroundColor:colorGray}}>
            <Spinner visible={spinner}  color={primary600} />
            <View style={{ paddingLeft: 20,paddingTop:20,flexDirection:'row' ,alignContent:'center',backgroundColor:surface}}>
                <TouchableOpacity onPress={() => navigation.goBack(null)}>
                    <MaterialIcons name="arrow-back" size={24} color={secondary} />
                </TouchableOpacity>
            </View>
            <View style={{paddingLeft: 20,backgroundColor:surface,paddingTop:10,paddingBottom:10,marginBottom:16 }}>
                <View style={{ marginTop: 15}}>
                    <Text style={{ fontFamily: "Gotham-Bold", color: secondary,fontSize:20, marginBottom:5,textTransform:'capitalize' }}>
                    {
                        parking !== null ?
                        parking.name
                        :
                        ""
                    }
                    </Text>
                </View>
            </View>
            <View style={styles.container}>
                <Text style={styles.title2}>Información del vehículo</Text>
                <Text style={styles.text2}>Descripción opcional...................</Text>
                <View style={styles.pickerStyle}>
                    <Picker 
                        selectedValue={vehicleTypeId}
                        style={{height: 40, width: width-40,borderColor: secondary,borderWidth: 1}} 
                        onValueChange={(itemValue, itemIndex) => setVehicleTypeId(itemValue)}>
                            {
                            vehicleType.map( (vehicle) => {
                            return <Picker.Item label={vehicle.name} color={colorInput} value={vehicle.id} key={vehicle.id}/>
                            })
                            }  
                    </Picker>
                </View>
                {
                    vehicleTypeId ?
                        vehicleTypeId != 3 ? 
                        <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,textTransform:'uppercase'}}  
                                onChangeText={(text) => setLicensePlate(text)} maxLength={6}
                                theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                mode='outlined' label="Placas del vehículo" outlineColor={colorInputBorder}/> 
                        :
                        <PaperTextInput style={{width:width-35,backgroundColor:surface,color:colorInput,marginVertical:20,fontSize:14,textTransform:'uppercase'}}  
                                onChangeText={(text) => setLicensePlate(text)} maxLength={16}
                                theme={{colors:{text:colorInput,primary:secondary},roundness:10}}
                                mode='outlined' label="Placas del vehículo" outlineColor={colorInputBorder}/> 

                    :
                    <View></View>

                }
                <View style={{marginBottom:10,flex:3, marginTop:70}}>
                    <TouchableOpacity style={{justifyContent:'center',alignItems:'center',backgroundColor: secondary,padding: 14,borderRadius: 10,width: width-30,}}
                        onPress={() => registerVehicle()}
                    >
                        <Text style={styles.titleButton1}>REGISTRAR VEHÍCULO</Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalRegister}
                >
                    <TouchableWithoutFeedback onPress={() => setModalRegister(!modalRegister)}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.title}>Registrar Vehiculo</Text>
                                <Text style={styles.text2}>¿Desea registrar el vehículo en su cuenta o sólo para {global.reserve && 'esta reserva?'}</Text>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => registerVehicle(1)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>REGISTRAR EN CUENTA</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{marginBottom:10}}>
                                    <TouchableOpacity style={{backgroundColor: secondary,padding: 14,borderRadius: 10, width: width-30,}} onPress={() => registerVehicle(1)}>
                                        <Text style={{color: colorPrimaryLigth, fontSize: 15, fontFamily:'Gotham-Bold',textAlignVertical: "center",textAlign: "center",}}>REGISTRAR EN ESTE PRODUCTO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        </ScrollView>
    );

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: surface,
      paddingBottom:20,
      paddingTop:20,
      paddingHorizontal:16,
      flex:1
    },
    title: {
        color: '#000',
        fontSize: 18,
        fontFamily:'Gotham-Medium',
    },
    title2: {
        color: secondary,
        fontSize: 23,
        fontFamily:'Gotham-Bold',
        marginBottom:20,
    },
    text: {
        color: surfaceMediumEmphasis,
        fontSize: 14,
        fontFamily: 'Gotham-Light',
        marginBottom:10,
    },
    text2: {
        color: '#000',
        fontSize: 13,
        fontFamily: 'Gotham-Light',
        marginBottom:30,
    },
    lineStyle:{
        marginTop:5,
        marginBottom:10,
        backgroundColor: colorGrayOpacity,
        height: 2,
        width: 320,
    },
    input:{
        height:50,  
        color:colorInput, 
        borderRadius:10,
        width:width-30,
        marginTop:10,
        marginBottom:70,
        paddingHorizontal:25,
        borderColor: colorInputBorder,
        borderWidth: 1,
        fontFamily:'Gotham-Light',
        textDecorationLine:'none'
      },
      titleButton1: {
        color: colorPrimaryLigth,
        fontSize: 15,
        fontFamily:'Gotham-Bold',
      },
      button1: {
        backgroundColor: secondary,
        padding: 14,
        borderRadius: 10,
        width: width-30,
      },
      button3: {
        height:50, 
        backgroundColor: "#fff",
        padding: 14,
        borderRadius: 10,
        width: 320,
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        borderColor: secondary,
        borderWidth: 1.2,
      },
      titleButton3: {
        color: secondary,
        fontFamily:'Gotham-Bold',
        fontSize: 15,
      },
      pickerStyle: {
        borderRadius:10,
        padding:5,
        height: 50,
        marginTop:10,
        marginBottom:10,
        borderColor: colorInputBorder,
        borderWidth: 1,
    },
    centeredView: {
        flex: 1,
        backgroundColor:'rgba(90, 90, 90, 0.5)'
      },
    modalView: {
        width:width,
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        position:'absolute',
        bottom:0,
        elevation: 5
    },
  })