import React from 'react';
import { StyleSheet, Text, View, StatusBar, ActivityIndicator  } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import * as Font from "expo-font";
import { NavigationContainer } from '@react-navigation/native';
import Constants from "expo-constants";
import { createStackNavigator } from '@react-navigation/stack';
import Register from './src/register';
import Login from './src/login';
import FormRegister from './src/formRegister';
import Home from './src/registered/home';
import BarNavigationRegister from './src/registered/barNavigationRegister';
import { primary500,surface } from './src/utils/colorVar';
import DetailParking from './src/parking/detailParking';
import Reserve from './src/parking/reserve/reserve';
import NoVehicle from './src/vehicles/noVehicle';
import AddVehicle from './src/vehicles/addVehible';
import ListVehicles from './src/vehicles/listVehicles';
import listVehiclesHour from './src/vehicles/listVehiclesHour';
import InfoServiceHour from './src/parking/horas/infoService';
import Pasadia from './src/parking/pasadia/pasadia';
import ListVehiclesPasadia from './src/parking/pasadia/listVehiclesPasadia';
import SumaryPasadia from './src/parking/pasadia/sumaryPasadia';
import SumaryReserve from './src/parking/reserve/sumaryReserve';
import ScheduledReserve from './src/parking/reserve/scheduledReserve';
import ActiveReserve from './src/parking/reserve/activeReserve';
import TimeExtensionReserve from './src/parking/reserve/timeExtensionReserve';
import ExtendedReserve from './src/parking/reserve/extendedReserve';
import ReserveCompleted from './src/parking/reserve/reserveCompleted';
import PurchasedPasadia from './src/parking/pasadia/purchasedPasadia';
import ActivePasadia from './src/parking/pasadia/activePasadia';
import PasadiaCompleted from './src/parking/pasadia/pasadiaCompleted';
import Account from './src/account/account';
import FormBeParking from './src/account/beParking/formBeParking';
import AddVehicleBeParking from './src/account/beParking/addVehicleBeParking';
import BeParking from './src/account/beParking/beParking';
import GetPoints from './src/account/beParking/getPoints';
import Defeated from './src/account/beParking/defeated';
import Courtesy from './src/account/courtesy/courtesy';
import ListVehicleCourtesy from './src/vehicles/listVehiclesCourtesy';
import AddVehicleCourtesy from './src/account/courtesy/addVehicleCourtesy';
import ActiveCourtesy from './src/account/courtesy/activeCourtesy1';
import ListCourtesy from './src/account/courtesy/listCourtesy';
import CourtesyCompleted from './src/account/courtesy/courtesyCompleted';
import ChangeVehicle from './src/account/courtesy/changeVehicle';
import Valet from './src/parking/valetParking/valet';
import MapsPrueba from './src/parking/mapsPrueba';
import ConsolidatedValet from './src/parking/valetParking/ consolidatedValet';
import ActiveValet from './src/parking/valetParking/activeValet';
import CheckListValet from './src/parking/valetParking/checkListValet';
import RequestVehicle from './src/parking/valetParking/requestVehicle';
import PaymentMethods from './src/parking/paymentMethods/paymentMethods';
import DescriptionBenefit from './src/account/beParking/descriptionBenefit';
import QRVehicle from './src/parking/valetParking/qrVehicle';
import VehicleImperfections from './src/parking/valetParking/vehicleImperfections';
import ValetCompleted from './src/parking/valetParking/valetCompleted';
import BeneficiaryCourtesy from './src/account/courtesy/beneficiaryCourtesy';
import ListParkingPasadia from './src/parking/pasadia/listParkingPasadia';
import ListParking from './src/parking/listParking';
import ListPasadia from './src/parking/pasadia/listPasadia';
import listVehiclesMoto from './src/parking/pasadia/listVehiclesMoto';
import ListReserve from './src/parking/reserve/listReserve';
import ServiceReserve from './src/parking/reserve/serviceReserve';
import ListParkingReserve from './src/parking/reserve/listParkingReserve';
import ListVehicleReserve from './src/parking/reserve/listVehicleReserve';
import Mensualidad from './src/parking/mensualidades/mensualidad';
import ListParkingMonthly from './src/parking/mensualidades/listParkingMonthly';
import SelectMonthly from './src/parking/mensualidades/selectMonthly';
import ListVehicleMonthly from './src/parking/mensualidades/listVehicleMonthly';
import SumaryMonthly from './src/parking/mensualidades/sumaryMonthly';
import PurchasedMonthly from './src/parking/mensualidades/purchasedMonthly';
import ActiveMonthly from './src/parking/mensualidades/activeMonthly';
import FilterList from './src/parking/mensualidades/filterList';
import PurchaseMonthly from './src/parking/mensualidades/purchaseMonthly';
import ListMonthly from './src/parking/mensualidades/listMonthly';
import InfoBeneficiaryMonthly from './src/parking/mensualidades/infoBeneficiaryMonthly';
import PurchaseMonthlys from './src/parking/mensualidades/purchaseMonthlys';
import CodeBeneficiary from './src/parking/mensualidades/codeBeneficiary';
import ListParksBeParking from './src/account/beParking/listParksBePaking'
import SelectedPark from './src/account/beParking/selectedPark'
import SelectVehicle from './src/account/beParking/selectVehicle'
import PurchasedBonus from './src/account/beParking/purchasedbonus'
import ActivedBonus from './src/account/beParking/activedBonus'
import FinalyBonus from './src/account/beParking/finalyBonus'
import ExtendBonus from './src/account/beParking/extendBonus'
import AddBeneficiary from './src/account/courtesy/addBeneficiary'
import BeParkingBlue from './src/account/beParking/beParkingBlue'
import DitailsCourtesy from './src/account/courtesy/ditailsCourtesy'
import ListParkingValet from './src/parking/valetParking/listParkingValet'
import RegisterVehicle from './src/parking/valetParking/registerVehicle'
import FormVehicle from './src/parking/valetParking/formVehicle'
import ActiveValet1 from './src/parking/valetParking/activeValet1'
//_____________________________
import LegalListVehicles from './src/parking/mensualidades/legalListVehicles';
import LegalSumaryMonthly from './src/parking/mensualidades/legalSumaryMonthly';
import LegalActive from './src/parking/mensualidades/legalActive';
import NaturalListVehicles from './src/parking/mensualidades/naturalListVehicles';
import ListVehicleBeParking from './src/account/beParking/listVehicleBeParking'
import LegalSumary2 from './src/parking/mensualidades/legalSumary2'
import CodeEvent from './src/parking/eventos/codeEvent'
import DetailsEvent from './src/parking/eventos/detailsEvent'
import InfoEvent from './src/parking/eventos/infoEvent'
import codeVipGold from './src/parking/vipGold/codeVipGold'
import FormVipGold from './src/parking/vipGold/formVipGold'
import ListVehiclesVipGold from './src/vehicles/listVehiclesVipGold'
import MembershipActivated from './src/parking/vipGold/membershipActivated'
import ListParkingVipGold from './src/parking/vipGold/listParkingVipGold'
import ListVip from './src/parking/vipGold/listVip'
import MembershipActivated1 from './src/parking/vipGold/merbershipActivated1'
import ValidationScanner from './src/account/validation/validationScanner'
import ViewValidation from './src/account/validation/viewValidation'
import SumaryValidation from './src/account/validation/sumaryValidation';
import ActivatedValidation from './src/account/validation/ActivatedValidation';
import HomeHours from './src/account/horas/HomeHours';
import ServiceHours from './src/account/horas/ServiceHours';
// import ListVehiclesHours from './src/account/horas/ListVehiclesHours';
import QrHours from './src/account/horas/QrHours';
import ActiveServiceHours from './src/account/horas/ActiveServiceHours';
import FinalHours from './src/account/horas/FinalHours';
import MyServices from './src/account/myServices/myServices';
import InfoWallet from './src/account/wallet/infoWallet';
//_____________________________
export default class App extends React.Component {
  state = {
    assetsLoaded: false,
    version:null,
  };

  async componentDidMount() {
    try {
      await Font.loadAsync({
        'Gotham-Black': require('./assets/fonts/Gotham-Black.otf'),
        'Gotham-Bold':require('./assets/fonts/Gotham-Bold.otf'),
        'Gotham-Book':require('./assets/fonts/Gotham-Book.otf'),
        'Gotham-Light':require('./assets/fonts/Gotham-Light.otf'),
        'Gotham-Medium':require('./assets/fonts/Gotham-Medium.otf'),
      });
      this.setState({version:Constants.manifest.version});
      this.setState({ assetsLoaded: true });
    } catch (error) {
      console.error('Error al cargar fuentes:', error);
    }
      
  };

  render(){
    const Stack = createStackNavigator();
    const {assetsLoaded} = this.state;
    if( assetsLoaded ) {
      return (
        <NavigationContainer style={styles.container}>
          <StatusBar backgroundColor={primary500} barStyle="dark-content" />
          <Stack.Navigator initialRouteName="Register" headerMode={'none'}>
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="FormRegister" component={FormRegister} />
            <Stack.Screen name="MyServices" component={MyServices} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="BarNavigationRegister" component={BarNavigationRegister} />
            <Stack.Screen name="DetailParking" component={DetailParking} />
            <Stack.Screen name="ListParking" component={ListParking} />
            <Stack.Screen name="Reserve" component={Reserve} />
            <Stack.Screen name="NoVehicle" component={NoVehicle} />
            <Stack.Screen name="AddVehicle" component={AddVehicle} />
            <Stack.Screen name="ListVehicles" component={ListVehicles} />
            <Stack.Screen name="listVehiclesHour" component={listVehiclesHour} />
            <Stack.Screen name="InfoServiceHour" component={InfoServiceHour} />
            <Stack.Screen name="Pasadia" component={Pasadia} />
            <Stack.Screen name="ListVehiclesPasadia" component={ListVehiclesPasadia} />
            <Stack.Screen name="SumaryPasadia" component={SumaryPasadia} />
            <Stack.Screen name="ListParkingPasadia" component={ListParkingPasadia} />
            <Stack.Screen name="SumaryReserve" component={SumaryReserve} />
            <Stack.Screen name="ScheduledReserve" component={ScheduledReserve} />
            <Stack.Screen name="ActiveReserve" component={ActiveReserve} />
            <Stack.Screen name="TimeExtensionReserve" component={TimeExtensionReserve} />
            <Stack.Screen name="ExtendedReserve" component={ExtendedReserve} />
            <Stack.Screen name="ReserveCompleted" component={ReserveCompleted} />
            <Stack.Screen name="PurchasedPasadia" component={PurchasedPasadia} />
            <Stack.Screen name="ActivePasadia" component={ActivePasadia} />
            <Stack.Screen name="PasadiaCompleted" component={PasadiaCompleted} />
            <Stack.Screen name="Account" component={Account} />
            <Stack.Screen name="FormBeParking" component={FormBeParking} />
            <Stack.Screen name="AddVehicleBeParking" component={AddVehicleBeParking} />
            <Stack.Screen name="BeParking" component={BeParking} />
            <Stack.Screen name="GetPoints" component={GetPoints} />
            <Stack.Screen name="Defeated" component={Defeated} />
            <Stack.Screen name="DescriptionBenefit" component={DescriptionBenefit} />
            <Stack.Screen name="Courtesy" component={Courtesy} />
            <Stack.Screen name="ListVehicleCourtesy" component={ListVehicleCourtesy} />
            <Stack.Screen name="AddVehicleCourtesy" component={AddVehicleCourtesy} />
            <Stack.Screen name="ActiveCourtesy" component={ActiveCourtesy} />
            <Stack.Screen name="ListCourtesy" component={ListCourtesy} />
            <Stack.Screen name="CourtesyCompleted" component={CourtesyCompleted} />
            <Stack.Screen name="BeneficiaryCourtesy" component={BeneficiaryCourtesy} />
            <Stack.Screen name="ChangeVehicle" component={ChangeVehicle} />
            <Stack.Screen name="Valet" component={Valet} />
            <Stack.Screen name="MapsPrueba" component={MapsPrueba} />
            <Stack.Screen name="ConsolidatedValet" component={ConsolidatedValet} />
            <Stack.Screen name="ActiveValet" component={ActiveValet} />
            <Stack.Screen name="CheckListValet" component={CheckListValet} />
            <Stack.Screen name="RequestVehicle" component={RequestVehicle} />
            <Stack.Screen name="PaymentMethods" component={PaymentMethods} />
            <Stack.Screen name="QRVehicle" component={QRVehicle} />
            <Stack.Screen name="VehicleImperfections" component={VehicleImperfections} />
            <Stack.Screen name="ValetCompleted" component={ValetCompleted} />
            <Stack.Screen name='ListPasadia' component={ListPasadia} /> 
            <Stack.Screen name='ListVehiclesMoto' component={listVehiclesMoto}/>
            <Stack.Screen name='ListReserve' component={ListReserve}/>
            <Stack.Screen name='ServiceReserve' component={ServiceReserve}/>
            <Stack.Screen name='ListParkingReserve' component={ListParkingReserve}/>
            <Stack.Screen name='ListVehicleReserve' component={ListVehicleReserve}/>
            <Stack.Screen name='Mensualidad' component={Mensualidad}/>
            <Stack.Screen name='ListParkingMonthly' component={ListParkingMonthly}/>
            <Stack.Screen name='SelectMonthly' component={SelectMonthly}/>
            <Stack.Screen name='ListVehicleMonthly' component={ListVehicleMonthly}/>
            <Stack.Screen name='SumaryMonthly' component={SumaryMonthly}/>
            <Stack.Screen name='PurchasedMonthly' component={PurchasedMonthly}/>
            <Stack.Screen name='ActiveMonthly' component={ActiveMonthly}/>
            <Stack.Screen name='FilterList' component={FilterList}/>
            <Stack.Screen name='PurchaseMonthly' component={PurchaseMonthly}/>
            <Stack.Screen name='ListMonthly' component={ListMonthly} />
            <Stack.Screen name='InfoBeneficiaryMonthly' component={InfoBeneficiaryMonthly} />
            <Stack.Screen name='PurchaseMonthlys' component={PurchaseMonthlys}/>
            <Stack.Screen name='CodeBeneficiary' component={CodeBeneficiary}/>
            <Stack.Screen name='ListParksBeParking' component={ListParksBeParking}/>
            <Stack.Screen name='SelectedPark' component={SelectedPark}/>
            <Stack.Screen name='SelectVehicle' component={SelectVehicle}/>
            <Stack.Screen name='PurchasedBonus' component={PurchasedBonus}/>
            <Stack.Screen name='ActivedBonus' component={ActivedBonus}/>
            <Stack.Screen name='FinalyBonus' component={FinalyBonus}/>
            <Stack.Screen name='ExtendBonus' component={ExtendBonus}/>
            <Stack.Screen name='AddBeneficiary' component={AddBeneficiary}/>
            <Stack.Screen name='BeParkingBlue' component={BeParkingBlue}/>
            <Stack.Screen name='DitailsCourtesy' component={DitailsCourtesy}/>
            <Stack.Screen name='ListParkingValet' component={ListParkingValet}/>
            <Stack.Screen name='RegisterVehicle' component={RegisterVehicle}/>
            <Stack.Screen name='FormVehicle' component={FormVehicle}/>
            <Stack.Screen name='ActiveValet1' component={ActiveValet1}/>
            {/* //______________________________________________ */}
            <Stack.Screen name='LegalListVehicles' component={LegalListVehicles}/>
            <Stack.Screen name='LegalSumaryMonthly' component={LegalSumaryMonthly}/>
            <Stack.Screen name='LegalActive' component={LegalActive}/>
            <Stack.Screen name='NaturalListVehicles' component={NaturalListVehicles}/>
            <Stack.Screen name='ListVehicleBeParking' component={ListVehicleBeParking}/>
            <Stack.Screen name='LegalSumary2' component={LegalSumary2}/>
            <Stack.Screen name='CodeEvent' component={CodeEvent}/>
            <Stack.Screen name='DetailsEvent' component={DetailsEvent}/>
            <Stack.Screen name='InfoEvent' component={InfoEvent}/>
            <Stack.Screen name='CodeVipGold' component={codeVipGold}/>
            <Stack.Screen name='FormVipGold' component={FormVipGold}/>
            <Stack.Screen name='ListVehiclesVipGold' component={ListVehiclesVipGold}/>
            <Stack.Screen name='MembershipActivated' component={MembershipActivated}/>
            <Stack.Screen name='ListParkingVipGold' component={ListParkingVipGold}/>
            <Stack.Screen name='ListVip' component={ListVip}/>
            <Stack.Screen name='MembershipActivated1' component={MembershipActivated1}/>
            <Stack.Screen name='ValidationScanner' component={ValidationScanner}/>
            <Stack.Screen name='ViewValidation' component={ViewValidation}/>
            <Stack.Screen name='SumaryValidation' component={SumaryValidation}/>
            <Stack.Screen name='ActivatedValidation' component={ActivatedValidation}/>
            <Stack.Screen name='HomeHours' component={HomeHours}/>
            <Stack.Screen name='ServiceHours' component={ServiceHours}/>
            {/* <Stack.Screen name='ListVehiclesHours' component={ListVehiclesHours}/> */}
            <Stack.Screen name='QrHours' component={QrHours}/>
            <Stack.Screen name='ActiveServiceHours' component={ActiveServiceHours}/>
            <Stack.Screen name='FinalHours' component={FinalHours}/>
            <Stack.Screen name='InfoWallet' component={InfoWallet}/>
            {/* //______________________________________________ */}

          </Stack.Navigator>
        </NavigationContainer>  
      );
    }else {
      return (
        <View style={styles.container}>
            <ActivityIndicator />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: surface
  },
});
